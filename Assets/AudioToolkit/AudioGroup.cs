﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioGroup : MonoBehaviour 
{
	public string groupName;
	public List<AudioEvent> audioEvents = new List<AudioEvent>();
	 
	void Awake () 
	{
		GetComponentsInChildren<AudioEvent>(false, audioEvents);
	}
}