﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEventTrigger : MonoBehaviour 
{

	public string eventName;
	public GameObject obj;
	public bool use2dPhysics;
	public TriggerOnEvent triggerEvent; 
	public TriggerType triggerType;
	 
	private void PlayEvent ()
	{
		if (eventName == null) return;

		AudioManager.instance.PlayEvent (eventName, gameObject);
	}

	private void StopEvent () 
	{
		if (eventName == null && obj == null) 
		{
			AudioManager.instance.StopAll ();
		}

		if (eventName != null && obj == null) 
		{
			AudioManager.instance.StopEvent (eventName);
		}

		if (eventName != null && obj !=null)
		{
			AudioManager.instance.StopEvent (eventName, obj);
		}
	}

	private void PauseEvent ()
	{
		if (eventName == null && obj == null) 
		{
			AudioManager.instance.PauseAll ();
		}

		if (eventName !=null && obj == null) 
		{
			AudioManager.instance.PauseEvent (eventName);
		}

		if (eventName != null && obj != null) 
		{
			AudioManager.instance.PauseEvent (eventName, obj);
		}
	}

	private void UnPauseEvent ()
	{
		if (eventName == null && obj == null) 
		{
			AudioManager.instance.UnpauseAll ();
		}

		if (eventName !=null && obj == null) 
		{
			AudioManager.instance.UnpauseEvent (eventName);
		}

		if (eventName != null && obj != null) 
		{
			AudioManager.instance.UnpauseEvent (eventName, obj);
		}
	}

	private void Trigger ()
	{
		if (triggerType == TriggerType.Play) PlayEvent ();

		if (triggerType == TriggerType.Stop) StopEvent ();

		if (triggerType == TriggerType.Pause) PauseEvent ();

		if (triggerType == TriggerType.Unpause) UnPauseEvent ();
	}

	void Start () 
	{
		if (triggerEvent == TriggerOnEvent.Start)   
		{
			Trigger ();
		}
	}
	
	void OnEnable ()
	{
		if (triggerEvent == TriggerOnEvent.OnEnable) 
		{
			Trigger ();
		}
	}

	void OnDestroy ()
	{
		if (triggerEvent == TriggerOnEvent.OnDestroy) 
		{
			Trigger ();
		}
	}

	void OnCollisionEnter ()
	{
		if (triggerEvent == TriggerOnEvent.OnCollisionEnter) 
		{
			Trigger ();
		}
	}

	void OnCollisionEnter2D ()
	{
		if (triggerEvent == TriggerOnEvent.OnCollisionEnter && use2dPhysics) 
		{
			Trigger ();
		}
	}

	void OnCollisionExit ()
	{
		if (triggerEvent == TriggerOnEvent.OnCollisionExit) 
		{
			Trigger ();
		}
	}

	void OnCollisionExit2D()
	{
		if (triggerEvent == TriggerOnEvent.OnCollisionExit && use2dPhysics) 
		{
			Trigger ();
		}
	}

	void OnTriggerEnter ()
	{
		if (triggerEvent == TriggerOnEvent.OnTriggerEnter) 
		{
			Trigger ();
		}
	}

	void OnTriggerEnter2D ()
	{
		if (triggerEvent == TriggerOnEvent.OnTriggerEnter && use2dPhysics) 
		{
			Trigger ();
		}
	}

	void OnTriggerExit ()
	{
		if (triggerEvent == TriggerOnEvent.OnTriggerExit) 
		{
			Trigger ();
		}
	}

	void OnTriggerExit2D ()
	{
		if (triggerEvent == TriggerOnEvent.OnTriggerExit && use2dPhysics) 
		{
			Trigger ();
		}
	}
}

public enum TriggerType
{
	Play,
	Pause,
	Unpause,
	Stop
}

public enum TriggerOnEvent
{
	Start,
	OnEnable,
	OnDestroy,
	OnCollisionEnter,
	OnCollisionExit,
	OnTriggerEnter,
	OnTriggerExit
}


