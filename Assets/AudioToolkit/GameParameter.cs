﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameParameter : ScriptableObject 
{
	public string paramName;
	public float minValue;
	public float maxValue;
	public float defaultValue;
	public float globalValue;

	public GameParameter ()
	{
		paramName = "new_parameter"; 
		minValue = 1;
		maxValue = 100;
		defaultValue = 50;
	}

	public void Initialize ()
	{
		globalValue = defaultValue;
	}
}
