﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class AudioEvent : MonoBehaviour {

	// EVENT SETTINGS
	public string eventName = "newEvent"; // event name that has to be called to Play this AudioEvent
	[Range (1, 100)]
	public int priority = 100; // event priority
	public bool staticPriority; // event priority does not scale with distance of emitter to player object
	[Range (1, 32)]
	public int globalInstanceLimit = 16; // max number of active instances
	[Range (1, 32)]
	public int localInstanceLimit = 16; // max number of active instances per game object
	[Range (1, 32)]
	public int radiusInstanceLimit = 16; // max number of active instances within radius
	[Range (0f, 500f)]
	public float radius = 0f;
	public bool discardOldest = true; // discard oldest emitter if instances are maxed
	public bool canVirtualize = false;

	//AUDIO SETTINGS
	[Range (-46f, 0f)]
	public float volume = 1f; // main volume (dB)
	[Range (-24f, 0f)]
	public float randomVolume; // random volume range (dB)
	[Range (-24f, 24f)]
	public float pitch; // main pitch (semitones)
	[Range (0f, 24f)]
	public float randomPitch; // random pitch range (semitones)
	[Range (20f, 22000f)]
	public float lpCutoff = 22000f; // lpw pass frequency
	[Range(20f, 22000f)]
	public float hpCutoff = 20f; // high pass frequency

	//SPATIALIZATION SETTINGS
	public bool spatializeSound;
	public bool ignoreThirdPersonMode;
	[Range (-1f, 1f)]
	public float panning;
	public bool childToCaller;
	public AudioRolloffMode rolloffMode = AudioRolloffMode.Custom;
	public AnimationCurve rolloffCurve = AnimationCurve.EaseInOut (0f, 1f, 1f, 0f);
	public AnimationCurve lpfCurve = AnimationCurve.EaseInOut (0f, 1f, 1f, 0f);
	[Range (1f, 500f)]
	public float minDist = 1f; // minimum distance for sound attenuation
	[Range (1f, 500f)]
	public float maxDist = 500f; // maximum distance for sound attenuation
	public bool occludable;
	public bool randomPosition; // enable/disable random position
	[Range (0, 250f)]
	public float minPosition; // minimum position
	[Range (0, 250f)]
	public float maxPosition; // maximum position

	//PLAYBACK SETTINGS
	public AudioMixerGroup mixerGroup;
	public bool dontRepeatLastClip;
	public PlayType playType;
	[Range (0f, 100f)]
	public float probability = 100f;
	[Range (0f, 15f)]
	public float delayTime; 
	[Range (0f, 15f)]
	public float randomDelay;
	[Range (0f, 15f)]
	public float fadeIn;
	[Range (0f, 15f)]
	public float fadeOut;
	public PlayMode playMode;
	public bool isLoop;
	[Range (0f, 15f)]
	public bool repeatSound; // repeat sound
	public bool dontDelayFirstPlay;

    //RTPC SETTINGS
	public List<RtpcSetting> rtpcSettings = new List<RtpcSetting>();
	public Dictionary<RtpcProperty, List<RtpcSetting>> rtpcByProp =  new Dictionary<RtpcProperty, List<RtpcSetting>>();

	//AUDIO CLIPS
	public string folderPath;
	public List<WeightedAudioClip> weightedClips = new List<WeightedAudioClip>(); 

	public int lastClipIndex = -1;
	public int lastRoll = -1;

	void Awake ()
	{
		AudioManager.instance.AddEvent (eventName, this);

		for (int i = 0; i < rtpcSettings.Count; i++) 
		{
			RtpcSetting rtpc = rtpcSettings [i];
			RtpcProperty property = rtpc.property;

			if(rtpcByProp.ContainsKey(property) == false) 
			{
				rtpcByProp[property] = new List<RtpcSetting>();
			}
			rtpcByProp[property].Add(rtpcSettings[i]);
		}
	}
}

public enum PlayType
{
	Random,
	Sequence
}

public enum PlayMode
{
	Step,
	Continuos
}