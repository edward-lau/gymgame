﻿using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioManager : MonoBehaviour 
{
	[Range (1, 64)]
	public int activeVoices = 32; //  the max number of sounds that can be playing at any given time (the number of ActiveEvents).
	[Range (1, 1024)]
	public int virtualVoices = 512; // the max number of virtual voices that can be playing (virtually) at any given time.
	public float minVolume = -46; 
	public float updateStep = 0.1f; 
	public Perspective perspective;
	public GameObject listenerObj; 
	public GameObject attenuationObj;
	public bool debugMode;

	public List<ActiveEvent> initializedEvents = new List<ActiveEvent> (); // contains all activeEvents that have been initialized
	public List<ActiveEvent> delayedEvents = new List<ActiveEvent> (); // contains activeEvents waiting to play after a delay
	public List<ActiveEvent> playingEvents = new List<ActiveEvent> (); // contains activeEvents playing sound via an emitter
	public List<ActiveEvent> virtualEvents = new List<ActiveEvent> (); // contains activeEvents that are playing virtually
	public List<ActiveEvent> pausedEvents = new List<ActiveEvent>(); // contains paused activeEvents

	public List<ActiveEvent> activeEventPool = new List<ActiveEvent> (); // contains pool of unused ActiveEvents
	public List<AudioEmitter> emitterPool = new List<AudioEmitter> (); // contains pool of unused AudioEmitters

	public List<GameParameter> gameParameters = new List<GameParameter> (); // contains list of game parameters used for setting RTPCs

	private Dictionary <string, AudioEvent > eventDictionary = new Dictionary<string, AudioEvent>(); // dictionary containing AudioEvents indexed by event name
	private Dictionary <string, GameParameter > paramDictionary = new Dictionary<string, GameParameter>();  

	private float currentUpdateTime = 0f; 
	private static AudioManager audioManager; 
	public static AudioManager instance
	{
		get
		{
			if (!audioManager)
			{
				audioManager = FindObjectOfType (typeof (AudioManager)) as AudioManager;
			}

			return audioManager; 
		}
	}

	#region Monobehaviour

	void Awake () 
	{
		for (int i = 0; i < activeVoices; i++) // Instantiate AudioEmitter pool;
		{
			AudioEmitter emitter = new GameObject().AddComponent<AudioEmitter>(); 
			emitter.transform.parent = gameObject.transform; 
			emitter.name = ("AudioEmitter"); 
			emitter.gameObject.SetActive (false); 
			instance.emitterPool.Add (emitter); 
		}

		for (int i = 0; i < virtualVoices; i++) // Instantiate virtual voices;
		{
			ActiveEvent activeEvent = ScriptableObject.CreateInstance<ActiveEvent>(); 
			instance.activeEventPool.Add (activeEvent); 
		}

		for (int i = 0; i < gameParameters.Count; i++) 
		{
			gameParameters [i].Initialize ();
		}
	}

    void Update ()
    {
        UpdateElapsedDelayTimes (); 
        UpdateElapsedPlayTimes ();  
        UpdateVirtualPlayTimes ();
    }

	void LateUpdate ()
	{
		currentUpdateTime += Time.deltaTime; 
		if (currentUpdateTime >= updateStep)  
		{
			currentUpdateTime = 0f; 

			if (virtualEvents.Count > 0) // attempt to devirtualize the highest priority virtual event
			{
				ActiveEvent virtualEvent = GetHighestPriorityVirtualEvent();
				if (virtualEvent)
				{
					DeVirtualize (virtualEvent);
				}
			}
		}
	}

	#endregion

	#region Public API

	/// <summary>
	/// Add an AudioEvent to the AudioManager's list of AudioEvents.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	/// <param name="audioEvent">Audio event.</param>
	public void AddEvent (string eventName, AudioEvent audioEvent)
	{
		if (eventDictionary.ContainsKey(eventName))
		{
			Debug.LogWarning ("There are multiple Audio Events named " + eventName);
			return;
		}

		eventDictionary.Add (eventName, audioEvent); 
	}

	/// <summary>
	/// Play an AudioEvent.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	public void PlayEvent (string eventName)
	{
		PlayEvent (eventName, gameObject);
	}

	/// <summary>
	/// Play an AudioEvent at a GameObject.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	/// <param name="gameObject">Game object.</param>
	public void PlayEvent (string eventName, GameObject eventCaller) 
	{
		AudioEvent audioEvent;

		if (!eventDictionary.TryGetValue (eventName, out audioEvent)) 
		{
			Debug.LogError ("Audio Event not found: " + eventName); 
			return;
		}

		if (audioEvent.weightedClips.Count == 0) 
		{
			Debug.LogError ("Audio Event does not contain any audio clips: " + eventName); 
			return;
		}

		ActiveEvent activeEvent = GetPooledActiveEvent (); 
		SetInitialActiveEventProperties (activeEvent, audioEvent, eventCaller);

		if (activeEvent.delayTime > 0 && audioEvent.dontDelayFirstPlay == false) 
		{
			initializedEvents.Add (activeEvent);
			delayedEvents.Add (activeEvent);
		}
		else
		{
			initializedEvents.Add (activeEvent);
			PlayEvent (eventName, audioEvent, activeEvent);
		}
	}

	/// <summary>
	/// Stops all AudioEvents.
	/// </summary>
	public void StopAll ()
	{
		for (int i = 0; i < initializedEvents.Count; i++) 
		{
			ActiveEvent activeEvent = initializedEvents[i];
			StopEvent (activeEvent);
		}
	}
		
	/// <summary>
	/// Stops all instances of an AudioEvent.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	public void StopEvent (string eventName)
	{
		for (int i = 0; i < initializedEvents.Count; i++) 
		{
			ActiveEvent activeEvent = initializedEvents[i];

			if (activeEvent.eventName == eventName) // Check parent and eventName match
			{
				StopEvent (activeEvent);
			}
		}
	}

	/// <summary>
	/// Stops all instances of an AudioEvent at a GameObject.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	/// <param name="obj">Object.</param>
	public void StopEvent (string eventName, GameObject eventCaller) // perhaps add fade out variable
	{
		for (int i = 0; i < initializedEvents.Count; i++) 
		{
			ActiveEvent activeEvent = initializedEvents[i];

			if (activeEvent.eventName == eventName && activeEvent.eventCaller == eventCaller) // Check parent and eventName match. Could also use AudioEvent?
			{
				StopEvent (activeEvent);
			}
		}
	}

	/// <summary>
	/// Pauses all AudioEvents.
	/// </summary>
	public void PauseAll ()
	{
		for (int i = 0; i < playingEvents.Count; i++) 
		{
			ActiveEvent activeEvent = playingEvents[i];
			PauseEvent (activeEvent);
		}
	}

	/// <summary>
	/// Pauses all instances of an AudioEvent.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	public void PauseEvent (string eventName)
	{
		for (int i = 0; i < playingEvents.Count; i++) 
		{
			ActiveEvent activeEvent = playingEvents[i];

			if (activeEvent.eventName == eventName) // Check parent and eventName match
			{
				PauseEvent (activeEvent);
			}
		}
	}

	/// <summary>
	/// Pauses all instances of an AudioEvent at a GameObject.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	/// <param name="eventCaller">Event caller.</param>
	public void PauseEvent (string eventName, GameObject eventCaller)
	{
		for (int i = 0; i < playingEvents.Count; i++) 
		{
			ActiveEvent activeEvent = playingEvents[i];

			if (activeEvent.eventName == eventName && activeEvent.eventCaller == eventCaller) // Check parent and eventName match. Could also use AudioEvent?
			{
				PauseEvent (activeEvent);
			}
		}
	}

	/// <summary>
	/// Unpauses all paused ActiveEvents.
	/// </summary>
	public void UnpauseAll ()
	{
		for (int i = 0; i < pausedEvents.Count; i++) 
		{
			ActiveEvent activeEvent = pausedEvents [i];
			UnpauseEvent (activeEvent);
		}
	}

	/// <summary>
	/// Unpauses all instances of an ActiveEvent playing an AudioEvent.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	public void UnpauseEvent (string eventName)
	{
		for (int i = 0; i < pausedEvents.Count; i++) 
		{
			ActiveEvent activeEvent = pausedEvents [i];

			if (activeEvent.eventName == eventName) // Check parent and eventName match
			{
				UnpauseEvent (activeEvent);
			}
		}
	}

	/// <summary>
	/// Unpauses all instances of an ActiveEvent playing an AudioEvent at a specified GameObject.
	/// </summary>
	/// <param name="eventName">Event name.</param>
	/// <param name="eventCaller">Event caller.</param>
	public void UnpauseEvent (string eventName, GameObject eventCaller)
	{
		for (int i = 0; i < pausedEvents.Count; i++) 
		{
			ActiveEvent activeEvent = pausedEvents [i];

			if (activeEvent.eventName == eventName && activeEvent.eventCaller == eventCaller) // Check parent and eventName match. Could also use AudioEvent?
			{
				UnpauseEvent (activeEvent);
			}
		}
	}

	/// <summary>
	/// Sets RTPC values of all ActiveEvents with properties set to be modified by a specified GameParameter
	/// </summary>
	/// <param name="paramName">Parameter name.</param>
	public void SetRtpc (string paramName, float paramValue)
	{
		for (int i = 0; i < gameParameters.Count; i++) 
		{
			GameParameter param = gameParameters [i];

			if (param.paramName == paramName) 
			{
				param.globalValue = paramValue;
			}
		}
	}

	/// <summary>
	/// Sets RTPC values of all ActiveEvents with properties set to be modified by a specified GameParameter at a game object.
	/// </summary>
	/// <param name="paramName">Parameter name.</param>
	public void SetRtpc (string paramName, float paramValue, GameObject obj)
	{
		for (int i = 0; i < gameParameters.Count; i++) 
		{
			GameParameter param = gameParameters [i];

			if (param.paramName == paramName) 
			{
				// loop through audio events at this rtpc a
			}
		}
	}
		
	#endregion

	#region Play Events

	private void PlayEvent (string eventName, AudioEvent audioEvent, ActiveEvent activeEvent)
	{
		if (audioEvent.probability < Random.Range (0, 100)) 
		{
			return;
		}
			
		GameObject eventCaller = activeEvent.eventCaller;
		AudioEmitter emitter = null;
		Vector3 position;
		int priority = 0;

		if (audioEvent.spatializeSound && audioEvent.randomPosition) // get a random transform for the emitter if necessary.
		{
			position = GetRandomTransform (eventCaller, audioEvent.minPosition, audioEvent.maxPosition); 
			activeEvent.localPosition = position - eventCaller.transform.position;
		} 
		else // if position is not randomized, the transform is equal to the event caller's transform
		{
			position = eventCaller.transform.position;
			activeEvent.localPosition = Vector3.zero;
		}

		if (audioEvent.spatializeSound) 
		{
			if (audioEvent.staticPriority == false) // if priority is not static, get a scaled priority for this initialized event
			{
				priority = GetScaledPriority (audioEvent, position, audioEvent.maxDist, audioEvent.priority);
			}
				
			if (audioEvent.maxDist < Vector3.Distance (position, GetAttenuatorPosition(audioEvent)) || audioEvent.radiusInstanceLimit <= GetInstancesInRadius (eventName, position, audioEvent.radius))
			{
				if (audioEvent.canVirtualize) 
				{
					PlayVirtualEvent (activeEvent, position);
					return;
				}
				else 
				{
					StopActiveEvent (activeEvent);
					return;
				}
			}
		} 
		else // if priority is static, priority is equal to the audioevent priority
		{
			priority = audioEvent.priority;
		}
			
		if (audioEvent.localInstanceLimit == GetLocalInstances (eventName, eventCaller)) // check if local instance count is maxed
		{
			if (!audioEvent.discardOldest) // if local instance count is reached and cannot discard, virtualize this event
			{
				if (audioEvent.canVirtualize) 
				{
					PlayVirtualEvent (activeEvent, position);
					return;
				}
				else 
				{
					StopActiveEvent (activeEvent);
					return;
				}
			}
			else // if local instance count is not maxed, discard the oldest local active event playing this event and get it's emitter
			{
				emitter = GetOldestLocalEmitter (eventName, eventCaller); 
			}
		}

		if (emitter == null) // if the local instance limit hasn't been reached, check if global instance count is maxed
		{
			if (audioEvent.globalInstanceLimit == GetGlobalInstances (eventName)) 
			{ 
				if (!audioEvent.discardOldest) // if global instance count is maxed and cannot discard, virtualize this event
				{
					if (audioEvent.canVirtualize) 
					{
						PlayVirtualEvent (activeEvent, position);
						return;
					}
					else 
					{
						StopActiveEvent (activeEvent);
						return;
					}
				}
				else // if global instance count is not maxed, discard the oldest global active event playing this event and get it's emitter
				{
					emitter = GetOldestEmitter (eventName); 
				}
			}
		}

		if (emitter == null)
		{
			emitter = GetPooledAudioEmitter (); // get a pooled audio emitter
		}

		if (emitter == null) // if no pooled emitters, get the emitter fromt the lowest priority active event
		{
			emitter = GetLowPriorityEmitter (priority); 
		}

		if (emitter == null) // If we could not get an active emitter playing a lower priority event...
		{
			emitter = GetEqualPriorityEmitter (priority); // get the oldest active emitter playing an equal priority event.
		}

		if (emitter == null) // if we could not get any emitters, virtualize if possible. Otherwise don't play anything.
		{
			if (audioEvent.canVirtualize) 
			{
				PlayVirtualEvent (activeEvent, position);
				return;
			}
			else 
			{
				StopActiveEvent (activeEvent);
				return;
			}
		}
			
		activeEvent.emitter = emitter;
		activeEvent.priority = priority;
		SetActiveEventProperties (activeEvent, audioEvent);
		activeEvent.state = ActiveEventStates.ToPlay;
		PlayEvent (activeEvent, position); // We have an emitter now. Play the event.
	}

    /// <summary>
    /// Play an AudioEvent.
    /// </summary>
    /// <param name="activeEvent">Active event.</param>
    /// <param name="position">Position.</param>
	private void PlayEvent (ActiveEvent activeEvent, Vector3 position)
	{
		AudioEvent audioEvent = activeEvent.audioEvent;
		AudioEmitter emitter = activeEvent.emitter;

		if (emitter) 
		{
			SetEmitterProperties (emitter, activeEvent); // set AudioEmitter properties
			SetEmitterTransform (emitter, activeEvent.eventCaller, position, audioEvent); // set emitter's parent and transform
			emitter.gameObject.SetActive (true); 
			if (activeEvent.state == ActiveEventStates.ToPlay) // if playing for the first time, start at the beginning of the audioclip
			{
				emitter.source.time = 0f; 
			}
			else if (activeEvent.state == ActiveEventStates.Devirtualizing) // if devirtualizing, start at the elapsed playtime
			{
				emitter.source.time = activeEvent.elapsedPlayTime; 
			}
			emitter.source.Play (); 
			if (audioEvent.fadeIn > 0f && activeEvent.state == ActiveEventStates.ToPlay) 
			{
				emitter.source.volume = 0f;
				emitter.FadeIn (activeEvent.baseVol, audioEvent.fadeIn); 
			} 
			else if (activeEvent.state == ActiveEventStates.Devirtualizing) 
			{
				float fadeTarget = activeEvent.volume;
				float fadeTime = 0.25f;
				if (activeEvent.elapsedPlayTime < activeEvent.audioEvent.fadeIn) 
				{
					fadeTarget = activeEvent.baseVol;
					fadeTime = activeEvent.audioEvent.fadeIn - activeEvent.elapsedPlayTime;
				}
				emitter.source.volume = 0f;
				emitter.FadeIn (fadeTarget, fadeTime); 
				virtualEvents.Remove (activeEvent);
			}
			activeEvent.state = ActiveEventStates.Playing; 
			playingEvents.Add (activeEvent);
		}
	}

	/// <summary>
	/// Play a virtual AudioEvent.
	/// </summary>
	/// <param name="activeEvent">Active event.</param>
	/// <param name="position">Position.</param>
	private void PlayVirtualEvent(ActiveEvent activeEvent, Vector3 position)
	{
		SetActiveEventProperties (activeEvent, activeEvent.audioEvent);
		activeEvent.volume = activeEvent.baseVol;
		activeEvent.state = ActiveEventStates.Virtual;
		virtualEvents.Add (activeEvent);
//		PlayEvent (activeEvent, position);
	}

	#endregion

	#region Set ActiveEvent / AudioEmitter Properties

	private void SetActiveEventProperties (ActiveEvent activeEvent, AudioEvent audioEvent)
	{
		activeEvent.eventName = audioEvent.eventName;
		activeEvent.audioEvent = audioEvent;
		if (audioEvent.playType == PlayType.Sequence) 
		{
			activeEvent.audioClip = GetNextClip (audioEvent.weightedClips, audioEvent);
		}
		else
		{
			activeEvent.audioClip = GetRandomClip (audioEvent.weightedClips, audioEvent); 
		}
		activeEvent.currentLength = activeEvent.audioClip.length / Mathf.Abs (AudioUtil.StToLinear (activeEvent.pitch));
		activeEvent.oldLength = activeEvent.currentLength;
		 
		float dB = 1f;
		if (audioEvent.randomVolume < 0f) 
		{
			dB = GetRandomVolume (audioEvent.volume, audioEvent.randomVolume);
		} 
		else 
		{
			dB = audioEvent.volume;
		}

		if (audioEvent.fadeIn > 0f) 
		{
			activeEvent.volume = -46f;
			activeEvent.fadeVol = -46f;
		} 
		else 
		{
			activeEvent.volume = dB;
			activeEvent.fadeVol = dB;
		}
		activeEvent.baseVol = dB;
		activeEvent.baseEventVol = audioEvent.volume;

		float st;
		if (audioEvent.randomPitch > 0f) 
		{
			st = GetRandomPitch (audioEvent.pitch, audioEvent.randomPitch);
		} 
		else 
		{
			st = audioEvent.pitch;
		}
		activeEvent.pitch = st;
		activeEvent.basePitch = st;
		activeEvent.baseEventPitch = audioEvent.pitch;
	}

	private void SetEmitterProperties (AudioEmitter emitter, ActiveEvent activeEvent)
	{
		AudioEvent audioEvent = activeEvent.audioEvent;
		emitter.gameObject.name = ("Audio Event: " + audioEvent.eventName); 
		emitter.activeEvent = activeEvent; 
		emitter.source.clip = activeEvent.audioClip;
		emitter.source.volume = AudioUtil.DbToLinear(activeEvent.volume);
		emitter.source.pitch = AudioUtil.StToLinear(activeEvent.pitch);
		emitter.source.loop = audioEvent.isLoop;
		emitter.source.dopplerLevel = 0f; 

		if (audioEvent.spatializeSound) 
		{
			emitter.source.spatialize = true;
			emitter.source.spatialBlend = 1f;
			emitter.source.rolloffMode = audioEvent.rolloffMode;
			if (audioEvent.rolloffMode == AudioRolloffMode.Custom) 
			{
				emitter.source.SetCustomCurve (AudioSourceCurveType.CustomRolloff, audioEvent.rolloffCurve); 
				emitter.lpFilter.customCutoffCurve = audioEvent.lpfCurve;
			}
			emitter.source.minDistance = audioEvent.minDist;
			emitter.source.maxDistance = audioEvent.maxDist;
		} 
		else 
		{
			emitter.source.spatialize = false;
			emitter.source.spatialBlend = 0f;
			emitter.source.panStereo = audioEvent.panning;
		}

		emitter.source.outputAudioMixerGroup = audioEvent.mixerGroup;
		emitter.lpFilter.cutoffFrequency = audioEvent.lpCutoff;
		emitter.hpFilter.cutoffFrequency = audioEvent.hpCutoff;
	}

	private void SetInitialActiveEventProperties (ActiveEvent activeEvent, AudioEvent audioEvent, GameObject eventCaller)
	{
		activeEvent.state = ActiveEventStates.Initialize;  
		activeEvent.audioEvent = audioEvent; 
		activeEvent.eventCaller = eventCaller;
		activeEvent.eventName = audioEvent.eventName;
		activeEvent.elapsedPlayTime = 0;
		activeEvent.elapsedDelayTime = 0;

		if (audioEvent.randomDelay > 0) 
		{
			activeEvent.delayTime = audioEvent.delayTime + Random.Range (0f - audioEvent.randomDelay, audioEvent.randomDelay);
		} 
		else
		{
			activeEvent.delayTime = audioEvent.delayTime;
		}
	}


	/// <summary>
	/// Sets the emitter parent and transform.
	/// </summary>
	/// <param name="emitter">Emitter.</param>
	/// <param name="obj">Object.</param>
	private void SetEmitterTransform (AudioEmitter emitter, GameObject eventCaller, Vector3 position, AudioEvent audioEvent)
	{
		emitter.transform.SetParent (eventCaller.transform);;
		if (perspective == Perspective.FirstPerson || audioEvent.ignoreThirdPersonMode || audioEvent.spatializeSound == false) 
		{
			emitter.transform.localPosition = new Vector3 (0, 0, 0);
		}
		else if (perspective == Perspective.ThirdPerson && audioEvent.ignoreThirdPersonMode == false)
		{
			float distance = Vector3.Distance (position, attenuationObj.transform.position);
			Vector3 normalizedVector = Vector3.Normalize (position - listenerObj.transform.position);
			emitter.transform.position = listenerObj.transform.position + (distance * normalizedVector); 
		}
	}
		
	#endregion

	#region Update ActiveEvents

	private void UpdateElapsedDelayTimes ()
	{
		for (int i = 0; i < delayedEvents.Count; i++) // update delayTime
		{
			ActiveEvent activeEvent = delayedEvents[i];

			if (activeEvent.isPaused == false)
			{
				activeEvent.elapsedDelayTime += Time.deltaTime;
			}

			if (activeEvent.elapsedDelayTime > activeEvent.delayTime) 
			{
				delayedEvents.Remove (activeEvent);
				PlayEvent (activeEvent.eventName, activeEvent.audioEvent, activeEvent);
			}
		}
	}

	private void UpdateElapsedPlayTimes()
	{
		for (int i = 0; i < playingEvents.Count; i++)
		{
			ActiveEvent activeEvent = playingEvents[i];

			activeEvent.currentLength = activeEvent.audioClip.length / Mathf.Abs (AudioUtil.StToLinear (activeEvent.pitch));

			if (activeEvent.currentLength != activeEvent.oldLength) 
			{
				activeEvent.elapsedPlayTime *= (activeEvent.currentLength / activeEvent.oldLength); 
				activeEvent.oldLength = activeEvent.currentLength;
			}

			if (activeEvent.isPaused == false)
			{
				activeEvent.elapsedPlayTime += Time.deltaTime;
			}

			if (activeEvent.audioEvent.isLoop) 
			{
				if (activeEvent.elapsedPlayTime > activeEvent.audioClip.length) 
				{
					activeEvent.elapsedPlayTime = 0f;
				}
				continue;
			}

			if (activeEvent.audioEvent.fadeOut > 0) // fade out if necessary
			{
				if ((activeEvent.elapsedPlayTime >= activeEvent.currentLength - activeEvent.audioEvent.fadeOut) && activeEvent.state == ActiveEventStates.Playing) 
				{
					activeEvent.state = ActiveEventStates.Stopping;
					activeEvent.emitter.FadeOut (activeEvent.volume, activeEvent.audioClip.length - activeEvent.elapsedPlayTime);
				}
			}

			if (activeEvent.elapsedPlayTime >= activeEvent.currentLength) // stop and clear
			{
				activeEvent.state = ActiveEventStates.Stopped;
				playingEvents.Remove (activeEvent); // remove active event from the list of playing events
				if (activeEvent.audioEvent.playMode == PlayMode.Continuos) 
				{
					AudioEvent audioEvent = activeEvent.audioEvent;
					ActiveEvent newActiveEvent = GetPooledActiveEvent (); 
					SetInitialActiveEventProperties (newActiveEvent, audioEvent, activeEvent.eventCaller);

					if (newActiveEvent.delayTime > 0)
					{ 
						initializedEvents.Add (newActiveEvent);
						delayedEvents.Add (newActiveEvent);
					} 
					else
					{
						AudioEmitter emitter = activeEvent.emitter; 
						StopEmitter (emitter); 
						ReturnAudioEmitter (emitter);
						initializedEvents.Add (newActiveEvent); 
						PlayEvent (newActiveEvent.eventName, audioEvent, newActiveEvent); 
					}
				} 

				StopActiveEvent (activeEvent);
			}
		}
	}

	private void UpdateVirtualPlayTimes ()
	{
		for (int i = 0; i < virtualEvents.Count; i++)
		{
			ActiveEvent activeEvent = virtualEvents[i];

			if (activeEvent.isPaused == false)
			{
				activeEvent.elapsedPlayTime += Time.deltaTime;
			}

			if (activeEvent.audioEvent.isLoop) 
			{
				if (activeEvent.elapsedPlayTime > activeEvent.audioClip.length) 
				{
					activeEvent.elapsedPlayTime = 0f;
				}
				continue;
			}

			float length = activeEvent.audioClip.length / Mathf.Abs (AudioUtil.StToLinear (activeEvent.pitch));
			if (activeEvent.elapsedPlayTime > length) 
			{
				virtualEvents.Remove (activeEvent);

				if (activeEvent.audioEvent.playMode == PlayMode.Continuos)  
				{
					AudioEvent audioEvent = activeEvent.audioEvent;
					ActiveEvent newActiveEvent = GetPooledActiveEvent (); 
					SetInitialActiveEventProperties (newActiveEvent, audioEvent, activeEvent.eventCaller);

					if (newActiveEvent.delayTime > 0)
					{
						initializedEvents.Add (newActiveEvent);
						delayedEvents.Add (newActiveEvent);
					}
					else
					{
						initializedEvents.Add (newActiveEvent);
						PlayEvent (newActiveEvent.eventName, audioEvent, newActiveEvent);
					}
				}

				StopActiveEvent (activeEvent);
			}
		}
	}

	#endregion

	#region Stop Events

	private void StopEvent (ActiveEvent activeEvent)
	{
		if (activeEvent.state == ActiveEventStates.Playing) 
		{
			playingEvents.Remove (activeEvent);
			AudioEmitter emitter = activeEvent.emitter;
			StopEmitter (emitter);
			ReturnAudioEmitter (emitter); 
		}
		StopActiveEvent (activeEvent);
	}

	private void StopActiveEvent (ActiveEvent activeEvent)
	{
		activeEvent.state = ActiveEventStates.Stopped; 
		initializedEvents.Remove (activeEvent);
		delayedEvents.Remove (activeEvent);
		virtualEvents.Remove (activeEvent);
		pausedEvents.Remove (activeEvent);

		if (activeEvent.emitter)
		{
			AudioEmitter emitter = activeEvent.emitter;
			StopEmitter (emitter); 
			ReturnAudioEmitter (emitter);
		}

		ReturnActiveEvent (activeEvent);
	}

	private void StopEmitter (AudioEmitter emitter)
	{
		if (emitter == null) return;
		emitter.activeEvent.emitter = null;
		emitter.gameObject.SetActive (false); 
		emitter.source.Stop ();
		emitter.activeEvent = null;
		emitter.updateEmitter = false;
	}

	#endregion

	#region Pausing/Unpausing Events

	private void PauseEvent (ActiveEvent activeEvent)
	{
		if (activeEvent.state == ActiveEventStates.Playing || activeEvent.state == ActiveEventStates.Stopping)
		{
			playingEvents.Remove (activeEvent);
			AudioEmitter emitter = activeEvent.emitter;
			StopEmitter (emitter);
			ReturnAudioEmitter (emitter); 
		}
		activeEvent.isPaused = true;
		activeEvent.state = ActiveEventStates.Paused;
		pausedEvents.Add (activeEvent);
	}

	private void UnpauseEvent (ActiveEvent activeEvent) 
	{
		activeEvent.isPaused = false;
		pausedEvents.Remove (activeEvent);
		activeEvent.state = ActiveEventStates.Virtual;
		virtualEvents.Add (activeEvent);
	}

	#endregion

	#region Virtualize / Devirtualize Events

	public void VirtualizeEvent (ActiveEvent activeEvent)
	{
		AudioEmitter emitter = activeEvent.emitter;
		StopEmitter (activeEvent.emitter); // stop the emitter
		ReturnAudioEmitter (emitter); // return to pool
		playingEvents.Remove (activeEvent); // remove this active event from the list of playing events

		if (activeEvent.audioEvent.canVirtualize) // virtualize this active event if required
		{
			activeEvent.state = ActiveEventStates.Virtual;
			virtualEvents.Add (activeEvent);
		} 
		else // otherwise this active event is killed
		{
			StopActiveEvent (activeEvent);
		}
	}

	private IEnumerator VirtualizeEmitter (ActiveEvent activeEvent)
	{
		AudioEmitter emitter = activeEvent.emitter;

		activeEvent.state = ActiveEventStates.Virtualizing;
		emitter.FadeOut (activeEvent.volume, 0.25f);
		yield return new WaitForSeconds (0.25f);
		if (activeEvent.state == ActiveEventStates.Virtualizing) 
		{
			StopEmitter (emitter);
			ReturnAudioEmitter (emitter);
			activeEvent.state = ActiveEventStates.Virtual;
		}
	}

	private void DeVirtualize (ActiveEvent activeEvent)
	{
		AudioEvent audioEvent = activeEvent.audioEvent;
		Vector3 position = activeEvent.eventCaller.transform.position + activeEvent.localPosition; 
		AudioEmitter emitter;
		int priority = 0;

		if (audioEvent.radiusInstanceLimit == GetInstancesInRadius (audioEvent.eventName, position, audioEvent.radius)) return; 
		if (audioEvent.localInstanceLimit == GetLocalInstances (audioEvent.eventName, activeEvent.eventCaller)) return; 
		if (audioEvent.globalInstanceLimit == GetGlobalInstances (audioEvent.eventName)) return; 

		emitter = GetPooledAudioEmitter(); 

		if (emitter == null) 
		{
			if (!audioEvent.staticPriority)
			{
				priority = GetScaledPriority(audioEvent, position, audioEvent.maxDist, audioEvent.priority);
			}
			else
			{
				priority = audioEvent.priority;
			}

			emitter = GetLowPriorityEmitter (priority); 
		}
		if (emitter == null) return;

		activeEvent.emitter = emitter;
		activeEvent.priority = priority;
		activeEvent.state = ActiveEventStates.Devirtualizing;
		PlayEvent (activeEvent, position);
	}

	private ActiveEvent GetHighestPriorityVirtualEvent ()
	{
		int highestPriority = 0;   
		int eventIndex = -1; 
		ActiveEvent virtualEvent = null;

		for (int i = virtualEvents.Count - 1; i >= 0; i--) // Get the highest priority virtual event
		{
			ActiveEvent activeEvent = virtualEvents [i];
			AudioEvent audioEvent = activeEvent.audioEvent; 
			Vector3 position = activeEvent.eventCaller.transform.position + activeEvent.localPosition; 

			if (Vector3.Distance (position, GetAttenuatorPosition(audioEvent)) <= audioEvent.maxDist) // if distance from listener is within range
			{
				int priority;

				if (audioEvent.staticPriority)
				{
					priority = audioEvent.priority;
				}
				else
				{
					priority = GetScaledPriority(audioEvent, position, audioEvent.maxDist, audioEvent.priority);
				}

				if (priority > highestPriority) 
				{ 
					highestPriority = priority; 
					eventIndex = i;
				}
			}
		}

		if (eventIndex > -1) 
		{
			virtualEvent = virtualEvents [eventIndex];
			return virtualEvent;
		} 
		else 
		{
			return null;
		}
	}

	#endregion

	#region Event Instance Counters

	private int GetGlobalInstances (string eventName)
	{
		int instanceCount = 0;

		for (int i = 0; i < playingEvents.Count; i++) 
		{
			ActiveEvent activeEvent = playingEvents[i];

			if (activeEvent.eventName == eventName) 
			{
				instanceCount++;
			}
		}

		return instanceCount;
	}

	private int GetLocalInstances (string eventName, GameObject eventCaller)
	{
		int instanceCount = 0;

		for (int i = 0; i < playingEvents.Count; i++) 
		{
			ActiveEvent activeEvent = playingEvents[i];

			if (activeEvent.eventName == eventName && activeEvent.eventCaller == eventCaller) 
			{
				instanceCount++;
			}
		}

		return instanceCount;
	}

	private int GetInstancesInRadius (string eventName, Vector3 position, float radius)
	{
		int instanceCount = 0;

		for (int i = 0; i < playingEvents.Count; i++) 
		{
			ActiveEvent activeEvent = playingEvents[i];
			AudioEmitter emitter = activeEvent.emitter;

			if (Vector3.Distance (activeEvent.eventCaller.transform.position+activeEvent.localPosition, position) <= radius && emitter.activeEvent.eventName == eventName) 
			{
				instanceCount++;
			}
		}

		return instanceCount;
	}

	#endregion

	#region Get/Return Emitters

	private AudioEmitter GetPooledAudioEmitter() 
	{
		AudioEmitter emitter; 
		int lastAvailableIndex = emitterPool.Count - 1;  

		if (lastAvailableIndex >= 0) 
		{
			emitter = emitterPool [lastAvailableIndex];  
			emitterPool.RemoveAt (lastAvailableIndex);  
			return emitter;
		} 

		return null; 
	}

	private void ReturnAudioEmitter (AudioEmitter emitter)
	{
		emitter.transform.SetParent (this.transform);
		emitter.gameObject.name = ("AudioEmitter"); 
		emitterPool.Add (emitter);
	}

	private AudioEmitter GetOldestEmitter (string eventName)
	{
		for (int i = 0; i < playingEvents.Count; i++)
		{
			ActiveEvent activeEvent = playingEvents[i];

			if (activeEvent.eventName == eventName) 
			{
				AudioEmitter emitter = KillAndGetEmitter (activeEvent); 
				return emitter; 
			}
		}

		return null;
	}

	private AudioEmitter GetOldestLocalEmitter (string eventName, GameObject eventCaller)
	{
		for (int i = 0; i < playingEvents.Count; i++)
		{
			ActiveEvent activeEvent = playingEvents[i];

			if (activeEvent.eventName == eventName && activeEvent.eventCaller == eventCaller) 
			{
				AudioEmitter emitter = KillAndGetEmitter (activeEvent); 
				return emitter; 
			}
		}

		return null;
	}

	private AudioEmitter GetLowPriorityEmitter(int priority)
	{
		int tempPriority = 101;
		int eventIndex = 0; 
		ActiveEvent activeEvent = null; 

		for (int i = 0; i < playingEvents.Count; i++) // get the lowest priority active event
		{
			activeEvent = playingEvents [i];

			if (activeEvent.priority < tempPriority) // if this active event's priority is lower than tempPriority...
			{ 
				tempPriority = activeEvent.priority; // this event's priority is now tempPriority...
				eventIndex = i; // and this emitter is now the oldest lowest priority emitter.
			}
		}

		activeEvent = playingEvents [eventIndex];
			
		if (activeEvent.priority < priority) // if the new Audio Event's audio emitter is less than the lowest priority emitter, stop and return.
		{
			AudioEmitter emitter = KillAndGetEmitter (activeEvent); 
			return emitter; // return the emitter!
		}

		return null; // return if no available emitters with a lower priority;
	}

	private AudioEmitter GetEqualPriorityEmitter (int priority)
	{
		for (int i = 0; i < playingEvents.Count; i++) 
		{
			ActiveEvent activeEvent = playingEvents [i]; 

			if (activeEvent.priority == priority) // check if this active event's priority is equal to the event that wants to play
			{
				AudioEmitter emitter = KillAndGetEmitter (activeEvent); 
				return emitter; // return the emitter.
			}
		}

		return null;
	}
		
	private AudioEmitter KillAndGetEmitter (ActiveEvent activeEvent) // kill event and get its emitter
	{
		AudioEmitter emitter = activeEvent.emitter;
		StopEmitter (activeEvent.emitter); // stop the emitter
		playingEvents.Remove (activeEvent); // remove this active event from the list of playing events
		if (activeEvent.audioEvent.canVirtualize) // virtualize this active event if required
		{
			activeEvent.state = ActiveEventStates.Virtual;
			virtualEvents.Add (activeEvent);
		} 
		else // otherwise this active event is killed
		{
			StopActiveEvent (activeEvent);
		}
		return (emitter);
	}

	#endregion

	#region Get/Return ActiveEvents

	private ActiveEvent GetPooledActiveEvent() 
	{
		ActiveEvent activeEvent; 
		int lastAvailableIndex = activeEventPool.Count - 1;  

		if (lastAvailableIndex >= 0) 
		{
			activeEvent = activeEventPool [lastAvailableIndex];  
			activeEventPool.RemoveAt (lastAvailableIndex);  
		} 
		else
		{
			activeEvent = ScriptableObject.CreateInstance<ActiveEvent>();
		}

		return activeEvent;
	}

	private void ReturnActiveEvent (ActiveEvent activeEvent)
	{
		activeEvent.state = ActiveEventStates.Initialize;
		activeEventPool.Add (activeEvent);
	}

	#endregion

	#region Utility

	private Vector3 GetRandomTransform (GameObject obj, float minDist, float maxDist)
	{
		float rot = Random.Range(1f, 360f);
		Vector3 direction = Quaternion.AngleAxis(rot, Vector3.up) * Vector3.forward;
		Ray ray = new Ray(obj.transform.position, direction);

		return ray.GetPoint(Random.Range(minDist, maxDist));
	}

	private AudioClip GetRandomClip(List<WeightedAudioClip> weightedClips, AudioEvent audioEvent)
	{
		int selectedClipIndex = audioEvent.lastClipIndex; 
		int lastRoll = audioEvent.lastRoll;
		int roll = 0;

		if (weightedClips.Count > 1) 
		{
			int totalWeight = 0;
			foreach(WeightedAudioClip clip in weightedClips) // add up the total weight
			{
				totalWeight += clip.weight; 
			}

			roll = Random.Range (0, totalWeight); // roll random number from 0 to total weight
			int sum = 0; // sum weights as we iterate over them

			if (audioEvent.dontRepeatLastClip) 
			{
				while (roll == lastRoll)
				{
					roll = Random.Range (0, totalWeight);
				}
			}

			for (int i = 0; i < weightedClips.Count; i++) 
			{
				sum += weightedClips[i].weight;
				if (sum > roll) 
				{
					selectedClipIndex = i; //If the added weight is now GREATER than what we rolled, we found our item, break
					break;
				}
			}
		}
		else  // if there is only one AudioClip the index is simply 0.
		{
			selectedClipIndex = 0;
		}

		audioEvent.lastRoll = roll;
		return weightedClips[selectedClipIndex].audioClip; 
	}

	private AudioClip GetNextClip(List<WeightedAudioClip> clips, AudioEvent audioEvent)
	{
		int lastClipIndex = audioEvent.lastClipIndex;
		int selectedClipIndex; 

		if (clips.Count > 1)  
		{
			if (lastClipIndex >= clips.Count-1) 
			{
				selectedClipIndex = 0;
			} 
			else 
			{
				selectedClipIndex = lastClipIndex + 1;
			}
		} 
		else 
		{
			selectedClipIndex = 0;
		}

		audioEvent.lastClipIndex = selectedClipIndex; 
		return clips[selectedClipIndex].audioClip;
	}

	private float GetRandomVolume (float volume, float randomVol)
	{
		float randomDb = volume + Random.Range (randomVol, 0f);
		return randomDb;
	}
		
	private float GetRandomPitch (float pitch, float randomPitch)
	{
		float randomSt = pitch + Random.Range (0f-randomPitch, randomPitch);
		return randomSt;
	}
		
	private int GetScaledPriority (AudioEvent audioEvent, Vector3 position, float maxDist, int priority)
	{
		float distance = Vector3.Distance (position, GetAttenuatorPosition(audioEvent)); // get distance between emitter and listener
		int scaledPriority = (int)(priority * (1-(distance / maxDist))); // scale priority to distance

		if (scaledPriority > 0) 
		{
			return scaledPriority; 
		} 
		else
		{
			return 0; // 0 is lowest priority
		}								
	}

	public Vector3 GetAttenuatorPosition (AudioEvent audioEvent) 
	{
		Vector3 attenuatorPosition = Vector3.zero; 
		if (perspective == Perspective.FirstPerson || audioEvent.ignoreThirdPersonMode) 
		{
			attenuatorPosition = listenerObj.transform.position;
		} 
		else if (perspective == Perspective.ThirdPerson && audioEvent.ignoreThirdPersonMode == false) 
		{
			attenuatorPosition = attenuationObj.transform.position;
		}
		return attenuatorPosition;
	}
		
	#endregion
}

public enum Perspective 
{
	FirstPerson,
	ThirdPerson
}