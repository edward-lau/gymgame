﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class ActiveEvent : ScriptableObject
{
	public AudioEvent audioEvent; // the audio event this active event is playing
    public GameObject eventCaller; // the parent gameobject of the audio event
	public AudioEmitter emitter; // the emitter used to play this event
	public AudioClip audioClip; // audio clip set when played
	public Vector3 localPosition; // local transform relative to the event caller

	public string eventName; // event name
	public int priority; // event priority (scaled if necessary)
	public float delayTime; // delay time set when played

	public float volume; // actual volume value
	public float baseVol; // volume set when played (after randomization if necessary)
	public float baseEventVol; // event volume when played
	public float targetVol; 

	public float pitch;
	public float basePitch;
	public float baseEventPitch;
	public float targetPitch;

	public float fadeInTarget;
	public float fadeVol;

	public bool isPaused;

    public float elapsedPlayTime;
	public float elapsedPlayRatio;
    public float elapsedDelayTime;

	public float currentLength;
	public float oldLength;

	public ActiveEventStates state; 

    /// <summary>
    /// Play this 
    /// </summary>
    public void Play ()
    {
        if (emitter)
        {
            emitter.source.Play();
        }
    }

    /// <summary>
    /// Pause this 
    /// </summary>
    public void Pause () 
    {
        isPaused = true;
    }

    /// <summary>
    /// Unpause this 
    /// </summary>
    public void UnPause ()
    {
        isPaused = false;
    }

    /// <summary>
    /// Stop and clear this 
    /// </summary>
    public void Stop ()
    {

	}
		
//	IEnumerator UpdateActiveEvent () 
//	{
//		while (true) 
//		{
//			UpdateRtpcValues (); 
//
//			targetVol = fadeVol + (audioEvent.volume - baseEventVol) + rtpcVol;  
//			if (volume != targetVol) // prevents unecessary calculations
//			{
//				volume = AudioUtil.DbToLinear (targetVol);
//				volume = targetVol;
//			}
//
//			targetPitch = basePitch + (audioEvent.pitch - baseEventPitch) + rtpcPitch;
//			if (pitch != targetPitch) // prevents unecessary calculations
//			{
//				pitch = AudioUtil.StToLinear (targetPitch);
//				pitch = targetPitch;
//			}
//
//			lpFilter.cutoffFrequency = audioEvent.lpCutoff;
//			hpFilter.cutoffFrequency = audioEvent.hpCutoff;
//
//			yield return new WaitForSeconds (0f);
//		}
//	}

//	private void UpdateRtpcValues () 
//	{
//		float tempVol = 0f;
//		float tempPitch = 0f;
//
//		for (int i = 0; i < audioEvent.rtpcSettings.Count; i++)
//		{
//			RtpcSetting rtpc = audioEvent.rtpcSettings [i];
//
//			if (rtpc.scope == RtpcScope.global) 
//			{
//				if (rtpc.property == RtpcProperty.volume) tempVol += rtpc.curve.Evaluate (rtpc.gameParam.globalValue);
//			}
//		}
//	}

}

public enum ActiveEventStates 
{ 
	Inactive,
	Initialize, 
	ToPlay,
	Devirtualizing,
	Playing,
	Virtualizing,
	Virtual,
	Pausing,
	Paused,
	Stopping,
	Stopped
}