﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
//using System;

[CustomEditor(typeof(AudioEvent))] 
public class AudioEventEditor : Editor 
{
	AudioEvent myTarget;
	public static bool viewEvents = true;
	public static bool viewAudio = true;
	public static bool viewSpatialization = true;
	public static bool viewPlayback = true;
	public static bool viewRtpc = true;
	public static bool viewClips = true;
	private Rect curveRect = Rect.MinMaxRect(0f, 0f, 1f, 1f);
//	private Rect rtpcCurveRect = Rect.MinMaxRect(0f, -1f, 1f, 1f);
	private AnimationCurve defaultRtpc = AnimationCurve.Linear (0f, 0f, 1f, 0f);
	private GUILayoutOption miniButtonWidth = GUILayout.Width(20f); 

	public override void OnInspectorGUI ()
	{
		myTarget = target as AudioEvent;
		SerializedProperty eventName = serializedObject.FindProperty ("eventName");
		SerializedProperty globalInstanceLimit = serializedObject.FindProperty ("globalInstanceLimit");
		SerializedProperty localInstanceLimit = serializedObject.FindProperty ("localInstanceLimit");
		SerializedProperty radiusInstanceLimit = serializedObject.FindProperty ("radiusInstanceLimit");
		SerializedProperty radius = serializedObject.FindProperty ("radius");
		SerializedProperty priority = serializedObject.FindProperty ("priority");
		SerializedProperty staticPriority = serializedObject.FindProperty ("staticPriority");
		SerializedProperty discardOldest = serializedObject.FindProperty ("discardOldest");
		SerializedProperty canVirtualize = serializedObject.FindProperty ("canVirtualize");
		SerializedProperty volume = serializedObject.FindProperty ("volume");
		SerializedProperty randomVolume = serializedObject.FindProperty ("randomVolume");
		SerializedProperty pitch = serializedObject.FindProperty ("pitch");
		SerializedProperty randomPitch = serializedObject.FindProperty ("randomPitch");
		SerializedProperty lpCutoff = serializedObject.FindProperty ("lpCutoff");
		SerializedProperty hpCutoff = serializedObject.FindProperty ("hpCutoff");
		SerializedProperty repeatSound = serializedObject.FindProperty ("repeatSound");
		SerializedProperty dontDelayFirstPlay = serializedObject.FindProperty ("dontDelayFirstPlay");
		SerializedProperty spatializeSound = serializedObject.FindProperty ("spatializeSound");
		SerializedProperty ignoreThirdPersonMode = serializedObject.FindProperty ("ignoreThirdPersonMode");
		SerializedProperty panning = serializedObject.FindProperty ("panning");
		SerializedProperty rolloffMode = serializedObject.FindProperty ("rolloffMode");
		SerializedProperty rolloffCurve = serializedObject.FindProperty ("rolloffCurve");
		SerializedProperty lpfCurve = serializedObject.FindProperty ("lpfCurve");
		SerializedProperty minDist = serializedObject.FindProperty ("minDist");
		SerializedProperty maxDist = serializedObject.FindProperty ("maxDist");
		SerializedProperty occludable = serializedObject.FindProperty ("occludable");
		SerializedProperty randomPosition = serializedObject.FindProperty ("randomPosition");
		SerializedProperty minPosition = serializedObject.FindProperty ("minPosition");
		SerializedProperty maxPosition = serializedObject.FindProperty ("maxPosition");
		SerializedProperty mixerGroup = serializedObject.FindProperty ("mixerGroup");
		SerializedProperty playType = serializedObject.FindProperty ("playType");
		SerializedProperty dontRepeatLastClip = serializedObject.FindProperty ("dontRepeatLastClip");
		SerializedProperty probability = serializedObject.FindProperty ("probability");
		SerializedProperty delay = serializedObject.FindProperty ("delayTime");
		SerializedProperty randomDelay = serializedObject.FindProperty ("randomDelay");
		SerializedProperty fadeIn = serializedObject.FindProperty ("fadeIn");
		SerializedProperty fadeOut = serializedObject.FindProperty ("fadeOut");
		SerializedProperty playMode = serializedObject.FindProperty ("playMode");
		SerializedProperty isLoop = serializedObject.FindProperty ("isLoop");
		SerializedProperty weightedClips = serializedObject.FindProperty ("weightedClips");
		SerializedProperty rtpcSettings = serializedObject.FindProperty ("rtpcSettings");

		serializedObject.Update ();

		EditorGUI.indentLevel++;

		EditorGUILayout.BeginVertical ("box");
		EditorGUILayout.Space();
		viewEvents = EditorGUILayout.Foldout (viewEvents, "Event Settings");
		if (viewEvents) 
		{
			eventName.stringValue = EditorGUILayout.TextField (new GUIContent ("Event Name"), eventName.stringValue);
			EditorGUILayout.Space();
			EditorGUILayout.IntSlider (priority, 1, 100, new GUIContent ("Priority"));
			staticPriority.boolValue = EditorGUILayout.Toggle (new GUIContent ("Static Priority"), staticPriority.boolValue);
			EditorGUILayout.Space();
			EditorGUILayout.IntSlider (globalInstanceLimit, 1, 32, new GUIContent ("Global Limit"));
			EditorGUILayout.IntSlider (localInstanceLimit, 1, 32, new GUIContent ("Local Limit"));
			EditorGUILayout.IntSlider (radiusInstanceLimit, 1, 32, new GUIContent ("Radius Limit"));
			EditorGUILayout.Slider (radius, 0f, 500f, new GUIContent ("Radius Size"));
			EditorGUILayout.Space();
			discardOldest.boolValue = EditorGUILayout.Toggle (new GUIContent ("Discard Oldest"), discardOldest.boolValue);
			EditorGUILayout.PropertyField (canVirtualize, new GUIContent ("Can Virtualize"));
		}
		EditorGUILayout.Space ();
		EditorGUILayout.EndVertical ();

		EditorGUILayout.BeginVertical("box");
		EditorGUILayout.Space();
		viewAudio = EditorGUILayout.Foldout (viewAudio, "Audio Settings");
		if (viewAudio) 
		{
			EditorGUILayout.Slider (volume, -46f, 0f, new GUIContent ("Volume (dB)"));
			EditorGUILayout.Slider (randomVolume, -24f, 0f, new GUIContent ("Random Volume (-)"));
			EditorGUILayout.Space ();
			EditorGUILayout.Slider (pitch, -24f, 24f, new GUIContent ("Pitch (st)"));
			EditorGUILayout.Slider (randomPitch, 0f, 24f, new GUIContent ("Random Pitch (+/-)"));
			EditorGUILayout.Space ();
			EditorGUILayout.Slider (lpCutoff, 20f, 22000f, new GUIContent ("Low Pass Cutoff"));
			EditorGUILayout.Slider (hpCutoff, 20f, 22000f, new GUIContent ("High Pass Cutoff"));
		}
		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical("box");
		EditorGUILayout.Space();
		viewSpatialization = EditorGUILayout.Foldout (viewSpatialization, "Spatialization Settings");
		if (viewSpatialization) 
		{
			spatializeSound.boolValue = EditorGUILayout.Toggle (new GUIContent ("Spatialize"), spatializeSound.boolValue);
			if (spatializeSound.boolValue)  
			{
				EditorGUILayout.Space ();
				ignoreThirdPersonMode.boolValue = EditorGUILayout.Toggle (new GUIContent ("Ignore 3rd Person Mode"), ignoreThirdPersonMode.boolValue);
				EditorGUILayout.PropertyField (rolloffMode, new GUIContent ("Volume Rolloff"));
				EditorGUILayout.CurveField (rolloffCurve, Color.red, curveRect, new GUIContent ("Custom Rolloff"));
				EditorGUILayout.CurveField (lpfCurve, Color.magenta, curveRect, new GUIContent ("Low Pass Rolloff"));
				EditorGUILayout.Slider (minDist, 1f, 500f, new GUIContent ("Min Distance"));
				EditorGUILayout.Slider (maxDist, 1f, 500f, new GUIContent ("Max Distance"));
				EditorGUILayout.Space ();
				randomPosition.boolValue = EditorGUILayout.Toggle (new GUIContent ("Random Position"), randomPosition.boolValue);
				if (randomPosition.boolValue) 
				{
					EditorGUILayout.Slider (minPosition, 0f, 250f, new GUIContent ("Min Position"));
					EditorGUILayout.Slider (maxPosition, 0f, 250f, new GUIContent ("Max Position"));
					EditorGUILayout.Space ();
				}
				occludable.boolValue = EditorGUILayout.Toggle (new GUIContent ("Occludable"), occludable.boolValue);
			} 
			else 
			{
				EditorGUILayout.Slider (panning, -1f, 1f, new GUIContent ("Stereo Pan"));
			}
		}
		EditorGUILayout.Space ();
		EditorGUILayout.EndVertical();

		EditorGUILayout.BeginVertical("box");
		EditorGUILayout.Space();
		viewPlayback = EditorGUILayout.Foldout (viewPlayback, "Playback Settings");
		if (viewPlayback) 
		{
			EditorGUILayout.PropertyField (mixerGroup, new GUIContent ("Mixer Group"));
			EditorGUILayout.PropertyField (playType, new GUIContent ("Play Type"));
			dontRepeatLastClip.boolValue = EditorGUILayout.Toggle (new GUIContent ("Don't Repeat Last"), dontRepeatLastClip.boolValue);
			EditorGUILayout.Slider (probability, 0f, 100f, new GUIContent ("Probability (%)"));
			EditorGUILayout.Space ();
			EditorGUILayout.Slider (delay, 0f, 15f, new GUIContent ("Delay (sec)"));
			EditorGUILayout.Slider (randomDelay, 0f, 15f, new GUIContent ("Random Delay (+/-)"));
			EditorGUILayout.Space ();
			EditorGUILayout.Slider (fadeIn, 0f, 15f, new GUIContent ("Fade In (sec)"));
			EditorGUILayout.Slider (fadeOut, 0f, 15f, new GUIContent ("Fade Out (sec)"));
			EditorGUILayout.Space ();
			EditorGUILayout.PropertyField (playMode, new GUIContent ("Play Mode"));

			if (playMode.enumValueIndex == 1) 
			{
				dontDelayFirstPlay.boolValue = EditorGUILayout.Toggle (new GUIContent ("Don't Delay First"), dontDelayFirstPlay.boolValue); 
				isLoop.boolValue = EditorGUILayout.Toggle (new GUIContent ("Loop"), isLoop.boolValue);
			}
			else 
			{
				dontDelayFirstPlay.boolValue = false;
				isLoop.boolValue = false;
			}
		}
		EditorGUILayout.Space ();
		EditorGUILayout.EndVertical ();

		EditorGUILayout.BeginVertical("box");
		EditorGUILayout.Space();

		viewRtpc = EditorGUILayout.Foldout (viewRtpc, "RTPC Settings");
		if (viewRtpc) 
		{
			List<string> paramList = new List<string>(); // list that stores GameParameter names

			if (AudioManager.instance.gameParameters.Count > 0) 
			{
				for (int i = 0; i < AudioManager.instance.gameParameters.Count; i++) 
				{ // populate string list with parameter names
					GameParameter param = AudioManager.instance.gameParameters [i];
					paramList.Add (param.paramName);
				}
			} 
			else 
			{
				paramList.Add ("No Parameters");
			}

			string[] gameParams = paramList.ToArray (); // convert list of parameter names to array (now usable by Popup)

			if (myTarget && myTarget.rtpcSettings.Count > 0) 
			{
				for (int i = 0; i < myTarget.rtpcSettings.Count; i++) 
				{
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Property", GUILayout.MinWidth (50));
					EditorGUILayout.LabelField ("Scope", GUILayout.MinWidth (50)); 
					GUILayout.Space (40);
					EditorGUILayout.EndHorizontal ();

					EditorGUILayout.BeginHorizontal ();
					RtpcSetting rtpc = myTarget.rtpcSettings [i];
					SerializedObject serializedRtpc = new SerializedObject (rtpc);
					serializedRtpc.Update (); 
						
					SerializedProperty property = serializedRtpc.FindProperty ("property"); 
					SerializedProperty scopeRef = serializedRtpc.FindProperty ("scope"); 
					SerializedProperty lastProperty = serializedRtpc.FindProperty ("lastProperty"); 
					SerializedProperty curveRef = serializedRtpc.FindProperty ("curve"); 
					SerializedProperty gameParam = serializedRtpc.FindProperty ("gameParam"); 

					EditorGUILayout.PropertyField (property, GUIContent.none);
					EditorGUILayout.PropertyField (scopeRef, GUIContent.none);

					if (GUILayout.Button ("\u21b4", EditorStyles.miniButtonLeft, miniButtonWidth)) 
					{
						rtpcSettings.MoveArrayElement (i, i + 1);
					}

					if (GUILayout.Button ("-", EditorStyles.miniButtonRight, miniButtonWidth)) 
					{
						int oldSize = rtpcSettings.arraySize;
						rtpcSettings.DeleteArrayElementAtIndex (i);
						if (rtpcSettings.arraySize == oldSize)
						{
							rtpcSettings.DeleteArrayElementAtIndex (i);
						}
					}
					EditorGUILayout.EndHorizontal ();
										 
					Rect rtpcCurveRect = Rect.MinMaxRect(0f, 0f, 1f, 1f);

					if (rtpc.property == RtpcProperty.volume)
					{
						rtpcCurveRect = Rect.MinMaxRect(0f, -46f, 1f, 46f);
					}

					if (rtpc.property == RtpcProperty.pitch)
					{
						rtpcCurveRect = Rect.MinMaxRect(0f, -24f, 1f, 24f);
					}

					if (rtpc.property == RtpcProperty.lowPassCutoff)
					{
						rtpcCurveRect = Rect.MinMaxRect(0f, -20000f, 1f, 20000f);
					}

					if (rtpc.property == RtpcProperty.highPassCutoff)
					{
						rtpcCurveRect = Rect.MinMaxRect(0f, -20000f, 1f, 20000f);
					}

					if (rtpc.property != rtpc.lastProperty) 
					{
						ScaleCurveKeys (lastProperty, property, curveRef, rtpc.lastProperty, rtpc.property);
					}
						
					EditorGUILayout.CurveField (curveRef, Color.green, rtpcCurveRect, GUIContent.none); 

					int paramIndex = 0;

					if (AudioManager.instance.gameParameters.Count > 0)
					{
						paramIndex = AudioManager.instance.gameParameters.IndexOf (rtpc.gameParam); // get the index of the rtpc's game parameter
					}

					paramIndex = EditorGUILayout.Popup("Game Parameter", paramIndex, gameParams); // popup now displays the correct name
					if (paramIndex < 0)
						paramIndex = 0;

					if (AudioManager.instance.gameParameters.Count > 0) 
					{
						gameParam.objectReferenceValue = AudioManager.instance.gameParameters [paramIndex]; 
					}

					serializedRtpc.ApplyModifiedProperties ();
				}
			}

			if (GUILayout.Button ("Add RTPC setting")) 
			{
				rtpcSettings.arraySize++; 
				RtpcSetting rtpc = new RtpcSetting();
				rtpcSettings.GetArrayElementAtIndex(rtpcSettings.arraySize - 1).objectReferenceValue = rtpc;
			}
		}

//		EditorGUILayout.PropertyField (rtpcSettings, true);

		EditorGUILayout.Space ();
		EditorGUILayout.EndVertical ();

		EditorGUILayout.BeginVertical("box");
		EditorGUILayout.Space();

		viewClips = EditorGUILayout.Foldout(viewClips, "Audio Clips");
		if (viewClips)
		{

			if (myTarget.weightedClips.Count > 0) {
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Clips", GUILayout.MinWidth (120));
				EditorGUILayout.LabelField ("Weight", GUILayout.MaxWidth (55)); 
				GUILayout.Space (40);
				EditorGUILayout.EndHorizontal (); 

				for (int i = 0; i < myTarget.weightedClips.Count; i++) 
				{
					EditorGUILayout.BeginHorizontal ();

					WeightedAudioClip clip = myTarget.weightedClips [i];
					SerializedObject serializedObj = new SerializedObject (clip);
					serializedObj.Update (); 

					SerializedProperty clipRef = serializedObj.FindProperty ("audioClip"); 
					SerializedProperty weightRef = serializedObj.FindProperty ("weight"); 

					EditorGUILayout.PropertyField (clipRef, GUIContent.none, GUILayout.MinWidth (120));
					EditorGUILayout.PropertyField (weightRef, GUIContent.none, GUILayout.MaxWidth (50));

					if (GUILayout.Button ("\u21b4", EditorStyles.miniButtonLeft, miniButtonWidth)) 
					{
						weightedClips.MoveArrayElement (i, i + 1);
					}

					if (GUILayout.Button ("-", EditorStyles.miniButtonRight, miniButtonWidth)) 
					{
						int oldSize = weightedClips.arraySize;
						weightedClips.DeleteArrayElementAtIndex (i);
						if (weightedClips.arraySize == oldSize)
						{
							weightedClips.DeleteArrayElementAtIndex (i);
						}
					}

					serializedObj.ApplyModifiedProperties ();
					EditorGUILayout.EndHorizontal ();
				}
			} 
			else 
			{
				EditorGUILayout.LabelField ("Add AudioClips by dragging them here");
			}
		}

//		EditorGUILayout.PropertyField (weightedClips, true);
			
		var e = Event.current.type;

		if (e == EventType.DragUpdated) 
		{
			DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
		}
		else if (e == EventType.DragPerform)
		{
			DragAndDrop.AcceptDrag ();

			foreach (UnityEngine.Object draggedObject in DragAndDrop.objectReferences)  
			{
				if (draggedObject is AudioClip) 
				{
					weightedClips.arraySize++; 
					WeightedAudioClip wClip = new WeightedAudioClip (draggedObject as AudioClip, 1); 
					weightedClips.GetArrayElementAtIndex(weightedClips.arraySize - 1).objectReferenceValue = wClip;
				}
			}
		}

		EditorGUILayout.Space ();
		EditorGUILayout.EndVertical();
		
		serializedObject.ApplyModifiedProperties ();
	}

	private void ScaleCurveKeys (SerializedProperty lastProp, SerializedProperty newProp, SerializedProperty curve, RtpcProperty oldProperty, RtpcProperty newProperty) 
	{
		float oldYMax = 0;
		float oldYMin = 0;
		float newYMax = 0;
		float newYMin = 0; 

		if (oldProperty == RtpcProperty.volume) 
		{
			oldYMax = 46f;
			oldYMin = -46f;
		}

		if (oldProperty == RtpcProperty.pitch) 
		{
			oldYMax = 24f;
			oldYMin = -24f;
		}

		if (oldProperty == RtpcProperty.lowPassCutoff || oldProperty == RtpcProperty.highPassCutoff)
		{
			oldYMax = 20000f;
			oldYMin = -20000f;
		}

		if (newProperty == RtpcProperty.volume) 
		{
			newYMax = 46f;
			newYMin = -46f;
		}

		if (newProperty == RtpcProperty.pitch) 
		{
			newYMax = 24f;
			newYMin = -24f;
		}

		if (newProperty == RtpcProperty.lowPassCutoff || newProperty == RtpcProperty.highPassCutoff)
		{
			newYMax = 20000f;
			newYMin = -20000f;
		}

		float oldYRange = (oldYMax - oldYMin); 
		float newYRange = (newYMax - newYMin);  

		AnimationCurve newCurve;
		Keyframe[] newKeys;

		newKeys = new Keyframe[curve.animationCurveValue.keys.Length];

		for (int i = 0; i < curve.animationCurveValue.keys.Length; i++) 
		{
			float oldYValue = curve.animationCurveValue.keys [i].value;
			float oldXValue = curve.animationCurveValue.keys [i].time;
			float oldInTangent = curve.animationCurveValue.keys [i].inTangent;
			float oldOutTangent = curve.animationCurveValue.keys [i].outTangent; 

			float newYValue = (((oldYValue - oldYMin) * newYRange) / oldYRange) + newYMin;
			float newInTangent = (((oldInTangent - oldYMin) * newYRange) / oldYRange) + newYMin;
			float newOutTangent = (((oldOutTangent - oldYMin) * newYRange) / oldYRange) + newYMin;

			newKeys [i] = new Keyframe(oldXValue, newYValue, newInTangent, newOutTangent); 
		}
		curve.animationCurveValue = new AnimationCurve (newKeys);

		lastProp.enumValueIndex = newProp.enumValueIndex;
	}
}