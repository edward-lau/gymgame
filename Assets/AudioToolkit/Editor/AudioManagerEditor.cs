﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(AudioManager))] 
public class AudioManagerEditor : Editor
{
	AudioManager myTarget;
	public static bool viewParams = true;
	private static GUILayoutOption miniButtonWidth = GUILayout.Width(20f);

	public override void OnInspectorGUI ()
	{
		myTarget = target as AudioManager;

		DrawDefaultInspector ();

		SerializedProperty activeVoices = serializedObject.FindProperty ("activeVoices");
		SerializedProperty virtualVoices = serializedObject.FindProperty ("virtualVoices");
		SerializedProperty gameParameters = serializedObject.FindProperty ("gameParameters");

		serializedObject.Update ();

		EditorGUI.indentLevel++;
		EditorGUILayout.BeginVertical ("box");

		EditorGUILayout.IntSlider (activeVoices, 1, 128, new GUIContent ("Active Voices"));
		EditorGUILayout.IntSlider (virtualVoices, 1, 1024, new GUIContent ("Virtual Voices"));

		EditorGUILayout.EndVertical ();
		EditorGUILayout.BeginVertical ("box");

		viewParams = EditorGUILayout.Foldout (viewParams, "Game Parameters");
		if (viewParams) 
		{
			if (myTarget.gameParameters.Count > 0) 
			{
				for (int i = 0; i < myTarget.gameParameters.Count; i++) 
				{
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Name");
					EditorGUILayout.EndHorizontal ();

					EditorGUILayout.BeginHorizontal ();
					GameParameter param = myTarget.gameParameters [i];
					SerializedObject serializedParam = new SerializedObject (param);
					serializedParam.Update (); 

					SerializedProperty nameRef = serializedParam.FindProperty ("paramName"); 
					SerializedProperty minValueRef = serializedParam.FindProperty ("minValue"); 
					SerializedProperty maxValueRef = serializedParam.FindProperty ("maxValue");
					SerializedProperty defaultValueRef = serializedParam.FindProperty ("defaultValue"); 

					EditorGUILayout.PropertyField (nameRef, GUIContent.none);
					EditorGUILayout.EndHorizontal ();

					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Min", GUILayout.MinWidth (50));
					EditorGUILayout.LabelField ("Max", GUILayout.MinWidth (50)); 
					EditorGUILayout.LabelField ("Default", GUILayout.MinWidth (50));
					GUILayout.Space (45);
					EditorGUILayout.EndHorizontal ();

					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.PropertyField (minValueRef, GUIContent.none, GUILayout.MinWidth (50));
					EditorGUILayout.PropertyField (maxValueRef, GUIContent.none, GUILayout.MinWidth (50));
					EditorGUILayout.PropertyField (defaultValueRef, GUIContent.none, GUILayout.MinWidth (50));


					if (GUILayout.Button ("\u21b4", EditorStyles.miniButtonLeft, miniButtonWidth)) 
					{
						gameParameters.MoveArrayElement (i, i + 1);
					}

					if (GUILayout.Button ("-", EditorStyles.miniButtonRight, miniButtonWidth)) 
					{
						int oldSize = gameParameters.arraySize;
						gameParameters.DeleteArrayElementAtIndex (i);
						if (gameParameters.arraySize == oldSize)
						{
							gameParameters.DeleteArrayElementAtIndex (i);
						}
					}

					EditorGUILayout.EndHorizontal ();

					serializedParam.ApplyModifiedProperties ();
				}
			}

			if (GUILayout.Button ("Add Game Parameter")) 
			{
				gameParameters.arraySize++; 
				GameParameter param = new GameParameter();
				gameParameters.GetArrayElementAtIndex(gameParameters.arraySize - 1).objectReferenceValue = param;
			}
		}

		EditorGUILayout.EndVertical();
		serializedObject.ApplyModifiedProperties();
	}
}
