﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(AudioGroup))]
public class AudioGroupEditor : Editor
{
	AudioGroup myTarget;

	public override void OnInspectorGUI ()
	{
		myTarget = target as AudioGroup;
		DrawDefaultInspector ();

	}
}
