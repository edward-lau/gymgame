﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RtpcSetting : ScriptableObject 
{
	public GameParameter gameParam;
	public RtpcScope scope;
	public RtpcProperty property;
	public RtpcProperty lastProperty;
	public AnimationCurve curve = AnimationCurve.Linear (0f, 0f, 1f, 0f);
}
 
public enum RtpcScope
{
	global,
	gameObject  
}

public enum RtpcProperty
{
	volume,
	pitch,
	lowPassCutoff,
	highPassCutoff
}