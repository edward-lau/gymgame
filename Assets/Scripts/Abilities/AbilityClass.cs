﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject {

	public string aName = "New Ability";
	public KeyCode triggerKey; 
	public float coolDownTime = 1f;
	public bool active;
	public int requiredStat;
	protected float coolDownCounter = 0f;

	public abstract void Initialize();
	public abstract void TriggerAbility();
	public abstract void CallUpdate();

	public void CheckForTrigger()
	{
		if (Input.GetKeyDown(triggerKey) && coolDownCounter <= 0 && active == false && PlayerController.Instance.isStunned == false) 
		{
			TriggerAbility ();
		}
	}
}
