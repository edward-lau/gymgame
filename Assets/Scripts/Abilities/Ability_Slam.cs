﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

[CreateAssetMenu (menuName = "Abilities/Ability_Slam")]
public class Ability_Slam : Ability
{
	public float abilityExp;
	public float slamTime;
	public float slamCounter;

	private GameObject slamObj;
	private GameObject slamCollider; 
	private AbilityUi abilityUi; 
	private GameObject lockedText;

	public override void Initialize() 
	{
		active = false;
		coolDownCounter = 0f;
		abilityExp = 0f;

		slamObj = GameObject.Find ("Slam"); 
		slamCollider = GameObject.Find ("SlamCollider");
		slamObj.transform.SetParent (null);
		slamObj.SetActive (false);

		abilityUi = GameObject.Find("UI_Ability_Slam").GetComponent<AbilityUi>();
		lockedText = GameObject.Find ("LockedText_Slam");
		abilityUi.coolDownTime = coolDownTime;
		abilityUi.coolDownCounter = 0f; 
	}

	public override void TriggerAbility()
	{
		if (PlayerController.Instance.isInBench == true || PlayerController.Instance.strengthStat < requiredStat) return;
		PlayerController.Instance.anim.Play ("Attack_Slam");
		PlayWhooshSound ();
	}

	public void Slam ()
	{
		slamObj.SetActive (false);
		slamObj.transform.position = PlayerController.Instance.transform.position;
		slamCollider.SetActive (true);
		slamCounter = slamTime;
		active = true;
		PlayerController.Instance.StopMoving ();
		PlayerController.Instance.attackTimeCounter = slamTime;
		PlayerController.Instance.attacking = true;
		ProCamera2DShake.Instance.Shake ("SmallExplosion");
		slamObj.SetActive (true);
		PlayImpactSound ();
	}

	public override void CallUpdate() 
	{
		CheckForTrigger ();

		if (active) 
		{
			slamCounter -= Time.deltaTime;
		}

		if (coolDownCounter > 0) 
		{
			coolDownCounter -= Time.deltaTime;
			abilityUi.coolDownCounter = coolDownCounter;
		}

		if (slamCounter <= 0 && active) 
		{
			active = false;
			slamCollider.SetActive (false);
			coolDownCounter = coolDownTime;
			PlayerController.Instance.attacking = false;
			abilityUi.coolDownCounter = 0f;
		}

		if (PlayerController.Instance.strengthStat < requiredStat) 
		{
			abilityUi.gameObject.SetActive (false);
		} 
		else
		{
			abilityUi.gameObject.SetActive (true);
			lockedText.SetActive (false);
		}
	}

	private void PlayWhooshSound()
	{
		AudioManager.instance.PlayEvent ("Ability_Slam_Whoosh", slamObj);
	}

	private void PlayImpactSound()
	{
		AudioManager.instance.PlayEvent ("Ability_Slam_Impact", slamObj);
	}
}