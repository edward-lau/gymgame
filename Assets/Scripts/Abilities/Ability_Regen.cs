﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Abilities/Ability_Regen")]
public class Ability_Regen : Ability 
{
	public float abilityExp;
	public float regenRate;
	public float regenTime;
	public float regenCounter;
	public float flashTime;
	float flashTimeCounter;
	int flasher;

	private AbilityUi abilityUi;  
	private GameObject lockedText;
	private GameObject regenSprites;

	public override void Initialize() 
	{
		active = false;
		coolDownCounter = 0f;
		abilityExp = 0f;

		abilityUi = GameObject.Find("UI_Ability_Regen").GetComponent<AbilityUi>();
		lockedText = GameObject.Find ("LockedText_Regen");
		regenSprites = GameObject.Find ("RegenSprites");
		regenSprites.SetActive (false); 
		abilityUi.coolDownTime = coolDownTime;
		abilityUi.coolDownCounter = 0f; 
	}

	public override void TriggerAbility()
	{
		if (PlayerController.Instance.isInBench == true || PlayerController.Instance.enduranceStat < requiredStat) return;
		coolDownCounter = coolDownTime; 
		abilityUi.coolDownCounter = coolDownCounter;
		active = true;
		regenCounter = regenTime;
		regenSprites.SetActive (true);
		PlayRegenActivateSound ();
	}

	public override void CallUpdate() {

		CheckForTrigger ();

		if (active) 
		{
			regenCounter -= Time.deltaTime;
			PlayerController.Instance.TakeDamage (-regenRate * Time.deltaTime);
//			FlashPlayer ();
		}
			
		if (coolDownCounter > 0 && active == false) 
		{
			coolDownCounter -= Time.deltaTime;
			abilityUi.coolDownCounter = coolDownCounter;
		}

		if (regenCounter <= 0 && active) 
		{
			active = false;
			PlayerController.Instance.SetNormalSprite ();
			regenSprites.SetActive (false);
		}

		if (coolDownCounter <= 0) 
		{
			abilityUi.coolDownCounter = 0f;
		}

		if (PlayerController.Instance.enduranceStat < requiredStat) 
		{
			abilityUi.gameObject.SetActive (false);
		} 
		else
		{
			abilityUi.gameObject.SetActive (true);
			lockedText.SetActive (false);
		}
	}

	private void PlayRegenActivateSound ()
	{
		AudioManager.instance.PlayEvent ("Ability_Regen", PlayerController.Instance.gameObject);
	}

//	public void FlashPlayer() 
//	{
//		if (PlayerController.Instance.isStunned == false)
//		{
//			if (flashTimeCounter > 0) 
//			{
//				flashTimeCounter -= Time.deltaTime;
//			} 
//			else 
//			{
//				flashTimeCounter = flashTime;
//				flasher++;
//				switch (flasher) 
//				{
//				case 1:
//					PlayerController.Instance.SetNormalSprite ();
//					break;
//				case 2: 
//					PlayerController.Instance.SetColorSprite (new Color (0.38f, 0.76f, 0.44f));
//					flasher = 0;
//					break;
//				}
//			}
//		}
//	}
}
