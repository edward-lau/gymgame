﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Abilities/Ability_Dash")]
public class Ability_Dash : Ability
{
	public float abilityExp;
	public float dashSpeed;
	public float dashTime;
	public float diagonalSpeedMod;
	private float currentDashSpeed;

	private float dashCounter;

	private Rigidbody2D myRigidBody;
	private Vector3 savedVelocity;
	private AbilityUi abilityUi;
	private GameObject lockedText;
	private PlayerController playerInstance; 
	private GameObject dashSprites;

	public override void Initialize() 
	{		
		active = false;
		coolDownCounter = 0f;
		abilityExp = 0f;

		playerInstance = PlayerController.Instance;
		abilityUi = GameObject.Find("UI_Ability_Dash").GetComponent<AbilityUi>();
		lockedText = GameObject.Find ("LockedText_Dash");
		myRigidBody = playerInstance.GetComponent<Rigidbody2D> ();
		dashSprites = GameObject.Find ("DashSprites");
		dashSprites.SetActive (false);
		dashSprites.transform.SetParent (null);

		abilityUi.coolDownTime = coolDownTime;
		abilityUi.coolDownCounter = 0f; 
	}

	public override void TriggerAbility()
	{
		if (playerInstance.isInBench || playerInstance.playerMoving == false || playerInstance.speedStat < requiredStat) return;

		playerInstance.attacking = true;
		playerInstance.canMove = false;
		playerInstance.attackTimeCounter = dashTime;
		dashCounter = dashTime;
		savedVelocity = myRigidBody.velocity;
		if (Mathf.Abs (Input.GetAxisRaw ("Horizontal")) > 0.5f && Mathf.Abs (Input.GetAxisRaw ("Vertical")) > 0.5f) 
		{
			currentDashSpeed = dashSpeed * diagonalSpeedMod;
		} 
		else
		{
			currentDashSpeed = dashSpeed;
		}
		myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x * currentDashSpeed, myRigidBody.velocity.y * currentDashSpeed);
		dashSprites.gameObject.SetActive (false);
		dashSprites.gameObject.SetActive (true);
		active = true;
		PlayDashSound ();
	}

	public override void CallUpdate() 
	{
		CheckForTrigger ();

		if (active) 
		{
			dashCounter -= Time.deltaTime;
		}

		if (coolDownCounter > 0) 
		{
			coolDownCounter -= Time.deltaTime;
			abilityUi.coolDownCounter = coolDownCounter;
		}

		if (dashCounter <= 0 && active) 
		{
			active = false;
			PlayerController.Instance.canMove = true;
			myRigidBody.velocity = savedVelocity;
			coolDownCounter = coolDownTime;
			abilityUi.coolDownCounter = 0f;
		}

		if (playerInstance.speedStat < requiredStat) 
		{
			abilityUi.gameObject.SetActive (false);
		} 
		else
		{
			abilityUi.gameObject.SetActive (true);
			lockedText.SetActive (false);
		}
	}

	private void PlayDashSound ()
	{
		AudioManager.instance.PlayEvent ("Ability_Dash", PlayerController.Instance.gameObject);
	}
}
