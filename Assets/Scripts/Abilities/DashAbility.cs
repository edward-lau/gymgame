﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class DashAbility : MonoBehaviour {

	public DashState dashState;
	public float dashCooldown;
	public float dashTimer;
	public float dashSpeed;
	public float maxDash = 20f;
	private PlayerController myPlayerController;
	private Rigidbody2D myRigidBody;
	private float currentSpeed;
	public Vector2 savedVelocity;

	void Start()
	{
		myRigidBody = GetComponent<Rigidbody2D> ();
		myPlayerController = GetComponent<PlayerController> ();
	}

	void Update () 
	{
		switch (dashState) 
		{
		case DashState.Ready:
			var isDashKeyDown = Input.GetKeyDown (KeyCode.LeftShift);
			if(isDashKeyDown) 
			{
				savedVelocity = myRigidBody.velocity;
				myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x * dashSpeed, myRigidBody.velocity.y * dashSpeed);
				dashState = DashState.Dashing;
			}
			break;
		case DashState.Dashing:
			dashTimer += Time.deltaTime * dashSpeed;
			if(dashTimer >= maxDash)
			{
				dashTimer = maxDash;
				myRigidBody.velocity = savedVelocity;
				dashState = DashState.Cooldown;
			}
			break;
		case DashState.Cooldown:
			dashTimer -= Time.deltaTime;
			if(dashTimer <= 0)
			{
				dashTimer = 0;
				dashState = DashState.Ready;
			}
			break;
		}
	}
}

public enum DashState 
{
	Ready,
	Dashing,
	Cooldown
}