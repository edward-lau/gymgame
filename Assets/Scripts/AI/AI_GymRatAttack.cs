﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_GymRatAttack : MonoBehaviour {

	public float speed;
	public float attackRange;
	public float cooldownTime;
	public float chargeTime;
	public float attackTime;
	public LayerMask layerMask;
	public GameObject hitBox;
	public AI_GymRatState state;

	private AIController myAIController;
	private Animator myAnim;
	private GameObject thePlayer;
	private RaycastHit2D rayhit;

	private Vector2 lastPlayerDirection;

	public float chargeTimeCounter;
	public float attackTimeCounter;
	public float coolDownCounter;

	void Awake () 
	{
		myAIController = GetComponent<AIController> ();
		myAnim = GetComponent<Animator> ();
		thePlayer = PlayerController.Instance.GetPlayer ();
	}

	void OnEnable ()
	{
		state = AI_GymRatState.NotAttacking;
		myAIController.isAttacking = false;
		myAIController.holdingPosition = false;
		myAnim.SetBool ("isAttacking", false);
		myAnim.SetBool ("isChargingAttack", false);
	}

	void Update ()
	{
		if (myAIController.isStunned) 
		{
			state = AI_GymRatState.NotAttacking;
			myAIController.isAttacking = false;
			myAIController.holdingPosition = false;
			myAnim.SetBool ("isAttacking", false);
			myAnim.SetBool ("isChargingAttack", false);
		}

		if (state == AI_GymRatState.ChargingAttack) 
		{
			if (chargeTimeCounter > 0) 
			{
				chargeTimeCounter -= Time.deltaTime;
				myAnim.SetBool ("isChargingAttack", true);
				lastPlayerDirection = thePlayer.transform.position - this.transform.position; 
			}
			if (chargeTimeCounter <= 0) 
			{
				myAnim.SetBool ("isChargingAttack", false);
				attackTimeCounter = attackTime;
				state = AI_GymRatState.ReadyToAttack;
				PlayWhooshSound ();
			}
		}

		if (state == AI_GymRatState.ReadyToAttack) 
		{
			if (attackTimeCounter > 0) 
			{
				attackTimeCounter -= Time.deltaTime;
				myAnim.SetBool ("isAttacking", true);
			} 
			else 
			{
				myAnim.SetBool ("isAttacking", false);
				coolDownCounter = cooldownTime;
				state = AI_GymRatState.CoolingDown;
			}
		}

		if (state == AI_GymRatState.CoolingDown) 
		{
			if (coolDownCounter > 0) 
			{ 
				coolDownCounter -= Time.deltaTime; 
				hitBox.SetActive (false);
				myAIController.holdingPosition = true;
//				myAnim.SetBool ("isCoolingDown", true);
			}
			if (coolDownCounter <= 0) // when cooldown is over, return to normal behaviour
			{
				state = AI_GymRatState.NotAttacking;
				myAIController.isAttacking = false;
				myAIController.holdingPosition = false;
//				myAnim.SetBool ("isCoolingDown", false);
			}
		}
	}
	
	void DoAIBehaviour () 
	{
		if (thePlayer == null) return; // return if no player

		if (myAIController.isAttacking == false) // if not attacking, attempt attack
		{
			float distance = Vector3.Distance (thePlayer.transform.position, this.transform.position); // get distance from player 
			if (distance >= attackRange) return; // stop if distance is greater than attack range

			Vector2 dir = thePlayer.transform.position - this.transform.position; // otherwise get direction to player
			rayhit = Physics2D.Raycast (this.transform.position, dir, attackRange, layerMask); // raycast to player 

			if (!rayhit) 
			{
				//check if rayhit exists to avoid error
			}
			else if (rayhit.collider.CompareTag ("Player")) // if player is in line of sight, begin attack
			{ 
				chargeTimeCounter = chargeTime; 
				state = AI_GymRatState.ChargingAttack;
				myAIController.holdingPosition = true; // tell AIController to hold position
				myAIController.isAttacking = true; // tell AIController it is attacking
			}
		}
		else // if attacking...
		{
			if (state != AI_GymRatState.ReadyToAttack) return; // don't proceed if not ready to attack

			if (state == AI_GymRatState.ReadyToAttack) // if we are ready to attack
			{
				myAIController.holdingPosition = false; // stop holding position
				Vector2 direction = lastPlayerDirection; // move to last known player location
				WeightedDirection wd = new WeightedDirection (direction, 1, speed); 
				myAIController.desiredDirections.Add (wd);
				hitBox.SetActive (true);
				myAnim.SetBool ("isAttacking", true); // play attacking animation
			}

			if (state == AI_GymRatState.CoolingDown)
			{
				myAIController.holdingPosition = true;
				myAnim.SetBool ("isAttacking", false);
			}
		}
	}

	private void PlayWhooshSound ()
	{
		AudioManager.instance.PlayEvent ("GymRat_Attack_Whoosh", this.gameObject);
	}
}

public enum AI_GymRatState
{
	NotAttacking,
	ChargingAttack,
	ReadyToAttack,
	Attacking,
	CoolingDown
}