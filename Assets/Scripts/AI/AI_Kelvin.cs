﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Kelvin : MonoBehaviour {

	public float speed;
	public float attackRange;
	public float cooldownTime;
	public float chargeTime;
	public float attackTime;
	public LayerMask layerMask;
	public GameObject hitBox;

	public AI_KelvinState state;
	public GameObject syringe;

	private AIController myAIController;
	private Animator myAnim;
	private GameObject thePlayer;
	private RaycastHit2D rayhit;
	private Vector2 playerDir;
	private float playerX;
	private Vector2 lastPlayerDirection;

	private float chargeTimeCounter;
	private float attackTimeCounter;
	private float coolDownCounter;

	void Awake () 
	{
		myAIController = GetComponent<AIController> ();
		myAnim = GetComponent<Animator> ();
		thePlayer = PlayerController.Instance.GetPlayer ();
	}

	// Use this for initialization
	void Start ()
	{
		state = AI_KelvinState.NotAttacking;
		myAIController.isAttacking = false;
		myAIController.holdingPosition = false;
		myAnim.SetBool ("isAttacking", false);
		myAnim.SetBool ("isChargingAttack", false);
	}
	
	// Update is called once per frame
	void Update () 
	{
//		if (myAIController.isStunned) 
//		{
////			state = AI_KelvinState.NotAttacking;
////			myAIController.isAttacking = false;
//			myAIController.holdingPosition = true;
////			myAnim.SetBool ("isAttacking", false);
////			myAnim.SetBool ("isChargingAttack", false);
//		}

		if (state == AI_KelvinState.ChargingAttack) 
		{
			if (chargeTimeCounter > 0) 
			{
				chargeTimeCounter -= Time.deltaTime;
				myAnim.SetBool ("isChargingAttack", true);
				lastPlayerDirection = thePlayer.transform.position - this.transform.position; 
				SetPlayerX();
			}
			if (chargeTimeCounter <= 0) 
			{
				myAnim.SetBool ("isChargingAttack", false);
				attackTimeCounter = attackTime;
				state = AI_KelvinState.ReadyToAttack;
				PlayWhooshSfx ();
			}
		}

		if (state == AI_KelvinState.ReadyToAttack) 
		{
			if (attackTimeCounter > 0) 
			{
				attackTimeCounter -= Time.deltaTime;
				myAnim.SetBool ("isAttacking", true);
			} 
			else 
			{
				myAnim.SetBool ("isAttacking", false);
//				myAIController.canBeHit = true;
				coolDownCounter = cooldownTime;
				state = AI_KelvinState.CoolingDown;
			}
		}

		if (state == AI_KelvinState.CoolingDown) 
		{
			if (coolDownCounter > 0) 
			{ 
				coolDownCounter -= Time.deltaTime;  
				myAIController.holdingPosition = true;
				myAnim.SetBool ("isAttacking", false);
			}
			if (coolDownCounter <= 0) // when cooldown is over, return to normal behaviour
			{
				state = AI_KelvinState.NotAttacking;
				myAIController.isAttacking = false;
				myAIController.holdingPosition = false;
				//myAnim.SetBool ("isCoolingDown", false);
			}
		}
	}

	void DoAIBehaviour () 
	{
		if (thePlayer == null) return; // return if no player

		if (myAIController.isAttacking == false && state == AI_KelvinState.NotAttacking) // if not attacking, attempt attack
		{
			float distance = Vector3.Distance (thePlayer.transform.position, this.transform.position); // get distance from player 
			if (distance >= attackRange) return; // stop if distance is greater than attack range
			SetPlayerX();
			myAnim.SetFloat ("playerX", playerX);
			chargeTimeCounter = chargeTime; 
			state = AI_KelvinState.ChargingAttack;
			myAIController.holdingPosition = true;
			myAIController.isAttacking = true;
		}
		else // if attacking...
		{
			if (state != AI_KelvinState.ReadyToAttack) return; // don't proceed if not ready to attack

			if (state == AI_KelvinState.ReadyToAttack) // if we are ready to attack
			{
				myAIController.holdingPosition = false; // stop holding position
				Vector2 direction = lastPlayerDirection; // move to last known player location
				WeightedDirection wd = new WeightedDirection (direction, 1, speed); 
				myAIController.desiredDirections.Add (wd);
				myAnim.SetBool ("isAttacking", true); // play attacking animation
			}
		}
	}

	void SetPlayerX ()
	{
		playerDir = thePlayer.transform.position - this.transform.position; 
		playerX = playerDir.x;
	}

	void PlayWhooshSfx ()
	{
		AudioManager.instance.PlayEvent ("Kelvin_Whoosh", this.gameObject);
	}
}

public enum AI_KelvinState
{
	NotAttacking,
	ChargingAttack,
	ReadyToAttack,
	Attacking,
	CoolingDown
}