﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_LungeLizardAttack : MonoBehaviour {

	public float speed;
	public float attackRange;
	public float restTime;
	public float cooldownTime;
	public float chargeTime;
	public float attackTime;
	public LayerMask layerMask;
	public GameObject hitBox;
	public GameObject chargeObject;
	public AI_LungeLizardState state;

	private AIController myAIController;
	private Animator myAnim;
	private GameObject thePlayer;
	private RaycastHit2D rayhit;
	private Vector2 lastPlayerDirection;
	private int repeatAttackCount; 

	public float chargeTimeCounter;
	public float attackTimeCounter;
	public float restTimeCounter;
	public float coolDownCounter;

	void Start () 
	{
		myAIController = GetComponent<AIController> ();
		myAnim = GetComponent<Animator> ();
		thePlayer = PlayerController.Instance.GetPlayer ();
		state = AI_LungeLizardState.NotAttacking;
	}

	void Update ()
	{
		if (myAIController.isStunned) 
		{
			state = AI_LungeLizardState.NotAttacking;
			myAIController.isAttacking = false;
			myAIController.holdingPosition = false;
			myAnim.SetBool ("isAttacking", false);
			myAnim.SetBool ("isCoolingDown", false);
		}

		if (state == AI_LungeLizardState.ChargingAttack) 
		{
			UpdateArmRotation ();

			if (chargeTimeCounter > 0) 
			{
				chargeTimeCounter -= Time.deltaTime;
				myAIController.isInvincible = true;
				myAIController.canBeHit = false;
				lastPlayerDirection = thePlayer.transform.position - this.transform.position; 
				myAnim.SetBool ("isChargingAttack", true);
				myAnim.SetFloat ("PlayerDirX", lastPlayerDirection.x);
			}
			if (chargeTimeCounter <= 0) 
			{
				chargeObject.SetActive (false);
				myAnim.SetBool ("isChargingAttack", false);
				attackTimeCounter = attackTime;
				state = AI_LungeLizardState.ReadyToAttack;
			}
		}

		if (state == AI_LungeLizardState.ReadyToAttack) 
		{
			if (attackTimeCounter > 0) 
			{
				attackTimeCounter -= Time.deltaTime;
				myAnim.SetBool ("isAttacking", true);
			} 

			if (attackTimeCounter <= 0)
			{
				restTimeCounter = restTime;
				myAnim.SetBool ("isAttacking", false);
				state = AI_LungeLizardState.Resting;
			}
		}

		if (state == AI_LungeLizardState.Resting) 
		{
			if (restTimeCounter > 0) 
			{ 
				restTimeCounter -= Time.deltaTime; 
				hitBox.SetActive (false);
				myAIController.isInvincible = false;
				myAIController.canBeHit = true;
				myAIController.holdingPosition = true;
				myAnim.SetBool ("isCoolingDown", true);
			}
			if (restTimeCounter <= 0) // when cooldown is over, return to normal behaviour
			{
				state = AI_LungeLizardState.CoolingDown; 
				coolDownCounter = cooldownTime;
				myAIController.isAttacking = false;
				myAIController.holdingPosition = false;
				myAnim.SetBool ("isCoolingDown", false);
			}
		}

		if (state == AI_LungeLizardState.CoolingDown) 
		{
			if (coolDownCounter > 0) 
			{ 
				coolDownCounter -= Time.deltaTime; 
			}
			if (coolDownCounter <= 0) 
			{
				state = AI_LungeLizardState.NotAttacking;
			}
		}
	}
	
	void DoAIBehaviour () 
	{
		if (thePlayer == null) return; // return if no player

		if (myAIController.isAttacking == false && state == AI_LungeLizardState.NotAttacking) // if not attacking, attempt attack
		{
			float distance = Vector3.Distance (thePlayer.transform.position, this.transform.position); // get distance from player 
			if (distance >= attackRange) return; // stop if distance is greater than attack range

			Vector2 dir = thePlayer.transform.position - this.transform.position; // otherwise get direction to player
			rayhit = Physics2D.Raycast (this.transform.position, dir, attackRange, layerMask); // raycast to player 

			if (!rayhit) 
			{
				//check if rayhit exists to avoid error
			}
			else if (rayhit.collider.CompareTag ("Player")) // if player is in line of sight, begin attack
			{ 
				chargeTimeCounter = chargeTime; 
				state = AI_LungeLizardState.ChargingAttack;
				myAIController.holdingPosition = true; // tell AIController to hold position
				myAIController.isAttacking = true; // tell AIController it is attacking
			}
		}
		else // if attacking...
		{
			if (state != AI_LungeLizardState.ReadyToAttack) return; // don't proceed if not ready to attack

			if (state == AI_LungeLizardState.ReadyToAttack) // if we are ready to attack
			{
				myAIController.holdingPosition = false; // stop holding position
				Vector2 direction = lastPlayerDirection; // move to last known player location
				WeightedDirection wd = new WeightedDirection (direction, 1, speed); 
				myAIController.desiredDirections.Add (wd);
				hitBox.SetActive (true);
				myAnim.SetBool ("isAttacking", true); // play attacking animation
			}

			if (state == AI_LungeLizardState.CoolingDown)
			{
				myAIController.holdingPosition = true;
				myAnim.SetBool ("isAttacking", false);
			}
		}
	}

	private void UpdateArmRotation()  
	{
		Vector3 moveDirection = (chargeObject.transform.position - thePlayer.transform.position).normalized; 

		if (moveDirection.x > 0) 
		{
			chargeObject.transform.localPosition = new Vector2 (0.25f, 1.01f);
		} 
		else if (moveDirection.x < 0)
		{
			chargeObject.transform.localPosition = new Vector2 (-0.25f, 1.01f);
		}

		if (moveDirection != Vector3.zero) 
		{
			float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
			chargeObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}
	}
}

public enum AI_LungeLizardState
{
	NotAttacking,
	ChargingAttack,
	ReadyToAttack,
	Attacking,
	Resting,
	CoolingDown
}