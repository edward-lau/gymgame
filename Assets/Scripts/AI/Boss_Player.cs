﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Player : MonoBehaviour {

	private static Boss_Player bossPlayer; 
	public static Boss_Player instance
	{
		get
		{
			if (!bossPlayer) 
			{ 
				bossPlayer = FindObjectOfType (typeof (Boss_Player)) as Boss_Player;
			}

			return bossPlayer;
		}
	}

	void Awake ()
	{
	}
	
	void Update () 
	{
		
	}
}
