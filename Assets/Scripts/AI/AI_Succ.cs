﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D; 

public class AI_Succ : MonoBehaviour {

	public GameObject succPoint;
	public GameObject parent;
	public float succSpeed;
	public float succRange; 
	public float swallowRange;
	public float succLiftRange; 
	public float succRotateSpeed;
	public float succTime;
	private float succTimeCounter;
	public float cooldownTime;
	private float cooldownCounter;
	public SuccState state;
	private CircleCollider2D collider; 
	private Animator myAnim;

	void Awake () 
	{
		succTimeCounter = 0;
		cooldownCounter = cooldownTime;
		state = SuccState.coolingDown;
		parent = this.transform.parent.gameObject;
		collider = GetComponent<CircleCollider2D> ();
		myAnim = GetComponentInParent<Animator> ();
	}
	
	void Update () 
	{
		if (state == SuccState.succing) 
		{
			if (succTimeCounter > 0)
			{
				succTimeCounter -= Time.deltaTime;
				Succ ();
			}

			if (succTimeCounter <= 0) 
			{
				cooldownCounter = cooldownTime;
				state = SuccState.coolingDown; 
				ProCamera2DShake.Instance.StopConstantShaking (0.3f);
				myAnim.SetBool ("isSucc", false);
				MusicManager.instance.StopTrack ("FinalBoss_Suck");
			}
		}

		if (state == SuccState.coolingDown) 
		{
			if (cooldownCounter > 0) 
			{
				cooldownCounter -= Time.deltaTime;
				StopSucc ();
			}

			if (cooldownCounter <= 0) 
			{
				succTimeCounter = succTime;
				state = SuccState.succing;
				ProCamera2DShake.Instance.ConstantShake ("EarthquakeSoft");
				myAnim.SetBool ("isSucc", true);
				MusicManager.instance.PlayTrack ("FinalBoss_Suck");
			}
		}
	}

	void Succ ()
	{
		collider.enabled = true;
		foreach (AIController enemy in Enemy_Spawner.instance.activeEnemies) 
		{
			if (enemy.myRenderer.isVisible == false) continue;
			if (Vector2.Distance (this.transform.position, enemy.transform.position) <= succRange) // start sucking enemies to base of boss
			{
				enemy.transform.position = Vector2.MoveTowards (enemy.transform.position, parent.transform.position, succSpeed * Time.deltaTime);
			}

			if (Vector2.Distance (parent.transform.position, enemy.transform.position) <= succLiftRange) // start lifting enemies to mouth
			{
				enemy.holdingPosition = true;
				enemy.myRenderer.transform.parent.localPosition = Vector2.MoveTowards (enemy.myRenderer.transform.parent.localPosition, new Vector2 (0f, 2.75f), succSpeed * Time.deltaTime);
				enemy.myRenderer.sortingLayerName = ("FlyingCharacters");
				enemy.myRenderer.gameObject.transform.Rotate (0, 0, succRotateSpeed * Time.deltaTime);
			}
		}

		if (Vector2.Distance (this.transform.position, PlayerController_Snail.instance.transform.position) <= succRange) 
		{
			PlayerController_Snail.instance.transform.position = Vector2.MoveTowards (PlayerController_Snail.instance.transform.position, parent.transform.position, (succSpeed/2.5f) * Time.deltaTime);
		}

		if (Vector2.Distance (parent.transform.position, PlayerController_Snail.instance.transform.position) <= 1) // start lifting enemies to mouth
		{
			PlayerController_Snail.instance.canMove = false;
			PlayerController_Snail.instance.myRenderer.transform.parent.localPosition = Vector2.MoveTowards (PlayerController_Snail.instance.myRenderer.transform.parent.localPosition, new Vector2 (0f, 2.75f), succSpeed * Time.deltaTime);
			PlayerController_Snail.instance.myRenderer.sortingLayerName = ("FlyingCharacters");
			PlayerController_Snail.instance.myRenderer.gameObject.transform.Rotate (0, 0, succRotateSpeed * Time.deltaTime);
		}

	}

	void StopSucc ()
	{
		collider.enabled = false; 
		foreach (AIController enemy in Enemy_Spawner.instance.activeEnemies) 
		{
			if (Vector2.Distance (this.transform.parent.transform.position, enemy.transform.position) <= succRange)
			{
				enemy.myRenderer.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
				enemy.myRenderer.transform.parent.localPosition = Vector2.MoveTowards (enemy.myRenderer.transform.parent.localPosition, new Vector2 (0f, 0f), (succSpeed * 2) * Time.deltaTime);
				if (enemy.myRenderer.transform.parent.localPosition.y == 0) 
				{
					enemy.holdingPosition = false;
					enemy.myRenderer.sortingLayerName = ("Characters");
				}
			}
		}

		if (Vector2.Distance (this.transform.parent.transform.position, PlayerController_Snail.instance.transform.position) <= succRange)
		{
			PlayerController_Snail.instance.myRenderer.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
			PlayerController_Snail.instance.myRenderer.transform.parent.localPosition = Vector2.MoveTowards (PlayerController_Snail.instance.myRenderer.transform.parent.localPosition, new Vector2 (0f, 0f), (succSpeed * 2) * Time.deltaTime);
			if (PlayerController_Snail.instance.myRenderer.transform.parent.localPosition.y == 0) 
			{
				PlayerController_Snail.instance.canMove = true;
				PlayerController_Snail.instance.myRenderer.sortingLayerName = ("Characters");
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			AIController enemy = other.gameObject.GetComponentInParent<AIController> (); 
			enemy.myRenderer.gameObject.GetComponent<ScaleFx> ().Fade (1, 0.01f);
		}

		if (other.gameObject.tag == "Player_Snail") 
		{
			PlayerController_Snail player = other.gameObject.GetComponentInParent<PlayerController_Snail> ();
			player.myRenderer.gameObject.GetComponent<ScaleFx> ().Fade (1, 0.01f);
		}
	}

}

public enum SuccState 
{
	succing,
	coolingDown
}