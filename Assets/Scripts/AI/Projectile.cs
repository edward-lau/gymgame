﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour 
{

	public float rotateSpeed;
	public GameObject spriteObj;
	private Animator anim;
	private Rigidbody2D rigidBody;
	private bool hit;

	void Awake ()
	{
		anim = GetComponent<Animator> ();
		rigidBody = GetComponent<Rigidbody2D> ();
	}

	void OnEnable()
	{
		spriteObj.transform.Rotate (0, 0, 0);
		hit = false;
	}

	void Update()
	{
		if (hit == false) 
		{
			spriteObj.transform.Rotate (0, 0, rotateSpeed * Time.deltaTime);
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Walls")) 
		{
			hit = true;
			rigidBody.velocity = Vector2.zero;
			rigidBody.Sleep ();
			anim.Play ("Hit");
			PlayHitSound ();
		}
	}

	void Deactivate ()
	{
		this.gameObject.SetActive (false);
	}

	void PlaySpinningSound()
	{
		AudioManager.instance.PlayEvent ("Ball_Projectile", this.gameObject);
	}

	void PlayHitSound()
	{
		AudioManager.instance.PlayEvent ("Ball_Projectile_Hit", this.gameObject);
	}
}