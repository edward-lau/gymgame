﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_LeaveGym : MonoBehaviour 
{
	public float weight;
	public float speed = 1f; 
	public float leaveRange = 1f;
	private bool leaveGym;
	AIController myAIController;

	void Start () 
	{
		myAIController = GetComponent<AIController> ();
	}

	void OnEnable ()
	{
		Reset ();
	}

	private void Reset()
	{
		leaveGym = false;
	}

	void DoAIBehaviour()
	{
		if (leaveGym == true) 
		{
			GameObject closest = null;
			float dist = Mathf.Infinity;

			foreach (SpawnLocation spawnLocation in Enemy_Spawner.instance.spawnLocations) 
			{
				if (spawnLocation.canSpawn == false) continue;

				float d = Vector2.Distance (this.transform.position, spawnLocation.transform.position);

				if (closest == null || d < dist) 
				{
					closest = spawnLocation.gameObject;
					dist = d;
				}
			}

			if (closest == null)  // if no valid stations, return
			{
				return;
			}

			if (dist < leaveRange) // use station when in range
			{
				myAIController.ReturnToPool ();
			}
			else
			{
				Vector2 dir = closest.transform.position - this.transform.position;

				WeightedDirection wd = new WeightedDirection (dir, weight, speed);

				myAIController.desiredDirections.Add (wd);
			}
		}
	}

	public void LeaveGym ()
	{
		leaveGym = true;
	}
}