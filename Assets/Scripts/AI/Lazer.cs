﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lazer : MonoBehaviour {

	public GameObject lazerStart;
	public GameObject lazerMiddle;
	public GameObject lazerEnd;

	public float shootTime;
	private float shootCounter; 

	public float maxCoolDown;
	public float minCoolDown;
	private float coolDownTime; 
	private float coolDownCounter;

	private LazerState state;
	private Vector3 target;
	private Vector3 secondTarget;

	public float xMin;
	public float xMax;
	public float yMin;
	public float yMax;
	public float speed;
	public float moveRange;
	private float distanceMoved;

	public List<GameObject> firePool = new List<GameObject> ();
	public List<GameObject> activeFire = new List<GameObject> ();
	public GameObject firePrefab;
	public AI_Succ succPoint;
	public int fireCount;
	public float firePerDistance;
	private GameObject currentFire;
	private int index = 0; 

	void Start () 
	{
		Debug.Log (this.transform.position);
		coolDownTime = 5f;
		coolDownCounter = 5f;
		state = LazerState.CoolingDown;
		SetLazerEnabled (false);

		for (int i = 0; i < fireCount; i++) 
		{
			GameObject newFire = Instantiate(firePrefab);  
			newFire.SetActive (false);
			newFire.transform.SetParent (this.gameObject.transform);
			firePool.Add (newFire);
		}
	}
	
	void Update () 
	{
		if (state == LazerState.Shooting) 
		{
			if (shootCounter > 0)
			{
				shootCounter -= Time.deltaTime;
				SetLazerEnabled (true);
				currentFire.transform.position = target;

				target = Vector3.MoveTowards (target, PlayerController_Snail.instance.transform.position, speed * Time.deltaTime);
				AudioManager.instance.PlayEvent ("FinalBoss_Laser_Loop", lazerEnd);
			} 

			if (shootCounter <= 0)
			{
				coolDownTime = Random.Range (minCoolDown, maxCoolDown);
				coolDownCounter = coolDownTime;
				state = LazerState.CoolingDown;
				SetLazerEnabled (false);
				AudioManager.instance.StopEvent ("FinalBoss_Laser_Loop", lazerEnd);
			}
		}

		if (state == LazerState.CoolingDown) 
		{
			if (coolDownCounter > 0)
			{
				coolDownCounter -= Time.deltaTime;
			}

			if (coolDownCounter <= 0) 
			{
				state = LazerState.Waiting;
				target = GetTarget ();
//				AudioEffectLowPass.instance.PlayLowPassHit ();
				StartCoroutine (WaitAndShoot ());
			}
		}

		Vector2 startPos = this.transform.position;
		Vector3 endPos = target;

		float currentLength = Vector2.Distance (startPos, endPos);

		lazerMiddle.transform.localScale = new Vector3 (currentLength - 1, lazerMiddle.transform.localScale.y, lazerMiddle.transform.localScale.z);

		lazerEnd.transform.position = endPos; 
		lazerEnd.transform.localPosition = new Vector2 (lazerEnd.transform.localPosition.x, 0.0f);

		lazerMiddle.transform.localPosition = new Vector2 ((lazerStart.transform.localPosition.x + lazerEnd.transform.localPosition.x) / 2.0f, lazerMiddle.transform.localPosition.y);

		UpdateRotation ();
	}

	private void UpdateRotation()  
	{
		Vector3 lazerDirection = (target - this.transform.position).normalized; 

		if (lazerDirection != Vector3.zero) 
		{
			float angle = Mathf.Atan2(lazerDirection.y, lazerDirection.x) * Mathf.Rad2Deg;
			this.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}
	}

	private void SetLazerEnabled (bool truOrNah)
	{
		lazerStart.SetActive (truOrNah);
		lazerMiddle.SetActive (truOrNah);
		lazerEnd.SetActive (truOrNah);
	}
		
	private Vector2 GetTarget ()
	{		
		target = new Vector2 (Random.Range (xMin, xMax), Random.Range (yMin, yMax));
//		target = PlayerController_Snail.instance.transform.position;

		return target;
	}

	private Vector2 SecondTarget ()
	{

		Vector2 secondTarget = new Vector2 (Random.Range (target.x - moveRange, target.x + moveRange), Random.Range (target.y - moveRange, target.y + moveRange));

		return secondTarget;
	}

	private void SpawnFire () 
	{
		GameObject fire = firePool [index]; 
		fire.GetComponent<SpriteRenderer> ().flipX = (Random.value > 0.5f);
		if (fire.activeInHierarchy == false) 
		{
			fire.transform.parent = null; 
			fire.SetActive (true);
		}
		fire.transform.position = target;
		fire.transform.rotation = Quaternion.EulerAngles (0, 0, 0);
		currentFire = fire;
		index++;
		if (index >= firePool.Count) 
		{
			index = 0;
		}
	}

	IEnumerator WaitAndShoot ()
	{
		AudioManager.instance.PlayEvent ("FinalBoss_Laser_Charge", this.gameObject);
		yield return new WaitForSeconds (1);
		shootCounter = shootTime;
		secondTarget = SecondTarget ();
		//speed = Vector3.Distance (target, secondTarget) / shootTime;
		SpawnFire ();
		state = LazerState.Shooting;
		AudioManager.instance.PlayEvent ("FinalBoss_Laser_Blast", this.gameObject);
	}
		
}

public enum LazerState
{
	Shooting,
	CoolingDown,
	Waiting
}
