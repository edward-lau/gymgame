﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Wander : MonoBehaviour {

	//private Dictionary<string, Vector2> normalDirections;
	private Vector2[] normalDirections = {new Vector2(0, 1), new Vector2(0, -1), new Vector2(1, 0), new Vector2(-1, 0), new Vector2(1, 1), new Vector2(1, -1), new Vector2(-1, 1), new Vector2(-1, -1)};

	public float speed;
	public float maxIdleTime; 
	public float minIdleTime; 
	public float maxMoveTime;
	public float minMoveTime;
	public float collisionAvoidRadius;
	public LayerMask layerMask;
	AIController myAIController;
	Vector2 randomDir;
	private float currentIdleTime = 0f;
	private float currentMoveTime = 0f;
	private float moveTime;
	private float idleTime;
	private RaycastHit2D rayHit;

	private bool isBlocked;

	 
	void Start () 
	{
		myAIController = GetComponent<AIController> (); 
		randomDir = normalDirections[Random.Range(0, normalDirections.Length)];
		idleTime = Random.Range (minIdleTime, maxIdleTime);
		moveTime = Random.Range (minMoveTime, maxMoveTime);
	} 
	
	void DoAIBehaviour () 
	{
		currentIdleTime += Time.deltaTime; // add idle time
		if (currentIdleTime > idleTime) { // if AI has finished idling

			currentMoveTime += Time.deltaTime; 
			if (currentMoveTime < moveTime) 
			{
				//Vector2 dir = randomDir;
	
				rayHit = Physics2D.Raycast (this.transform.position, randomDir, collisionAvoidRadius, layerMask);

				while (rayHit.distance > 0) // if ray is hitting an obstacle
				{
					//randomDir = Random.onUnitSphere; // get a new random direction
					randomDir = normalDirections[Random.Range(0, normalDirections.Length)];
					rayHit = Physics2D.Raycast (this.transform.position, randomDir, collisionAvoidRadius, layerMask); // raycast to new direction
					if (rayHit.distance > 0) {
						randomDir = randomDir * -1;
					}
				}

				WeightedDirection wd = new WeightedDirection (randomDir, 0.01f, speed);
				myAIController.desiredDirections.Add (wd);
			}
			else // move complete, reset and get new random parameters
			{
				currentMoveTime = 0f;
				currentIdleTime = 0f; 
				moveTime = Random.Range (minMoveTime, maxMoveTime);
				idleTime = Random.Range (minIdleTime, maxIdleTime);
				randomDir = normalDirections[Random.Range(0, normalDirections.Length)];
			}
		} 
	}
}