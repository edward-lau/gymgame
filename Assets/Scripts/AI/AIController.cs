﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class AIController : MonoBehaviour 
{
	public string aiType = "GymRat";
	public float baseHealth = 100f;
	private float health;
	public float speed;
//	public float knockBackTime;
	public float knockBackMultiplier; 
	private float knockBackSpeed;
	public float stunTimeMultiplier;
//	public float stunTime;
	public float flashTime;
	public bool isAttacking;
	public bool holdingPosition;
	public bool canBeHit = true;
	public int dangerLevel;

	public bool isGoingToDie = false;
	public BodManager bodyManager;

	private Rigidbody2D myRigidBody;
	private Vector2 velocity;
	private Animator myAnimator;
	public SpriteRenderer myRenderer;
	private Shader shaderGUItext;
	private Shader shaderSpritesDefault;
	private byte flasher = 0;

	public bool isInvincible;
	public bool isStunned = false;
	private float stunTimeCounter;
	private Vector2 knockbackDir; 
	private float knockBackCounter = 0;
	private float flashTimeCounter = 0;
	public string hitSound;

	static public Dictionary<string, List<AIController>> aiByType;
	public List<WeightedDirection> desiredDirections;

	void Awake () 
	{
		myAnimator = GetComponent<Animator>();
		myRigidBody = GetComponent<Rigidbody2D> ();
		shaderGUItext = Shader.Find("GUI/Text Shader");
		shaderSpritesDefault = Shader.Find("Sprites/Default");
		health = baseHealth;

		if(aiByType == null) 
		{
			aiByType = new Dictionary<string, List<AIController>>();
		}
		if(aiByType.ContainsKey(aiType) == false) 
		{
			aiByType[aiType] = new List<AIController>();
		}
		aiByType[aiType].Add(this);
	}

	void OnEnable ()
	{
		myRenderer.sortingLayerName = ("Characters");
		health = baseHealth;
	}

	void OnDestroy()
	{
		// Remove destroyed object from list
		aiByType[aiType].Remove(this);
	}

	void Update ()
	{
		UpdateStunTimeCounter ();
	}

	void UpdateStunTimeCounter() 
	{
		if (stunTimeCounter > 0) 
		{
			stunTimeCounter -= Time.deltaTime;
		}
		if (stunTimeCounter <= 0) 
		{
			isStunned = false;
			SetNormalSprite ();
			if(health <= 0) 
			{
				bool flip;

				if (myAnimator.GetFloat ("LastMoveX") < 0) 
				{
					flip = true;
				}
				else 
				{
					flip = false;
				}

				bodyManager.SpawnBod (this.transform.position, flip);
				PickupManager.instance.SpawnPickup (this.transform.position);
				ReturnToPool();
				return;
			}
		}
	}

	public void ReturnToPool ()
	{
		this.gameObject.SetActive(false);
		health = baseHealth;
		Enemy_Spawner.instance.activeEnemies.Remove (this);
		Enemy_Spawner.instance.poolByType [aiType].Add (this);
		Enemy_Spawner.instance.totalPool.Add (this);
	}
		

	void FixedUpdate () 
	{
		desiredDirections = new List<WeightedDirection>();
		BroadcastMessage("DoAIBehaviour", SendMessageOptions.DontRequireReceiver);

		// Add up all the desired directions by weight
		Vector2 dir = Vector2.zero;

		speed = 0;
		float totalWeight = 0;
		foreach(WeightedDirection wd in desiredDirections) 
		{
			dir += wd.direction * wd.weight; 
			totalWeight += wd.weight; // get the total weight
			speed += wd.speed * wd.weight; // get the total speed * weight
		}

		if (totalWeight != 0) 
		{
			speed /= totalWeight; 
		}

		velocity = Vector2.Lerp(velocity, dir.normalized * speed, Time.deltaTime * 5f);
		Vector3 v3 = velocity;

		// Move in the desired direction at our top speed.
		if (isStunned == false && holdingPosition == false) 
		{
//			transform.Translate (velocity * Time.deltaTime);
			myRigidBody.MovePosition (transform.position + (v3 * Time.deltaTime));

			float compare = .1f;

			if (velocity.x < compare && velocity.x > -compare && velocity.y < compare && velocity.y > -compare) 
			{
				myAnimator.SetBool ("isMoving", false);
			} 
			else 
			{
				if (isAttacking == false) 
				{
					myAnimator.SetBool ("isMoving", true);
				}
				myAnimator.SetFloat ("MoveX", velocity.x);
				myAnimator.SetFloat ("LastMoveX", velocity.x);
			}
		}

		if (holdingPosition) 
		{
			myAnimator.SetBool ("isMoving", false);
		}

		if (isStunned)
		{
			if (flashTimeCounter > 0) 
			{
				flashTimeCounter -= Time.deltaTime;
			} 
			else 
			{
				flashTimeCounter = flashTime;
				flasher++;
				switch (flasher) 
				{
					case 1:
						SetWhiteSprite ();
						break;
					case 2: 
						SetNormalSprite ();
						break;
					case 3:
						SetColorSprite (new Color (1f, 0.4f, 0.4f));
						break;
					case 4:
						SetNormalSprite ();
						flasher = 0;
						break;
				}
			}
			if (knockBackCounter > 0) 
			{
				transform.Translate (knockbackDir.normalized * knockBackSpeed * Time.deltaTime);
				knockBackCounter -= Time.deltaTime;
			}
		}
	}

	public void TakeDamage (float damage, GameObject attackObj, float stunTime, float knockSpeed, float knockBackTime, float shakeStrength, float shakeDur, bool playSfx) 
	{
		if (isStunned == false && canBeHit == true) 
		{
			isAttacking = false;
			stunTimeCounter = stunTime * stunTimeMultiplier;
			knockBackSpeed = knockSpeed * knockBackMultiplier;
			knockBackCounter = knockBackTime;
			isStunned = true;
			if (isInvincible == false) 
			{
				health -= damage;
				if (health <= 0) 
				{
					isGoingToDie = true;
				}
			}
			knockbackDir = (this.transform.position - attackObj.transform.position).normalized;

			flashTimeCounter = flashTime;
			flasher = 0;
			SetWhiteSprite ();
			myAnimator.SetBool ("isMoving", false);
			if (playSfx) 
			{
				PlayHitSound (); 
			}
		}
	}

	public void ScreenShake (float duration, Vector2 strength, float angle)
	{
		ProCamera2DShake.Instance.Shake (0.04f, strength, 0, 0f, angle, default(Vector3), 0.1f, true); 
	}

	private void SetWhiteSprite() 
	{
		myRenderer.material.shader = shaderGUItext;
		myRenderer.color = Color.white; 
	}

	private void SetColorSprite(Color color)
	{
		myRenderer.material.shader = shaderGUItext;
		myRenderer.color = color; 
	}
		
	private void SetNormalSprite() 
	{
		myRenderer.material.shader = shaderSpritesDefault;
		myRenderer.color = Color.white;
	}

	void OnCollisionExit2D ()
	{
		myRigidBody.velocity = Vector3.zero;
	}

	private float GetAngle(Vector2 pointA, Vector2 pointB)
	{
		Vector2 direction = (pointA - pointB).normalized;
		float angle = Mathf.Atan2(direction.y,  direction.x) * Mathf.Rad2Deg;
		if (angle < 0f) angle += 360f;
		return angle;
	}

	private void PlayHitSound ()
	{
		AudioManager.instance.PlayEvent (hitSound, this.gameObject);
	}
}