﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_SeekBoss : MonoBehaviour {

	public int weight;
	public float speed;
	public float seekDistance; 
	public float attackRange;

	private AIController myAIController;
	private GameObject theBoss;

	void Start () 
	{
		myAIController = GetComponent<AIController>();
		if (Boss_Player.instance)
		{
			theBoss = Boss_Player.instance.gameObject;
		}
	}

	void DoAIBehaviour () 
	{
		if (theBoss == null)
		{
			return;
		}

		float distance = Vector3.Distance (theBoss.transform.position, this.transform.position);
	
		if (distance <= attackRange) 
		{

		} 
		else 
		{
			Vector2 direction = theBoss.transform.position - this.transform.position; 
			WeightedDirection wd = new WeightedDirection (direction, weight, speed);
			myAIController.desiredDirections.Add (wd);
		}
	} 
}
