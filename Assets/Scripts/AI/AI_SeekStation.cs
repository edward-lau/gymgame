﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_SeekStation : MonoBehaviour {

	public float speed = 1f; 
	public float useRange = 1f;
	public float useStationTime = 5f;
	public float waitTime = 5f;

	public AI_SeekStationState state;

	private float useStationTimeCounter;
	private float waitTimeCounter; 
	private WorkoutStation stationInUse;

	AIController myAIController;
//	AI_LeaveGym myLeaveGym;

	void Start () 
	{
		myAIController = GetComponent<AIController> ();
//		myLeaveGym = GetComponent<AI_LeaveGym> ();
		state = AI_SeekStationState.Seeking;
	}

	void OnEnable ()
	{
		Reset ();
	}

	private void Reset ()
	{
		state = AI_SeekStationState.Seeking;
	}

	void Update ()
	{
		if (state == AI_SeekStationState.UsingStation) 
		{
			if (useStationTimeCounter > 0) 
			{
				useStationTimeCounter-= Time.deltaTime;
			}
			if (useStationTimeCounter <= 0) 
			{
				waitTimeCounter = waitTime;
				stationInUse.LeaveStation ();
				myAIController.holdingPosition = false;
				myAIController.canBeHit = true;
				state = AI_SeekStationState.Waiting;
			}
		}

		if (state == AI_SeekStationState.Waiting)
		{
			if (waitTimeCounter > 0)
			{
				waitTimeCounter -= Time.deltaTime;
			}
			if (waitTimeCounter <= 0) 
			{
				state = AI_SeekStationState.Seeking;
			}
		}

//		if (state == AI_SeekStationState.Leaving) 
//		{
//			myLeaveGym.LeaveGym ();
//		}
	}
	
	void DoAIBehaviour()
	{
		if (WorkoutStationList.stations == null) return;

		if (state == AI_SeekStationState.Seeking) 
		{
			WorkoutStation closest = null;
			float dist = Mathf.Infinity;

			foreach (WorkoutStation station in WorkoutStationList.stations) 
			{
				if (station.isOccupied) // ignore station if occupied
				{ 
					continue;
				}

				float d = Vector2.Distance (this.transform.position, station.transform.position);

				if (closest == null || d < dist) 
				{
					closest = station;
					dist = d;
				}
			}

			if (closest == null)  // if no valid stations, return
			{
				return;
			}

			if (dist < useRange) // use station when in range
			{
				closest.UseStation (this.gameObject, 1);
				useStationTimeCounter = useStationTime;
				myAIController.holdingPosition = true;
				myAIController.canBeHit = false;
				state = AI_SeekStationState.UsingStation;
				stationInUse = closest;
			} 
			else // move in range of station
			{
				Vector2 dir = closest.transform.position - this.transform.position;

				WeightedDirection wd = new WeightedDirection (dir, 1, speed);

				myAIController.desiredDirections.Add (wd);
			}
		}

	}
}

public enum AI_SeekStationState
{
	Seeking,
	UsingStation,
	Waiting,
	Leaving
}