﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum AI_FlyingState 
{
	isFlying,
	isLanding,
	isGrounded,
	isTakingOff,
}

public enum AI_FlyAttackState  
{
	NotAttacking,
	Seeking,
	ChargingAttack,
	ReadyToAttack,
	Attacking,
	CoolingDown
}

public enum AI_FlyProjectileState
{
	Chillin,
	Shootin,
	Explodin,
}

public class AI_FlyAttack: MonoBehaviour 
{

	public float speed;
	public float attackRange;
	public float cooldownTime;
	public float chargeTime; 
	public float attackTime;

	public float projectileActiveY;
	public float projectileSpeed;


	public float minFlyTime;
	public float maxFlyTime;
	public float landingTime;
	public float takeoffTime;
	public float groundTime; 
	public float groundYValue; 
	public float flyingYValue; 
	public float projectileDestroyTime; 
	public LayerMask layerMask; 
	public GameObject hitBox; 
	public AI_FlyingState flyingState; 
	public AI_FlyAttackState attackState;  
	public AI_FlyProjectileState projectileState;
	public int shotsBeforeLanding;

	private int shotsFired;

	public GameObject flyObj;
	public GameObject flyProjectile;
	private CircleCollider2D flyProjectileCollider; 
	private HurtPlayer hurtComponent; 
	private Animator projectileAnim; 
	private float flyTime;

	private AIController myAIController;
	private Animator myAnim; 
	private GameObject thePlayer;
	private RaycastHit2D rayhit;

	private Vector3 lastPlayerDirection;
	public SpriteRenderer renderer;
	 
	public float chargeTimeCounter; 
	public float attackTimeCounter;
	public float coolDownCounter;
	public float flyTimeCounter;
	public float landingTimeCounter;
	public float groundTimeCounter;
	public float projectileDestroyCounter;

	void Start () 
	{
		myAIController = GetComponent<AIController> (); 
		myAnim = GetComponentInChildren<Animator> ();
		thePlayer = PlayerController.Instance.GetPlayer ();
		attackState = AI_FlyAttackState.NotAttacking;
		flyingState = AI_FlyingState.isTakingOff; 
		myAnim.SetBool ("isFlying", true); 
		myAIController.canBeHit = false; 
		flyTime = Random.Range (minFlyTime, maxFlyTime);
		flyTimeCounter = flyTime;

		flyProjectileCollider = flyProjectile.GetComponent<CircleCollider2D> (); 
		hurtComponent = flyProjectile.GetComponent<HurtPlayer> ();  
		projectileAnim = flyProjectile.GetComponent<Animator> ();

		FlyFly ();
	}

	void LandFly()
	{
		flyingState = AI_FlyingState.isLanding;
		StartCoroutine (FadeY(flyingYValue, groundYValue, landingTime, flyObj));
		flyTime = Random.Range (minFlyTime, maxFlyTime);
	}

	void FlyFly()
	{
		flyingState = AI_FlyingState.isTakingOff;
		StartCoroutine (FadeY(groundYValue, flyingYValue, takeoffTime, flyObj));
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (flyingState == AI_FlyingState.isFlying) 
		{
			if (flyTimeCounter > 0) 
			{
				flyTimeCounter -= Time.deltaTime; 
			}

			if (flyTimeCounter <= 0)
			{
				LandFly ();
			}
		}

		if (flyingState == AI_FlyingState.isLanding) 
		{
			myAIController.holdingPosition = true;
			if (flyObj.transform.localPosition.y == groundYValue) {
				groundTimeCounter = groundTime;
				flyingState = AI_FlyingState.isGrounded;
				myAnim.SetBool ("isFlying", false);
			}
		}
			
		if (flyingState == AI_FlyingState.isGrounded) 
		{
			if (groundTimeCounter > 0)
			{
				myAIController.holdingPosition = true;
				myAIController.canBeHit = true;
				groundTimeCounter -= Time.deltaTime;
				myAnim.SetBool ("isFlying", false);
			}

			if (groundTimeCounter <= 0 || (myAIController.isStunned && myAIController.isGoingToDie == false)) 
			{
				FlyFly ();
			}
		}

		if (flyingState == AI_FlyingState.isTakingOff) 
		{
			myAnim.SetBool ("isFlying", true);
			myAIController.canBeHit = false;
			myAIController.holdingPosition = false;
			if (flyObj.transform.localPosition.y == flyingYValue) 
			{
				flyTimeCounter = flyTime;
				flyingState = AI_FlyingState.isFlying;
			}
		}

		if (flyProjectile.transform.localPosition.y <= projectileActiveY) 
		{
			flyProjectileCollider.enabled = true;
		}

		if ((hurtComponent.hitPlayer || flyProjectile.transform.localPosition.y == 0) && projectileState != AI_FlyProjectileState.Explodin) 
		{
			projectileDestroyCounter = projectileDestroyTime;
			projectileState = AI_FlyProjectileState.Explodin;
			hurtComponent.hitPlayer = false;
			projectileAnim.SetBool ("projectileImpact", true);
		}



		if (projectileState == AI_FlyProjectileState.Explodin)
		{
			if (projectileDestroyCounter > 0) 
			{
				projectileDestroyCounter -= Time.deltaTime;
			}

			if (projectileDestroyCounter <= 0) 
			{
				projectileState = AI_FlyProjectileState.Chillin;
				flyProjectile.SetActive (false);
				flyProjectile.transform.localPosition = new Vector3(0, 2.0f, 0); 
				projectileAnim.SetBool ("projectileImpact", false);
				shotsFired++;
			}
		}

		if (attackState == AI_FlyAttackState.Attacking) 
		{
			if (attackTimeCounter > 0) 
			{
				attackTimeCounter -= Time.deltaTime;
			}

			if (attackTimeCounter <= 0) 
			{
				coolDownCounter = cooldownTime;  
				flyTimeCounter = flyTime;
				myAIController.holdingPosition = false;
				attackState = AI_FlyAttackState.CoolingDown;
				myAnim.SetBool ("isAttacking", false);
			}
		}

		if (attackState == AI_FlyAttackState.CoolingDown)
		{
			if (coolDownCounter > 0) 
			{ 
				coolDownCounter -= Time.deltaTime; 
			}
			if (coolDownCounter <= 0) 
			{
				attackState = AI_FlyAttackState.NotAttacking;
				myAIController.isAttacking = false;
			}
		}

		if (shotsFired >= shotsBeforeLanding) 
		{
			LandFly ();
			shotsFired = 0;
		}
	}

	void DoAIBehaviour ()
	{
		if (thePlayer == null) return; // return if no player

		if (flyingState != AI_FlyingState.isFlying) return;

		if (myAIController.isAttacking == false && projectileState == AI_FlyProjectileState.Chillin) // if not attacking, attempt attack
		{
			float distance = Vector3.Distance (thePlayer.transform.position, this.transform.position); // get distance from player 
			if (distance >= attackRange) return; // stop if distance is greater than attack range

//			Vector2 dir = thePlayer.transform.position - this.transform.position; // otherwise get direction to player
			projectileState = AI_FlyProjectileState.Shootin;
			myAIController.holdingPosition = true; // tell AIController to hold position
			myAIController.isAttacking = true; 
			//Time.timeScale = 0;
		}
		else // if attacking...
		{
			if (attackState == AI_FlyAttackState.Attacking || attackState == AI_FlyAttackState.CoolingDown) return;
			attackTimeCounter = attackTime;
			attackState = AI_FlyAttackState.Attacking;
			flyProjectile.SetActive (true);
			StartCoroutine (FadeY (2f, 0f, projectileSpeed, flyProjectile));
			myAnim.SetBool ("isAttacking", true); 
		}
	}

	private IEnumerator FadeY (float start, float target, float fadeTime, GameObject obj)
	{
		float startY = start;
		float targetY = target;
		float SecondsToFade = fadeTime; 
		float rate = 1.0f / SecondsToFade;
		float yValue;
		float x = 0.0f;

		for (x = 0.0f; x <= 1.0f; x += Time.deltaTime * rate) 
		{
			yValue = Mathf.Lerp (startY, targetY, x);  
			obj.transform.localPosition = new Vector3(0, yValue, 0);

			if (obj == flyObj && flyingState == AI_FlyingState.isLanding && x > .5)
			{
				renderer.sortingLayerName = "Characters";
			}
			else if (obj == flyObj && flyingState == AI_FlyingState.isTakingOff && x > .5) 
			{
				renderer.sortingLayerName = "FlyingCharacters";
			}
				
			yield return null;
		}

		if (x >= 1.0f) 
		{
			obj.transform.localPosition = new Vector3( 0, target, 0);
		}
	}
}