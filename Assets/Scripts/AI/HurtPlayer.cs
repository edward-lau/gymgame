﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayer : MonoBehaviour {

	public float damage;
	public float stunTime;
	public float knockBackSpeed; 
	public float knockBackTime;
	public float shakeStrength;
	public float shakeDuration;
	public bool hitPlayer;
	public bool playHitSound;

	void OnEnable ()
	{
		hitPlayer = false;
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			other.gameObject.GetComponent<PlayerController>().TakeDamage (damage, gameObject, stunTime, knockBackSpeed, knockBackTime, shakeStrength, shakeDuration, playHitSound);
			hitPlayer = true;
		}
	}
}
