﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_SeekPlayer : MonoBehaviour {

	public float speed;
	public float seekDistance; 
	public float attackRange;
	public LayerMask layerMask;

	private AIController myAIController;
	private GameObject thePlayer;
	private RaycastHit2D rayhit;
	private Vector3 lastPlayerLocation;

	public float minMemoryTime;
	public float maxMemoryTime;

	private float memoryTime;
	private float currentMemoryTime;
	private bool remembersPlayerLocation;

	void Start () 
	{
		myAIController = GetComponent<AIController>();
		thePlayer = PlayerController.Instance.GetPlayer ();
		memoryTime = Random.Range (minMemoryTime, maxMemoryTime);
	}

	void DoAIBehaviour () 
	{
		if (thePlayer == null)
		{
			return;
		}

		Vector2 dir = thePlayer.transform.position - this.transform.position; // get the direction to the player

		rayhit = Physics2D.Raycast (this.transform.position, dir, seekDistance, layerMask); // raycast to player

		if (!rayhit) 
		{
			//check if rayhit exists to avoid error
		}
		else if (rayhit.collider.CompareTag("Player")) // if player is in line of sight, attempt to move towards player
		{
			float distance = Vector3.Distance (thePlayer.transform.position, this.transform.position);

			currentMemoryTime = 0f;
			lastPlayerLocation = thePlayer.transform.position;
			remembersPlayerLocation = true;

			if (distance <= attackRange) 
			{
				
			} 
			else 
			{
				Vector2 direction = thePlayer.transform.position - this.transform.position; 
				WeightedDirection wd = new WeightedDirection (direction, 1, speed);
				myAIController.desiredDirections.Add (wd);
			}
		} 
		else // if player is not in sight, attempt to move towards last known player location
		{
			if (currentMemoryTime < memoryTime && remembersPlayerLocation == true) // move towards last known player position
			{
				currentMemoryTime += Time.deltaTime;
				Vector2 direction = lastPlayerLocation - this.transform.position;

				WeightedDirection wd = new WeightedDirection (direction, 1, speed);
				myAIController.desiredDirections.Add (wd);
			} 
			else 
			{
				if (remembersPlayerLocation) // otherwise get a new memory time that will be used the next time player is spotted
				{
					memoryTime = Random.Range (minMemoryTime, maxMemoryTime);
					remembersPlayerLocation = false; 
				} 
			}
		}
	} 
}
