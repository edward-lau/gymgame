﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_BallAttack : MonoBehaviour {

	public float projectileSpeed;
	public float attackRange;
	public float cooldownTime;
	public float chargeTime;
	public float attackTime;
	public LayerMask layerMask;
	public GameObject hitBox;
	public AI_BallState state;

	private AIController myAIController;
	private Animator myAnim;
	private GameObject thePlayer;
	private RaycastHit2D rayhit;

	private Vector2 lastPlayerDirection;
	private Vector2 projectileSpawnPos;

	public GameObject projectile;
	public Rigidbody2D projectileBody;

	public float chargeTimeCounter;
	public float attackTimeCounter;
	public float coolDownCounter;

	void Awake () 
	{
		myAIController = GetComponent<AIController> ();
		myAnim = GetComponent<Animator> ();
		thePlayer = PlayerController.Instance.GetPlayer ();
		if (projectile) {
			projectileBody = projectile.GetComponent<Rigidbody2D> ();
		}
	}

	void OnEnable ()
	{
		state = AI_BallState.NotAttacking;
		myAIController.isAttacking = false;
		myAIController.holdingPosition = false;
		myAnim.SetBool ("isAttacking", false);
		myAnim.SetBool ("isChargingAttack", false);
	}

	void Update ()
	{
		if (myAIController.isStunned) 
		{
			state = AI_BallState.NotAttacking;
			myAIController.isAttacking = false;
			myAIController.holdingPosition = false;
			myAnim.SetBool ("isAttacking", false);
			myAnim.SetBool ("isChargingAttack", false);
		}

		if (state == AI_BallState.ChargingAttack) 
		{
			if (chargeTimeCounter > 0) 
			{
				chargeTimeCounter -= Time.deltaTime;
				myAnim.SetBool ("isChargingAttack", true);
				lastPlayerDirection = thePlayer.transform.position - this.transform.position;
				if (lastPlayerDirection.x > 0) 
				{
					myAnim.SetFloat ("LastMoveX", 1);
				}
				else if (lastPlayerDirection.x < 0) 
				{
					myAnim.SetFloat ("LastMoveX", -1);
				}
				myAnim.SetFloat ("PlayerDirX", lastPlayerDirection.x);
			}
			if (chargeTimeCounter <= 0) 
			{
				myAnim.SetBool ("isChargingAttack", false);
				attackTimeCounter = attackTime;
				state = AI_BallState.ReadyToAttack;
			}
		}

		if (state == AI_BallState.ReadyToAttack) 
		{
			myAnim.SetBool ("isAttacking", true);

			if (lastPlayerDirection.x > 0)
			{
				projectileSpawnPos = new Vector2 (0.9f, 0.75f);
			} 
			else 
			{
				projectileSpawnPos = new Vector2 (-0.9f, 0.75f);
			}

//			lastPlayerDirection -= projectileSpawnPos;
			state = AI_BallState.Attacking;
		}

		if (state == AI_BallState.Attacking) 
		{
			if (attackTimeCounter > 0) 
			{
				attackTimeCounter -= Time.deltaTime;
				myAnim.SetBool ("isAttacking", true);
			} 
			else 
			{
				myAnim.SetBool ("isAttacking", false);
				coolDownCounter = cooldownTime;
				state = AI_BallState.CoolingDown;
				myAIController.holdingPosition = false;
			}
		}

		if (state == AI_BallState.CoolingDown) 
		{
			if (coolDownCounter > 0) 
			{ 
				coolDownCounter -= Time.deltaTime; 
			}
			if (coolDownCounter <= 0)
			{
				state = AI_BallState.NotAttacking;
				myAIController.isAttacking = false;
			}
		}
	}

	void DoAIBehaviour () 
	{
		if (thePlayer == null) return; // return if no player

		if (myAIController.isAttacking == false) // if not attacking, attempt attack
		{
			chargeTimeCounter = chargeTime; 
			state = AI_BallState.ChargingAttack;
			myAIController.holdingPosition = true; // tell AIController to hold position
			myAIController.isAttacking = true;
		}
		else // if attacking...
		{
			if (state != AI_BallState.ReadyToAttack) return; // don't proceed if not ready to attack

			if (state == AI_BallState.ReadyToAttack) // if we are ready to attack
			{
				myAIController.holdingPosition = true; 
				myAnim.SetBool ("isAttacking", true); // play attacking animation
			}

			if (state == AI_BallState.CoolingDown)
			{
				myAIController.holdingPosition = true;
				myAnim.SetBool ("isAttacking", false);
			}
		}
	}

	private void ShootProjectile ()
	{
		if (projectile) 
		{
			projectile.SetActive (true);
			projectile.transform.localPosition = projectileSpawnPos;
			projectileBody.velocity = lastPlayerDirection.normalized * projectileSpeed;
		}
	}
}

public enum AI_BallState
{
	NotAttacking,
	ChargingAttack,
	ReadyToAttack,
	Attacking,
	CoolingDown
}
