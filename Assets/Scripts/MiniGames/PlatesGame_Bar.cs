﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesGame_Bar : MonoBehaviour 
{
	public List<GameObject> plates = new List<GameObject> (); 
	public int totalWeight;

	void Update ()
	{
	}

	void OnTriggerStay2D (Collider2D other) 
	{
		if (other.gameObject.tag == "Plate" && plates.Contains (other.gameObject) == false)
		{
			PlatesGame_Plate plateComp = other.gameObject.GetComponent<PlatesGame_Plate> ();
			if (plateComp.isGrabbed == false) 
			{
				plates.Add (other.gameObject); 
				totalWeight += plateComp.weight;
				if (totalWeight != PlatesManager.instance.requiredWeight) 
				{
					AudioManager.instance.PlayEvent ("MiniGame_Score");
				}
			}
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (other.gameObject.tag == "Plate" && plates.Contains (other.gameObject) == true) 
		{
			PlatesGame_Plate plateComp = other.gameObject.GetComponent<PlatesGame_Plate> (); 
			plates.Remove (other.gameObject); 
			totalWeight -= plateComp.weight;
		}
	}
}