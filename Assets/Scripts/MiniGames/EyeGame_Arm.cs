﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeGame_Arm : MonoBehaviour {

	public float moveSpeed;

	private Rigidbody2D myRigidBody;
	private bool armMoving;
	public bool isAttacking;

	public GameObject arm;
	private Animator myAnim;

	public int hitGoal;
	private int hitCount;


	void Awake ()
	{
		myRigidBody = GetComponent<Rigidbody2D> ();
		myAnim = GetComponentInChildren<Animator> ();
	}

	void Update () 
	{
		if (Input.GetAxisRaw ("Horizontal") > 0.5f || Input.GetAxisRaw ("Horizontal") < -0.5f) 
		{
			myRigidBody.velocity = new Vector2 (Input.GetAxisRaw ("Horizontal") * moveSpeed, myRigidBody.velocity.y);
			armMoving = true;
		}

		if (Input.GetAxisRaw ("Horizontal") < 0.5f && Input.GetAxisRaw ("Horizontal") > -0.5f) 
		{
			myRigidBody.velocity = new Vector2 (0f, myRigidBody.velocity.y);
		}

		if (Input.GetKeyDown (KeyCode.Space) && isAttacking == false) 
		{
			isAttacking = true;
			myAnim.SetBool ("isAttacking", true);
			PlayWhooshSound ();
		}
	}

	void OnEnable ()
	{
		hitCount = 0;
		MiniGameManager.Instance.expSlider.maxValue = hitGoal;
		MiniGameManager.Instance.expSlider.value = 0f;
		MiniGameManager.Instance.instructionText.text = "Use [SPACE] to poke the eye!";
		MiniGameManager.Instance.instructionText.rectTransform.localPosition = new Vector2 (-3.22f, 0.625f);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "SnailEye")  
		{
			hitCount++;
			MiniGameManager.Instance.expSlider.value = hitCount;
			if (hitCount == hitGoal) 
			{
				hitCount = 0;
				MiniGameManager.Instance.expSlider.value = 0f;
				PlayerController.Instance.UpgradeStat (StatUpgrade.Speed);
				AudioManager.instance.PlayEvent ("MiniGame_Win");
			} 
			else
			{
				AudioManager.instance.PlayEvent ("MiniGame_Score");
			}
		}
	}

	void PlayWhooshSound ()
	{
		AudioManager.instance.PlayEvent ("Player_Melee_Whoosh", this.gameObject);
	}
}
