﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;  

public class WorkoutStation : MonoBehaviour 
{
	public string miniGameName;
	public StatUpgrade statUpgrade;
	public bool isOccupied;
	private int userNumber;
	private Animator anim;
	private GameObject userObj;
	private Vector2 originalUserPos;
	SpriteRenderer userRenderer;
	PlayerController player;
	AIController gymGoer; 
	public float staminaDrainRate;
	public float useDistance; 
	public GameObject useIcon;
	public string usedDialog;
	private DialogueManager dMan;
	private bool dialogActive;
	public bool randomDialog;

	void Start () 
	{
		anim = GetComponent<Animator> ();
		dMan = FindObjectOfType<DialogueManager>();
		dialogActive = false;

		if (WorkoutStationList.stations == null)
		{
			WorkoutStationList.stations = new List<WorkoutStation> ();
		}
		WorkoutStationList.AddStation (this);
	}
	
	void Update () 
	{
		if (isOccupied == false || !player) return; //if station is not occupied or is used by NPC

		if (staminaDrainRate == 0) return;

		if (player.TakeDamage (Time.deltaTime * staminaDrainRate) <= 0) 
		{
			player.TakeDamage (staminaDrainRate * Time.deltaTime, this.gameObject, 0.5f, 10f, 0.15f, 1f, 1f, false);
		}
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.CompareTag("Player") && isOccupied == false)
		{
			useIcon.SetActive (true);
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			useIcon.SetActive (false);
		}
	}

	void OnDestroy ()
	{
		WorkoutStationList.stations.Remove (this);
	}

	public void UseStation (GameObject user, int userNum)
	{
		if (isOccupied == false)
		{
			isOccupied = true;
			userNumber = userNum;
			userObj = user;

			if (userNumber == 0) 
			{
				player = user.GetComponent<PlayerController> ();
				userRenderer = player.myRenderer;
				player.isInBench = true;
				player.stationInUse = this;
				userRenderer.enabled = false; // make the user invisible
				player.StopMoving();
				MiniGameManager.Instance.EnableMiniGame (miniGameName);
				MusicManager.instance.PlayTrack ("DK");
				//MusicManager.instance.PlayTrack ("Drum");
			}

			if (userNumber == 1) 
			{
				gymGoer = user.GetComponent<AIController> ();
				userRenderer = gymGoer.myRenderer;
			}

			userRenderer.enabled = false; // make the user invisible
			originalUserPos = userObj.transform.position; // save the users original position
			userObj.transform.position = this.gameObject.transform.position; // move the user to the stations position
			anim.SetBool ("isOccupied", true);
			anim.SetInteger ("userNum", userNumber);
		} 
		else 
		{
			if (dialogActive == false)
			{
				if (randomDialog) 
				{
					usedDialog = getRandomDialog ();
				}
				dMan.ShowBox (usedDialog);
				dialogActive = true;
			} 
			else 
			{
				Debug.Log ("Turnoff Dialog!");
				dialogActive = false;
				dMan.HideBox ();
			}
		}
			
	}

	string getRandomSetsDialog() 
	{
		string dialog = "I Only Have " + Random.Range (2, 10) + " More Sets...";
		return dialog;
	}

	string getRandomDialog() 
	{
		int rand = Random.Range (0, 3); 
		string dialog = null;
		switch (rand) 
		{
		case 0:
			dialog = "I just started.";
			break;
		case 1:
			dialog = "Wait your turn.";
			break;
		case 2:
			dialog = getRandomSetsDialog();
			break;
		}
		return dialog;
	}

	public void LeaveStation ()
	{
		if (isOccupied == true) 
		{
			if (userNumber == 0) 
			{ 
				player.isInBench = false;
				MiniGameManager.Instance.LeaveMiniGame();  
//				MusicManager.instance.StopTrack ("Drum");
				MusicManager.instance.StopTrack ("DK");
			}

			if (userNumber == 1) 
			{
				gymGoer.holdingPosition = false;
				if (statUpgrade == StatUpgrade.Speed) 
				{
					PlayTreadmillStop ();
				}
			}

			userObj.transform.position = originalUserPos; // move the user back to the position they entered in
			userRenderer.enabled = true; // make them visible
			player = null;
			isOccupied = false;
			anim.SetBool ("isOccupied", isOccupied);

			dialogActive = false;
			dMan.HideBox ();
		}
	}

	private void PlayTreadmillSnail ()
	{
		AudioManager.instance.PlayEvent ("Treadmill_Snail", this.gameObject);
		AudioManager.instance.PlayEvent ("Snail_Footstep", this.gameObject);
	}

	private void PlayTreadmillPlayer ()
	{
		AudioManager.instance.PlayEvent ("Treadmill_Player", this.gameObject);
	}

	private void PlayTreadmillStop()
	{
		AudioManager.instance.PlayEvent ("Treadmill_Stop", this.gameObject); 
	}

	private void PlayVendingMachinePlop()
	{
		AudioManager.instance.PlayEvent ("VendingMachine_Plop", this.gameObject);
	}
}

static class WorkoutStationList
{
	static public List<WorkoutStation> stations; 

	public static void AddStation(WorkoutStation station)
	{
		stations.Add(station);
	}
}

public enum StatUpgrade
{
	Strength,
	Endurance,
	Speed
}
