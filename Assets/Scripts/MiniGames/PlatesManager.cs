﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlatesManager : MonoBehaviour {

	public List<PlatesGame_Rack> racks = new List<PlatesGame_Rack> ();
	public List<GameObject> platesPool = new List<GameObject> ();

	public GameObject handObj;

	public int platesTotal;  
	public GameObject bar;

	public int platesNum = 6;

	public GameObject sPlatePf; 
	public GameObject mPlatePf;
	public GameObject lPlatePf;

	public int minWeight; 
	public int maxWeight; 
	public int requiredWeight;
	public int weightOnBar;

	private static PlatesManager platesManager;  
	public static PlatesManager instance;

	private PlatesGame_Bar barComp;

	public int expGain;

	public bool inProgress = false;

	void Awake () 
	{
		if (!platesManager) 
		{ 
			platesManager = FindObjectOfType (typeof (PlatesManager)) as PlatesManager; 
		}
		instance = this;

		barComp = bar.GetComponent<PlatesGame_Bar> ();

		for (int i = 0; i < platesTotal; i++) 
		{
			GameObject newSPlate = (GameObject)Instantiate(sPlatePf); 
			GameObject newMPlate = (GameObject)Instantiate(mPlatePf);
			GameObject newLPlate = (GameObject)Instantiate(lPlatePf);

			newSPlate.SetActive (false);
			newMPlate.SetActive (false);
			newLPlate.SetActive (false);

			newSPlate.transform.SetParent (this.gameObject.transform);
			newMPlate.transform.SetParent (this.gameObject.transform);
			newLPlate.transform.SetParent (this.gameObject.transform);

			platesPool.Add (newSPlate);
			platesPool.Add (newMPlate);
			platesPool.Add (newLPlate);
		}
	}

	void OnEnable ()
	{
		if (inProgress == false) {
			Setup ();
		}
		inProgress = true;
		MiniGameManager.Instance.instructionText.text = "Use [SPACE] to put " + requiredWeight + "lbs on the bar!";
	}

	private void SetHandLocation (GameObject obj)
	{
		
	}
	
	void Update () 
	{
		weightOnBar = barComp.totalWeight;
		MiniGameManager.Instance.expSlider.value = barComp.totalWeight;
		if (barComp.totalWeight == requiredWeight) 
		{
			PlayerController.Instance.UpgradeStat (StatUpgrade.Strength);
			Reset ();
			AudioManager.instance.PlayEvent ("MiniGame_Win");
		}
	}

	void Reset ()
	{
		for (int i = 0; i < racks.Count; i++) 
		{
			racks [i].ReturnPlatesFromRack ();
		}
		Setup ();
	}

	void Setup ()
	{
		handObj.transform.localPosition = new Vector2 (-3.25f, 0f);
		requiredWeight = Random.Range (minWeight / 5, maxWeight / 5) * 5;

		MiniGameManager.Instance.instructionText.rectTransform.localPosition = new Vector2 (-3.22f, 0.625f);

		for (int i = 0; i < platesNum; i++) 
		{
			GameObject newPlate = platesPool[Random.Range(0, platesPool.Count)];
			PlatesGame_Plate plateComp = newPlate.GetComponent<PlatesGame_Plate> (); 

			while (plateComp.weight > requiredWeight)
			{
				newPlate = platesPool[Random.Range(0, platesPool.Count)];
				plateComp = newPlate.GetComponent<PlatesGame_Plate> (); 
			}

			platesPool.Remove (newPlate);

			PlatesGame_Rack rack = racks [Random.Range (0, racks.Count)];

			while (rack.isFull) 
			{
				rack = racks [Random.Range (0, racks.Count)];
			}

			rack.plates.Add (newPlate);
			int plateIndex = rack.plates.Count;

			SetPlateTransform (newPlate, rack);
		}
		
		MiniGameManager.Instance.expSlider.maxValue = requiredWeight;
		MiniGameManager.Instance.expSlider.value = 0f;
	}

	private void SetPlateTransform (GameObject plate, PlatesGame_Rack rackComp) 
	{
		plate.transform.SetParent (rackComp.gameObject.transform);

		float newX = 0.87f - (0.37f * rackComp.plates.Count);  

		plate.transform.localPosition = new Vector3 (newX, 0, 0);

		plate.transform.SetParent (this.transform);

		plate.SetActive (true);
	}
}

