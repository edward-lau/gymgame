﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Com.LuisPedroFonseca.ProCamera2D;

public class SyringeController : MonoBehaviour {

	public float loadTime;
	public float fadeTime;
	public float fadeOutTime;
	public float statBoostRate;
	public GameObject stunObj;

	public GameObject eyePrefab;
	private List<GameObject> eyes = new List<GameObject> ();
	public float eyeNum;
	public float[] eyeScales;

	private float animationCounter;
	private float statBoostCounter;
	private Animator myAnim;
	private bool triggered;

	void Awake ()
	{
		myAnim = GetComponent<Animator> ();
		statBoostCounter = statBoostRate;
	}
	
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Space) && triggered == false) 
		{
			triggered = true;
			myAnim.Play("Stab");
		}

		for (int i = 0; i < eyeNum; i++)
		{
			GameObject newEye = Instantiate (eyePrefab);
			newEye.SetActive (false);
			newEye.transform.SetParent (this.transform); 
			eyes.Add (newEye);
		}
	}

	void OnEnable ()
	{
		MiniGameManager.Instance.instructionText.text = "Press [SPACE] for INSTANT GAINS";
		MiniGameManager.Instance.exitInstructionsText.SetActive (false);
		MusicManager.instance.PlayTrack ("DK");
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag("Neck"))
		{
			StartCoroutine (BoostStats ());
			FadeController.instance.FadeOut(fadeTime);
			StartCoroutine (WaitAndLoadLevel ());
			ProCamera2DShake.Instance.ConstantShake ("EarthquakeSoft");
			stunObj.transform.position = PlayerController.Instance.transform.position;
			stunObj.SetActive (true);
			AudioManager.instance.PlayEvent ("Syringe_Impact", this.gameObject);
			AudioManager.instance.PlayEvent ("Syringe_Transform");
		}
	}

	IEnumerator BoostStats ()
	{
		while (true) 
		{
			statBoostCounter -= Time.deltaTime;
			PlayerController.Instance.UpgradeStat (StatUpgrade.Strength);
			PlayerController.Instance.UpgradeStat (StatUpgrade.Speed);
			PlayerController.Instance.UpgradeStat (StatUpgrade.Endurance);
			yield return new WaitForSeconds(statBoostRate);
		}
	}

	IEnumerator WaitAndLoadLevel ()
	{ 
		MusicManager.instance.StopTrack ("DK");
		MusicManager.instance.PlayTrack ("DKacid");
		yield return new WaitForSeconds (fadeTime);
//		StartCoroutine (SpawnEyes ());
		yield return new WaitForSeconds (loadTime-fadeTime-2);
		SceneFade.instance.fadeSpeed = fadeOutTime;
		SceneFade.instance.BeginFade (1);
		AudioEffectFade.instance.fadeSpeed = fadeOutTime;
		AudioEffectFade.instance.FadeOut ();
		yield return new WaitForSeconds (7f);
		SceneManager.LoadScene ("Sewers");
	}

	IEnumerator SpawnEyes ()
	{
		while (true)
		{
			if (eyes.Count == 0) break;
		
			int lastIndex = eyes.Count - 1;
			GameObject eye = eyes [lastIndex];
			eye.transform.position = new Vector2 (Random.Range (-11f, 11f), Random.Range (-6f, 6f));

			float scaleVal = eyeScales[Random.Range (0, eyeScales.Length)];
			eye.transform.localScale = new Vector3 (scaleVal, scaleVal, 0);
			eye.SetActive (true);
			eyes.Remove (eye);
			yield return new WaitForSeconds (0.01f);
		}
	}
}