﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesGame_Hand : MonoBehaviour {

	public float moveSpeed;

	private Rigidbody2D myRigidBody;
	private Animator myAnim;

	private GameObject plate;

	private FixedJoint2D plateJoint;
	public SpriteRenderer myRenderer;

	public bool isGrabbing = false;


	void Awake ()
	{
		myRigidBody = GetComponent<Rigidbody2D> ();
		myAnim = GetComponent<Animator> ();
	}

	void OnEnable ()
	{
		//isGrabbing = false; 
	}

	void OnDisable ()
	{
//		if (plateJoint) 
//			plateJoint.connectedBody = null;
	}

	void Update () 
	{
		if (Input.GetAxisRaw ("Horizontal") > 0.5f || Input.GetAxisRaw ("Horizontal") < -0.5f) 
		{
			myRigidBody.velocity = new Vector2 (Input.GetAxisRaw ("Horizontal") * moveSpeed, myRigidBody.velocity.y);
		}

		if (Input.GetAxisRaw ("Vertical") > 0.5f || Input.GetAxisRaw ("Vertical") < -0.5f) 
		{
			myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, Input.GetAxisRaw ("Vertical") * moveSpeed);
		}

		if (Input.GetAxisRaw ("Horizontal") < 0.5f && Input.GetAxisRaw ("Horizontal") > -0.5f) 
		{
			myRigidBody.velocity = new Vector2 (0f, myRigidBody.velocity.y);
		}

		if (Input.GetAxisRaw ("Vertical") < 0.5f && Input.GetAxisRaw ("Vertical") > -0.5f) 
		{
			myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, 0f);
		}

		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			isGrabbing = !isGrabbing;

			if (plate && !isGrabbing) 
			{
				plate.GetComponent<PlatesGame_Plate> ().isGrabbed = false;
				plateJoint.connectedBody = null;  
				myRenderer.color = new Color(1f, 1f, 1f, 1f);
				plateJoint.enabled = false;
				plateJoint = null;
				plate = null;
			}

			myAnim.SetBool ("isGrabbing", isGrabbing);
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{

	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.gameObject.tag == "Plate" && isGrabbing && plate == null) 
		{
			plate = other.gameObject;
			plate.GetComponent<PlatesGame_Plate> ().isGrabbed = true;
			myRenderer.color = new Color(1f, 1f, 1f, .5f);
			this.transform.position = plate.transform.position;
			plateJoint = plate.GetComponent<FixedJoint2D> ();
			plateJoint.enabled = true;
			plateJoint.connectedBody = myRigidBody; 
		}
	}
}
