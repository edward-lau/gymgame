﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouthGame_SnailDestroyer : MonoBehaviour {

	public MouthGame_SnailSpawner snailSpawner; 
	 
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "SnailBoy")  
		{
			snailSpawner.ReturnSnail (other.gameObject);
		}
	}

}
