﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeGame_FinishAttack : MonoBehaviour {

	private Animator myAnim;
	private EyeGame_Arm arm;

	void Awake ()
	{
		myAnim = GetComponent<Animator> ();
		arm = GetComponentInParent<EyeGame_Arm>();
	}
		
	private void FinishAttack ()
	{
		arm.isAttacking = false;
		myAnim.SetBool ("isAttacking", false);
	}
}
