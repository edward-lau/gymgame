﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesGame_Rack : MonoBehaviour {

	public List<GameObject> plates = new List<GameObject>();
	public bool isFull;
	
	void Update () 
	{
		if (plates.Count == 4) 
		{
			isFull = true;
		} else 
		{
			isFull = false;
		}
	}

	void OnDisable ()
	{
//		ReturnPlatesFromRack ();
	}

	public void ReturnPlatesFromRack ()
	{
		int numPlates = plates.Count;

		for (int i = 0; i < numPlates; i++) 
		{
			GameObject plate = plates [i];
			PlatesManager.instance.platesPool.Add (plate);
			plate.SetActive (false);
		}
		plates.Clear ();
		isFull = false;
	}
}