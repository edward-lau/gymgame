﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatesGame_Plate : MonoBehaviour 
{
	public int weight;
	public bool isGrabbed;
	public string sfxName;

	void OnCollisionEnter2D (Collision2D other)
	{
		if (other.gameObject.CompareTag ("Plate") && isGrabbed) 
		{
			AudioManager.instance.PlayEvent (sfxName, this.gameObject);
		}

		if (other.gameObject.CompareTag ("PlateGameBorder")) 
		{
			AudioManager.instance.PlayEvent (sfxName, this.gameObject);
		}

		if (other.gameObject.CompareTag ("Bar"))
		{
			AudioManager.instance.PlayEvent (sfxName, this.gameObject);
		}
	}
}