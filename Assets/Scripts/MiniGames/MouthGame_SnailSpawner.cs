﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouthGame_SnailSpawner : MonoBehaviour {

	public List<GameObject> snailPool;
	public List<GameObject> activeSnails;
	public GameObject snailPrefab;
	public GameObject snailDestroyer;

	public int snailCount;

	public float maxSpawnTime;
	public float minSpawnTime;

	public float minX;
	public float maxX;

	private float spawnTimeCounter;


	void Awake ()
	{
		for (int i = 0; i < snailCount; i++)
		{
			GameObject newSnail = (GameObject)Instantiate(snailPrefab);
			newSnail.SetActive (false);
			newSnail.transform.SetParent (this.gameObject.transform);
			snailPool.Add (newSnail);
		}
			
		spawnTimeCounter = Random.Range (minSpawnTime, maxSpawnTime);
	}

	void Update ()
	{
		if (spawnTimeCounter > 0) {
			spawnTimeCounter -= Time.deltaTime;
		} 
		if (spawnTimeCounter <= 0) 
		{
			spawnTimeCounter = Random.Range (minSpawnTime, maxSpawnTime);
			SpawnSnail ();
		}
			

	}
		
	private void SpawnSnail ()
	{
		GameObject snail; 
		int lastAvailableIndex = snailPool.Count - 1;  

		if (lastAvailableIndex >= 0) 
		{
			snail = snailPool [lastAvailableIndex];  
			snailPool.RemoveAt (lastAvailableIndex);  
			float randomX = Random.Range (minX, maxX);
			snail.transform.position = this.gameObject.transform.position + new Vector3(randomX, 0, 0);
			snail.SetActive (true);
			activeSnails.Add (snail);
		} 

	}

	void OnDisable ()
	{
		for (int i = 0; i < activeSnails.Count; i++)
		{
			GameObject snail = activeSnails [i];
			ReturnSnail (snail);
		}
		activeSnails.Clear ();
	}

	public void ReturnSnail (GameObject snail)
	{
		snail.SetActive (false);
		snailPool.Add (snail);
	}
}
