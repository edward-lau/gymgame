﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeGame_ActualEye : MonoBehaviour {

	private Animator myAnim;
	private bool isHit;

	void Awake ()
	{
		myAnim = GetComponent <Animator> ();
		myAnim.SetBool ("isHit", false);
		isHit = false;
	}

	void OnDisable ()
	{
		myAnim.SetBool ("isHit", false);
		isHit = false;
		this.transform.localPosition = new Vector2 (0f, 0f);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "PlayerArm" && isHit == false) 
		{
			isHit = true;
			myAnim.SetBool ("isHit", true);
			PlayHitSound ();
		}
	}

	private void FinishedHit ()
	{
		isHit = false;
		myAnim.SetBool ("isHit", false);
	}

	private void PlayHitSound()
	{
		AudioManager.instance.PlayEvent ("Eye_Impact", this.gameObject);
	}
}
