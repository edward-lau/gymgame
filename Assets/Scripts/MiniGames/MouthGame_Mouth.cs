﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouthGame_Mouth : MonoBehaviour 
{
	public float moveSpeed;
	public MouthGame_SnailSpawner snailSpawner;

	private Rigidbody2D myRigidBody;
	private bool mouthMoving;

	public int snailGoal;  
	private int snailsEaten; 

	void Awake ()
	{
		myRigidBody = GetComponent<Rigidbody2D> ();
	}

	void Update () 
	{
		if (Input.GetAxisRaw ("Horizontal") > 0.5f || Input.GetAxisRaw ("Horizontal") < -0.5f) 
		{
			myRigidBody.velocity = new Vector2 (Input.GetAxisRaw ("Horizontal") * moveSpeed, myRigidBody.velocity.y);
			mouthMoving = true;
		}

		if (Input.GetAxisRaw ("Horizontal") < 0.5f && Input.GetAxisRaw ("Horizontal") > -0.5f) 
		{
			myRigidBody.velocity = new Vector2 (0f, myRigidBody.velocity.y);
		}
	}

	void OnEnable ()
	{
		snailsEaten = 0;
		MiniGameManager.Instance.expSlider.maxValue = snailGoal; 
		MiniGameManager.Instance.expSlider.value = 0f;
		MiniGameManager.Instance.instructionText.text = "Eat protein!";
		MiniGameManager.Instance.instructionText.rectTransform.localPosition = new Vector2 (-3.22f, 0.625f);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "SnailBoy")  
		{
			snailSpawner.ReturnSnail (other.gameObject);
			snailsEaten++; 
			MiniGameManager.Instance.expSlider.value = snailsEaten;
			if (snailsEaten == snailGoal) 
			{
				snailsEaten = 0;
				MiniGameManager.Instance.expSlider.value = 0f;
				PlayerController.Instance.UpgradeStat (StatUpgrade.Endurance);
				AudioManager.instance.PlayEvent ("MiniGame_Win");
			}
			else 
			{
				AudioManager.instance.PlayEvent ("MiniGame_Score");
			}
			PlayEatSound();
			PlaySwallowSound();
		}
	}

	private void PlayEatSound ()
	{
		AudioManager.instance.PlayEvent ("Mouth_Eat", this.gameObject);
	}

	private void PlaySwallowSound()
	{
		AudioManager.instance.PlayEvent ("Mouth_Swallow", this.gameObject);
	}
}
