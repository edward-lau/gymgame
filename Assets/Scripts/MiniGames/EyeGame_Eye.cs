﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeGame_Eye : MonoBehaviour {

	private float posX; 
	public float pingPong;
	public float speed;
	public float minChangeDir;
	public float maxChangeDir;

	public float minSpeed;
	public float maxSpeed;

	private float changeDirCounter;

	private float dir = 1f;

	void OnEnable () 
	{
		posX = Random.Range (-8.55f, 1.69f);  
		transform.localPosition = new Vector3 (posX, 4.19f, 0f);

		changeDirCounter = Random.Range (minChangeDir, maxChangeDir);
		speed = Random.Range (minSpeed, maxSpeed);
	}

	void Update () 
	{
		if (changeDirCounter > 0) 
		{
			changeDirCounter -= Time.deltaTime;
		}

		if (changeDirCounter <= 0) 
		{
			dir *= -1;
			changeDirCounter = Random.Range (minChangeDir, maxChangeDir);
			speed = Random.Range(minSpeed, maxSpeed); 
			speed *= dir;
		}
			
		pingPong += Time.deltaTime * speed;
		float newX = Mathf.PingPong(pingPong, 10.24f) - 8.55f;
		transform.localPosition = new Vector3 (newX, 4.19f, 0f);
	}
}
