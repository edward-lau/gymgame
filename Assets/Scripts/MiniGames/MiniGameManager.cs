﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MiniGameManager : MonoBehaviour 
{
	public List<MiniGameContainer> miniGames = new List<MiniGameContainer> ();
	public GameObject miniGameUI;
	public GameObject miniMap;
	public TextMeshProUGUI instructionText; 
	public GameObject exitInstructionsText;
	public Slider expSlider;
	public bool isPlayingGame;
	private GameObject currentGame;
	private Dictionary<string, MiniGameContainer> miniGameDict = new Dictionary<string, MiniGameContainer> (); 
	private static MiniGameManager miniGameManager; 
	public static MiniGameManager Instance
	{
		get
		{
			if (!miniGameManager) 
			{ 
				miniGameManager = FindObjectOfType (typeof (MiniGameManager)) as MiniGameManager;

				if (!miniGameManager)
				{
					Debug.LogError ("The scene is missing a miniGameManager"); 
				}
			}

			return miniGameManager;
		}
	}

	void Awake ()
	{
		isPlayingGame = false;

		for (int i = 0; i < miniGames.Count; i++) 
		{
			miniGameDict.Add (miniGames[i].name, miniGames[i]);
		}
	}

	public void EnableMiniGame (string name) 
	{
		if (isPlayingGame == false) 
		{
			if (miniGameDict.ContainsKey (name))
			{
				isPlayingGame = true;
				currentGame = miniGameDict[name].miniGameObj; 
				miniGameDict[name].miniGameObj.SetActive (true);
				miniMap.SetActive (true);
				miniGameUI.SetActive (true);
			}
		}
	}

	public void LeaveMiniGame ()
	{
		if (isPlayingGame) 
		{
			isPlayingGame = false;
			currentGame.SetActive (false);
			miniMap.SetActive (false);
			miniGameUI.SetActive (false);
		}
	}

	public static Vector3 GetScreenPosition(Transform transform,Canvas canvas,Camera cam)
	{
		Vector3 pos;
		float width = canvas.GetComponent<RectTransform>().sizeDelta.x;
		float height = canvas.GetComponent<RectTransform>().sizeDelta.y;
		float x = Camera.main.WorldToScreenPoint(transform.position).x / Screen.width;
		float y = Camera.main.WorldToScreenPoint(transform.position).y / Screen.height;
		pos = new Vector3(width * x - width / 2, y * height - height / 2);    
		return pos;    
	}
}

[System.Serializable]
public class MiniGameContainer 
{
	public string name;
	public GameObject miniGameObj;
}