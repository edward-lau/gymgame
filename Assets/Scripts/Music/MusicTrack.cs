﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTrack : MonoBehaviour {

	public string trackName;
	public float fadeSpeed = 0.8f;
	public bool playAtStart;
	private int fadeDir = -1; // -1 = fade in, 1 = fade out
	private float vol = 0;

	private AudioSource source;

	void Awake ()
	{
		source = GetComponent<AudioSource> ();
		MusicManager.instance.AddTrack (trackName, this);
	}

	void Start ()
	{
		if (playAtStart)
		{
			MusicManager.instance.PlayTrack (trackName);
		}
	}

	public void Play ()
	{
		fadeDir = 1;
	}

	public void Stop ()
	{
		fadeDir = -1;
	}

	void Update () 
	{
		vol += fadeDir * fadeSpeed * Time.deltaTime; // fade in/out the alpha value using direction, speed and time.deltatime
		vol = Mathf.Clamp01(vol); // clamp the value between 0 and 1
		source.volume = vol;
	}
}
