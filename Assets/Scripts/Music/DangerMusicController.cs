﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerMusicController : MonoBehaviour {

	public float currentDangerLevel;
	public List<DangerMusicTrack> musicTracks = new List<DangerMusicTrack>();

	void Start () 
	{
		
	}
	
	void Update () 
	{
		currentDangerLevel = GetDangerLevel ();

		for (int i = 0; i < musicTracks.Count; i++) 
		{
			DangerMusicTrack dangerTrack = musicTracks [i];
			if (currentDangerLevel >= dangerTrack.minDanger && currentDangerLevel <= dangerTrack.maxDanger) 
			{
				MusicManager.instance.PlayTrack (dangerTrack.music.trackName);
			} 
			else 
			{
				MusicManager.instance.StopTrack (dangerTrack.music.trackName);
			}
		}
	}

	private int GetDangerLevel ()
	{
		int totalDanger = 0;

		for (int i = 0; i < Enemy_Spawner.instance.activeEnemies.Count; i++) 
		{
			totalDanger += Enemy_Spawner.instance.activeEnemies [i].dangerLevel;
		}

		return totalDanger;
	}
}

[System.Serializable]
public class DangerMusicTrack
{
	public MusicTrack music;
	public int minDanger;
	public int maxDanger;
}
