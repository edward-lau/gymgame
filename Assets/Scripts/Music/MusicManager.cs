﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

	public bool enabled;

	private Dictionary<string, MusicTrack> tracks = new Dictionary<string, MusicTrack>(); 

	private static MusicManager musicManager; 
	public static MusicManager instance 
	{
		get
		{
			if (!musicManager) 
			{ 
				musicManager = FindObjectOfType (typeof (MusicManager)) as MusicManager;
			}
			return musicManager;
		}
	}

	public void AddTrack (string eventName, MusicTrack track)
	{
		if (tracks.ContainsKey(eventName))
		{
			Debug.LogWarning ("There are multiple Music Events named " + eventName);
			return;
		}

		tracks.Add (eventName, track); 
	}

	public void PlayTrack (string eventName) 
	{
		if (enabled == false) return;

		MusicTrack track;

		if (tracks.TryGetValue (eventName, out track))  
		{
			track.Play ();
		}
	}
	
	public void StopTrack (string eventName) 
	{
		MusicTrack track;

		if (tracks.TryGetValue (eventName, out track))
		{
			track.Stop ();
		}
	}
}
