﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSequencer : MonoBehaviour {

	[Serializable]
	public class Step
	{
		public bool active;
	}

	public delegate void HandleTick (double tickTime);
	public event HandleTick Ticked;

	[SerializeField] private Metronome metronome;
	[SerializeField] private List<Step> steps;

	private int currentTick = 0;

	#if UNITY_EDITOR
	public List<Step> GetSteps()
	{
		return steps;
	}
	#endif

	private void OnEnable () 
	{
		if (metronome)
		{
			metronome.Ticked += HandleTicked;
		}
	}
	
	private void OnDisable ()
	{
		if (metronome) 
		{
			metronome.Ticked -= HandleTicked;
		}
	}

	public void HandleTicked(double tickTime)
	{
		int numSteps = steps.Count;

		if (numSteps == 0)
		{
			return;
		}

		Step step = steps[currentTick];

		if (step.active)
		{
			if (Ticked != null)
			{
				Ticked(tickTime);
			}
		}

		currentTick = (currentTick + 1) % numSteps;
	}
}
