﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SamplerVoice : MonoBehaviour {

	private AudioSource source;

	private void Awake ()
	{
		source = GetComponent<AudioSource> ();
	}

	public void Play (AudioClip clip, double startTime)
	{
		source.clip = clip;
		source.PlayScheduled (startTime);
	}

	public void SetVolume (float vol)
	{
		source.volume = vol;
	}
}
