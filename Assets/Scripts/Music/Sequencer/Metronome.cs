﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Metronome : MonoBehaviour {

	public event Action<double> Ticked;

	[SerializeField, Range (15f, 200f)]
	private double tempo = 120.0;
	[SerializeField, Range (1, 8)]
	private int subdivisions = 4;

	private double tickLength;
	private double nextTickTime;

	// recalculate tick length and reset next tick time
	private void Reset()
	{
		Recalculate ();
		nextTickTime = AudioSettings.dspTime + tickLength;
	}

	// calcualte length of tick in seconds from tempo and subdivision values
	private void Recalculate()
	{
		double beatsPerSecond = tempo / 60.0;
		double ticksPerSecond = beatsPerSecond * subdivisions;
		tickLength = 1.0 / ticksPerSecond;
	}

	private void Awake () 
	{
		Reset ();
	}

	// called in the editor when inspector control changes
	// recalculate tick length
	private void OnValidate ()
	{
		if (Application.isPlaying) 
		{
			Recalculate ();
		}
	}

	private void Update ()
	{
		// dspTime is in seconds and is super accurate because its based on the number of samples the audio system processes
		double currentTime = AudioSettings.dspTime;

		// look ahead the length of one frame
		currentTime += Time.deltaTime;

		// catch all ticks within next frame
		while (currentTime > nextTickTime) 
		{
			// if someone has subscribed to ticks from the metronome, let them know we got a tick
			if (Ticked != null)
			{
				// report to any other objects who have subscribed that a tick has occured
				// it also passes the time the tick occured (because of the lookahead, its our scheduled play time)
				Ticked (nextTickTime);
			}

			// increment next tick time
			nextTickTime += tickLength;
		}

	}
}
