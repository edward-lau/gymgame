﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sampler : MonoBehaviour {

	[SerializeField] private StepSequencer sequencer;
	[SerializeField] private List<AudioClip> clips = new List<AudioClip>();
	[SerializeField, Range (1, 8)]
	private int numVoices = 4;
	[SerializeField, Range (-40, 0)]
	private float volume;

	private SamplerVoice[] voices;
	private int nextVoiceIndex;

	private void Awake()
	{
		voices = new SamplerVoice[numVoices];

		for (int i = 0; i < numVoices; ++i) 
		{
			SamplerVoice voice = new GameObject().AddComponent<SamplerVoice>();
			voice.transform.parent = this.transform;
			voice.transform.localPosition = Vector3.zero;
			voice.SetVolume (AudioUtil.DbToLinear (volume));
			voices [i] = voice;
		}

		nextVoiceIndex = 0;
	}

	// subscribe to metronome ticks
	private void OnEnable ()
	{
		if (sequencer) 
		{
			sequencer.Ticked += HandleTicked;
		}
	}
		
	private void OnDisable ()
	{
		if (sequencer) 
		{
			sequencer.Ticked -= HandleTicked;
		}
	}
		
	// play scheduled sound by passing tick time
	private void HandleTicked (double tickTime) 
	{
		AudioClip clip = clips[Random.Range (0, clips.Count)];
		voices [nextVoiceIndex].Play (clip, tickTime);
		nextVoiceIndex = (nextVoiceIndex + 1) % voices.Length;
	}
}
