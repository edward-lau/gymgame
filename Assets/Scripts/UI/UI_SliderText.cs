﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class UI_SliderText : MonoBehaviour {

	public TextMeshProUGUI[] valueText; 
	private Slider slider;


	void Awake ()
	{
		slider = GetComponent<Slider> ();
	} 
	
	void Update () 
	{
		for (int i = 0; i < valueText.Length; i++) 
		{
			valueText[i].text = ((int)slider.value + "/" + (int)slider.maxValue);
		}
	}
}
