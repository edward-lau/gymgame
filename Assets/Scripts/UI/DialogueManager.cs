﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour {

	public GameObject dBox;
	public TextMeshProUGUI dText;
	public bool dialogActive; 
	public float dialogTime;
	private float dialogCounter;
	private CanvasGroup transCanvas;

	private static DialogueManager dialogueManager; 
	public static DialogueManager instance 
	{
		get
		{
			if (!dialogueManager) 
			{ 
				dialogueManager = FindObjectOfType (typeof (DialogueManager)) as DialogueManager;
			}
			return dialogueManager;
		}
	}


	// Use this for initialization
	void Start () {
		dBox.SetActive (false);
//		transCanvas = GetComponent<CanvasGroup> ();
//		transCanvas.alpha = 0.5f;
		dialogActive = false;
	}
	
	// Update is called once per frame
	void Update () {
		/*if (dialogActive && Input.GetKeyDown (KeyCode.Return)) 
		{
			dBox.SetActive (false);
			dialogActive = false;

		}*/
		if (dialogCounter > 0)
		{
			dialogCounter -= Time.deltaTime;
		}
		if (dialogCounter <= 0 && dialogActive) {
			dialogActive = false;
			dBox.SetActive (false);
		}
	}

	public void ShowBox(string dialog)
	{
		dialogCounter = dialogTime;
		dialogActive = true;
		dBox.SetActive (true);
		dText.text = dialog;
//		dBox.GetComponent<RectTransform> ().position = Camera.main.WorldToScreenPoint(boxPos);
	}

	public void HideBox()
	{
		dialogActive = false;
		dBox.SetActive (false);
	}



}
