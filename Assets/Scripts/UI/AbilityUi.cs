﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityUi : MonoBehaviour { 

	public List<Sprite> sprites = new List<Sprite> (); 
	public float coolDownTime; 
	public float coolDownCounter; 

	private SpriteRenderer myRenderer;

	void Start ()  
	{
		myRenderer = GetComponent<SpriteRenderer> ();
	}
	
	void Update ()  
	{  
		int i = (int)((coolDownCounter / coolDownTime) * (sprites.Count-1));
		myRenderer.sprite = sprites [i];
	}
}