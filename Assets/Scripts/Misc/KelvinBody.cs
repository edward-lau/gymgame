﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KelvinBody : MonoBehaviour 
{
	public GameObject syringe;

	void OnEnable ()
	{
		AudioManager.instance.PlayEvent ("Kelvin_Death", this.gameObject);
	}
	
	void SpawnSyringe () 
	{
		syringe.transform.SetParent (null);
		syringe.gameObject.SetActive (true);
	}
}