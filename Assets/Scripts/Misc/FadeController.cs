﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeController : MonoBehaviour {

	private SpriteRenderer renderer;
	private Shader shaderGUItext;  

	private static FadeController fadeFx;
	public static FadeController instance
	{
		get
		{
			if (!fadeFx)
			{ 
				fadeFx = FindObjectOfType (typeof (FadeController)) as FadeController;
			}
			return fadeFx;
		}
	}

	void Awake ()
	{
		renderer = GetComponent<SpriteRenderer> ();
		shaderGUItext = Shader.Find("GUI/Text Shader");
		renderer.color = new Color (Color.red.r, Color.red.g, Color.red.b, 0);
	}

	public void FadeOut (float time) 
	{
		StartCoroutine (Fade(time));
	}
	
	IEnumerator Fade (float time)
	{
		float rate = 1.0f / time;
		float scaleValue;
		float x = 0.0f;

		for (x = 0.0f; x <= 1.0f; x += Time.deltaTime * rate) 
		{
			scaleValue = Mathf.Lerp (0f,1f, x);  
			renderer.color = new Color (renderer.color.r, renderer.color.g, renderer.color.b, scaleValue);
			yield return null;
		}

		if (x >= 1.0f) 
		{
			renderer.color = new Color (renderer.color.r, renderer.color.g, renderer.color.b, 255);
		}
	}
}