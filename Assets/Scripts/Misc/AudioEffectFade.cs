﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioEffectFade : MonoBehaviour {

	public AudioMixer mixer;

	public float fadeSpeed = 0.8f;
	public bool fadeOnAwake = false;
	private int fadeDir = -1; // -1 = fade in, 1 = fade out
	private float volumeScale = 0;
	private Animator animator;

	private static AudioEffectFade audioEffectFade;
	public static AudioEffectFade instance
	{
		get
		{
			if (!audioEffectFade) 
			{
				audioEffectFade = FindObjectOfType (typeof (AudioEffectFade)) as AudioEffectFade;
			}
			return audioEffectFade;
		}
	}

	void Awake ()
	{
		if (fadeOnAwake) 
		{
			FadeIn ();
		}
	}

	public void FadeIn ()
	{
		fadeDir = -1;
	}

	public void FadeOut ()
	{
		fadeDir = 1;
	}

	void Update () 
	{
		volumeScale += fadeDir * fadeSpeed * Time.deltaTime; // fade in/out the alpha value using direction, speed and time.deltatime
		volumeScale = Mathf.Clamp01(volumeScale); // clamp the value between 0 and 1
		float newVol = volumeScale * -80f;
		mixer.SetFloat ("MasterVol", newVol);
	}
}
