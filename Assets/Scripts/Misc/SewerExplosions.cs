﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class SewerExplosions : MonoBehaviour {

	public float minCooldown;
	public float maxCooldown;
	private float cooldownTime;
	private float cooldownCounter; 

	void Awake ()
	{
		cooldownTime = Random.Range (minCooldown, maxCooldown);
		cooldownCounter = cooldownTime;
	}
	
	void Update () 
	{
		if (cooldownCounter > 0) 
		{
			cooldownCounter -= Time.deltaTime;
		}

		if (cooldownCounter <= 0)
		{
			ProCamera2DShake.Instance.Shake ("SmallExplosion");
			cooldownTime = Random.Range (minCooldown, maxCooldown);
			cooldownCounter = cooldownTime;
			AudioManager.instance.PlayEvent ("Sewers_Slam");
		}
	}
}
