﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneFade : MonoBehaviour {

	public Texture2D fadeTexture;
	public float fadeSpeed = 0.8f;

	private int drawDepth = -1000;
	private float alpha = 1.0f;
	private int fadeDir = -1; // -1 = fade in, 1 = fade out

	private static SceneFade sceneFade; 
	public static SceneFade instance 
	{
		get
		{
			if (!sceneFade)
			{ 
				sceneFade = FindObjectOfType (typeof (SceneFade)) as SceneFade;
			}
			return sceneFade;
		}
	}

	void OnGUI () 
	{
		alpha += fadeDir * fadeSpeed * Time.deltaTime; // fade in/out the alpha value using direction, speed and time.deltatime
		alpha = Mathf.Clamp01(alpha); // clamp the value between 0 and 1
		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha); 
		GUI.depth = drawDepth;
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), fadeTexture);
	}

	public float BeginFade (int direction)
	{
		fadeDir = direction;
		return (fadeSpeed);
	}

	void OnLevelWasLoaded ()
	{
		alpha = 1;
		BeginFade (-1);
	}
}
