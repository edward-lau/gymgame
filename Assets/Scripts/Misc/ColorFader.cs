﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorFader : MonoBehaviour {

	public float changeRate;

	public Color[] colors;
	private SpriteRenderer myRenderer; 
	public float counter;
	private Shader shaderGUItext;
	private Shader shaderSpritesDefault;
	private int currentIndex = 0;
	private int targetIndex;


	void Awake () 
	{
//		startColour = new Color(Random.value, Random.value, Random.value);
//		targetColour = new Color(Random.value, Random.value, Random.value);
		myRenderer = GetComponent<SpriteRenderer> (); 
		shaderGUItext = Shader.Find("GUI/Text Shader");
		targetIndex = 1;
		myRenderer.material.shader = shaderGUItext;
		myRenderer.material.color = colors[currentIndex];
	}
	
	// Update is called once per frame
	void Update ()
	{
		counter += Time.deltaTime * changeRate; 
		myRenderer.material.color = Color.Lerp (colors[currentIndex], colors[targetIndex], counter);
		if (counter >= 1) 
		{
			counter = 0;
			currentIndex = targetIndex;
			targetIndex++;
			if (targetIndex >= colors.Length) 
			{
				targetIndex = 0;
			}
		}
	}
}
