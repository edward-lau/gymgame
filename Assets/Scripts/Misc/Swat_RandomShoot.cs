﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swat_RandomShoot : MonoBehaviour {


	public float shootTime;

	public float minCoolDown; 
	public float maxCoolDown; 

	private float coolDownTime;

	private float coolDownCounter; 
	private float shootCounter; 
	private Animator myAnim;
	private SwatState state;

	void Start () 
	{
		myAnim = GetComponent<Animator> ();	
		coolDownTime = Random.Range (minCoolDown, maxCoolDown); 
		coolDownCounter = coolDownTime;
		state = SwatState.Waiting;
		myAnim.SetBool ("isShooting", false);
	}
	
	void Update () 
	{
		if (state == SwatState.Waiting)
		{
			if (coolDownCounter > 0) 
			{
				coolDownCounter -= Time.deltaTime; 
			}
			else 
			{
				shootCounter = shootTime;
				state = SwatState.Shooting; 
				myAnim.SetBool ("isShooting", true);
				AudioManager.instance.PlayEvent ("Swat_Firing", this.gameObject);
			}
		}

		if (state == SwatState.Shooting)
		{
			if (shootCounter > 0) 
			{
				shootCounter -= Time.deltaTime; 
			}
			else 
			{
				coolDownTime = Random.Range (minCoolDown, maxCoolDown);
				coolDownCounter = coolDownTime;
				state = SwatState.Waiting; 
				myAnim.SetBool ("isShooting", false);
			}
		}
	}
}

public enum SwatState
{
	Waiting,
	Shooting
}
