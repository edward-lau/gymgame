﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneTrigger : MonoBehaviour {

	public string sceneName;
	
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag ("Enemy")) 
		{
			StartCoroutine (LoadNextLevel ());
		}
	}

	IEnumerator LoadNextLevel ()
	{
		SceneFade.instance.fadeSpeed = 0.8f;
		SceneFade.instance.BeginFade (1);
		AudioEffectFade.instance.FadeOut ();
		yield return new WaitForSeconds (1f);
		GameManager.instance.LoadScene ("finalboss");
	}
}
