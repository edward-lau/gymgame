﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleFx : MonoBehaviour {

	public float fadeTime;
	public bool isFading;
	public bool onEnable;

	void OnEnable () 
	{
		if (onEnable) 
		{
			StartCoroutine (FadeIn (0.01f, 1f));
		}
		else
		{
			isFading = false;
			transform.parent.transform.localPosition = new Vector3 (0, 0, 0);
			this.transform.localScale = new Vector3 (1f, 1f, 0f);
		}
		transform.rotation = Quaternion.Euler (0, 0, 0);
	}

	public void Fade (float start, float target)
	{
		StartCoroutine (FadeOut (start, target)); 
	}

	public IEnumerator FadeIn (float start, float target)
	{
		float SecondsToFade = fadeTime; 
		float rate = 1.0f / SecondsToFade;
		float scaleValue;
		float x = 0.0f;

		for (x = 0.0f; x <= 1.0f; x += Time.deltaTime * rate) 
		{
			scaleValue = Mathf.Lerp (start, target, x);  
			this.transform.localScale = new Vector3(scaleValue, scaleValue, 0f);
			yield return null;
		}

		if (x >= 1.0f) 
		{
			this.transform.localScale = new Vector3 (1f, 1f, 1f);
		}
	}

	public IEnumerator FadeOut (float start, float target)
	{
		float SecondsToFade = fadeTime; 
		float rate = 1.0f / SecondsToFade;
		float scaleValue;
		float x = 0.0f;

		for (x = 0.0f; x <= 1.0f; x += Time.deltaTime * rate) 
		{
			scaleValue = Mathf.Lerp (start, target, x);  
			this.transform.localScale = new Vector3(scaleValue, scaleValue, 0f);
			yield return null;
		}

		if (x >= 1.0f) 
		{
			this.transform.localScale = new Vector3 (1f, 1f, 1f);
			if (GetComponentInParent<AIController> ()) 
			{
				GetComponentInParent<AIController> ().ReturnToPool ();
			}

			if (GetComponentInParent<PlayerController_Snail> ()) 
			{
				GetComponentInParent<PlayerController_Snail> ().gameObject.SetActive (false);
			}
		}
	}
}
