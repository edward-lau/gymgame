﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFlasher : MonoBehaviour {

	public float flashTime;
	public SpriteRenderer myRenderer;
	private bool flashing;
	private float flashTimeCounter;
	private int flasher;
	private Shader shaderGuiText;
	private Shader shaderSpritesDefault;


	void Awake ()
	{
		shaderGuiText = Shader.Find("GUI/Text Shader");
		shaderSpritesDefault = Shader.Find("Sprites/Default");
	}
	
	void Update ()
	{
		if (flashing)
		{
			if (flashTimeCounter > 0) 
			{
				flashTimeCounter -= Time.deltaTime;
			} 
			else 
			{
				flashTimeCounter = flashTime;
				flasher++;
				switch (flasher) 
				{
				case 1:
					SetWhiteSprite ();
					break;
				case 2: 
					SetNormalSprite ();
					flasher = 0;
					StopFlashing ();
					break;
//				case 3:
//					SetWhiteSprite ();
//					break;
//				case 4:
//					SetNormalSprite ();
//					flasher = 0;
//					StopFlashing ();
//					break;
				}
			}
		}
	}

	public void FlashSprite ()
	{
		flashing = true;
	}

	public void StopFlashing ()
	{
		flashing = false;
		SetNormalSprite ();
	}

	private void SetWhiteSprite() 
	{
		myRenderer.material.shader = shaderGuiText;
		myRenderer.color = Color.white; 
	}

	private void SetNormalSprite()
	{
		myRenderer.material.shader = shaderSpritesDefault;
		myRenderer.color = Color.white;
	}
}