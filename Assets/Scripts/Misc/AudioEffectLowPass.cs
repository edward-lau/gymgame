﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioEffectLowPass: MonoBehaviour 
{
	public AudioMixer mixer;
	Animator animator;
	public float lowPassCutoff;

	private static AudioEffectLowPass audioEffectLowPass;
	public static AudioEffectLowPass instance
	{
		get
		{
			if (!audioEffectLowPass) 
			{
				audioEffectLowPass = FindObjectOfType (typeof (AudioEffectLowPass)) as AudioEffectLowPass;
			}
			return audioEffectLowPass;
		}
	}

	void Start ()
	{
		animator = GetComponent<Animator>();
	}

	public void PlayLowPassHit ()
	{
		animator.Play ("Hit");
	}

	void Update ()
	{
		mixer.SetFloat ("MasterLowPass", lowPassCutoff);
	}
}
