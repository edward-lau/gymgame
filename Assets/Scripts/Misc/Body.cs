﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Body : MonoBehaviour 
{
	public float fadeSpeed = 0.8f;
	public float activeTime;
	public float returnTime; 
	public BodManager bodManager;
	public bool isFading; 
	public string deathSfx;
	public bool flipY;

	private SpriteRenderer renderer;
	private float alpha = 1.0f;
	private int fadeDir = 1; // -1 = fade in, 1 = fade out

	void Awake ()
	{
		renderer = GetComponentInChildren<SpriteRenderer> ();
	}

	void Update () 
	{
		if (isFading) 
		{
			alpha += fadeDir * fadeSpeed * Time.deltaTime; // fade in/out the alpha value using direction, speed and time.deltatime
			alpha = Mathf.Clamp01 (alpha); // clamp the value between 0 and 1
			renderer.color = new Color (renderer.color.r, renderer.color.g, renderer.color.b, alpha); 
		}
	}

	public float BeginFade (int direction)
	{
		fadeDir = direction;
		isFading = true;
		return (fadeSpeed);
	}

	public void ActivateBody (bool flipY)
	{
		alpha = 1;
		renderer.color = new Color (renderer.color.r, renderer.color.g, renderer.color.b, alpha);
		renderer.flipX = flipY;
		StartCoroutine (WaitAndFade ());
	}

	IEnumerator WaitAndFade ()
	{
		yield return new WaitForSeconds (activeTime);
		BeginFade (-1);
		yield return new WaitForSeconds (returnTime);
		isFading = false;
		bodManager.ReturnBod (this);
	}

	private void PlayDeathSfx()
	{
		AudioManager.instance.PlayEvent (deathSfx, this.gameObject); 
	}
}
