﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthSorter : MonoBehaviour {

	private Renderer renderer;
	public int TargetOffset = 0;

	// Use this for initialization
	void Start () {

		renderer = GetComponent<SpriteRenderer> ();
		
	}
	
	// Update is called once per frame
	void Update () {

		renderer.sortingOrder = -(int)(transform.position.y * 100f)+TargetOffset;

		//tutorial VVVV
		/*if (Target == null)
			Target = transform;
		Renderer renderer = GetComponent();
		renderer.sortingOrder = -(int)(Target.position.y * IsometricRangePerYUnit) + TargetOffset;
		*/
	}
}
