﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle_Plate : MonoBehaviour {

	public int plateHealth;
	public bool isInvincible;
	public List<Sprite> sprites = new List<Sprite> ();
	public GameObject brokenPieces;
	private Animator myAnim;
	private SpriteRenderer myRenderer;
	private Rigidbody2D myRigidBody;
	private bool isMoving;
	private Vector3 lastPosition;
	private Vector3 currentPosition;
	private SpriteFlasher spriteFlasher;

	void Awake ()
	{
		myAnim = GetComponent<Animator> ();
		myRenderer = GetComponentInChildren<SpriteRenderer> ();
		myRigidBody = GetComponent<Rigidbody2D> ();
		spriteFlasher = GetComponent<SpriteFlasher> ();
		lastPosition = transform.position;
	}

	public void TakeDamage ()
	{
		plateHealth--;
		int spriteIndex = 0;

		if (plateHealth > sprites.Count - 1) 
		{
			spriteIndex = sprites.Count - 1;
		} 
		else 
		{
			spriteIndex = plateHealth;
		}

		myRenderer.sprite = sprites [spriteIndex];
		if (plateHealth <= 0) 
		{
			brokenPieces.SetActive (true);
			brokenPieces.transform.SetParent (null);
			Destroy (this.gameObject);
		}
		else 
		{
			myAnim.Play ("Hit");
			PlayImpactSound ();
			spriteFlasher.FlashSprite ();
		}
	}

	void Update ()
	{
		if (myRigidBody.velocity != Vector2.zero) 
		{
			if (isMoving == false) 
			{
				isMoving = true;
				PlayPushedSound ();
			}
		}

		if (myRigidBody.velocity == Vector2.zero) 
		{
			if (isMoving) 
			{
				isMoving = false;
				StopPushedSound ();
			}
		}
	}

	void PlayPushedSound ()
	{
		AudioManager.instance.PlayEvent ("Plate_Pushed", this.gameObject);
	}

	void StopPushedSound ()
	{
		AudioManager.instance.StopEvent ("Plate_Pushed", this.gameObject);
	}

	void PlayImpactSound ()
	{
		AudioManager.instance.PlayEvent ("Plate_Impact", this.gameObject);
	}
}