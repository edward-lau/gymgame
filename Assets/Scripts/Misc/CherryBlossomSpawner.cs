﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherryBlossomSpawner : MonoBehaviour {

	public List<GameObject> pooledObjects;
	public List<GameObject> activeObjects;
	public GameObject prefab;

	public int maxInstances;

	public float maxSpawnTime;
	public float minSpawnTime;

	public float minX;
	public float maxX;

	private float spawnTimeCounter;


	void Awake ()
	{
		for (int i = 0; i < maxInstances; i++)
		{
			GameObject newObject = (GameObject)Instantiate(prefab);
			newObject.SetActive (false);
			newObject.transform.SetParent (this.gameObject.transform);
			pooledObjects.Add (newObject);
		}

		spawnTimeCounter = Random.Range (minSpawnTime, maxSpawnTime);
	}

	void Update ()
	{
		if (spawnTimeCounter > 0)
		{
			spawnTimeCounter -= Time.deltaTime;
		} 

		if (spawnTimeCounter <= 0) 
		{
			spawnTimeCounter = Random.Range (minSpawnTime, maxSpawnTime);
			SpawnObject ();
		}
	}

	private void SpawnObject ()
	{
		GameObject newObject; 
		int lastAvailableIndex = pooledObjects.Count - 1;  

		if (lastAvailableIndex >= 0) 
		{
			newObject = pooledObjects [lastAvailableIndex];  
			pooledObjects.RemoveAt (lastAvailableIndex);  
			float randomX = Random.Range (minX, maxX);
			newObject.transform.position = this.gameObject.transform.position + new Vector3(randomX, 0, 0);
			newObject.SetActive (true);
			activeObjects.Add (newObject);
		} 

	}

	void OnDisable ()
	{
		for (int i = 0; i < activeObjects.Count; i++)
		{
			GameObject activeObject = activeObjects [i];
			ReturnObject (activeObject);
		}
	}

	public void ReturnObject (GameObject activeObject)
	{
		activeObject.SetActive (false);
		pooledObjects.Add (activeObject);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag("CherryBlossom"))
		{
			ReturnObject (other.gameObject);
		}
	}
}
