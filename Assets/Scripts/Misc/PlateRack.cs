﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateRack : MonoBehaviour 
{
	public int maxhealth;
	public int maxPlates;
	public GameObject platePrefab;
	private int currentHealth;
	public List<GameObject> platePool = new List<GameObject>(); 
	private Animator myAnim;
	private SpriteFlasher spriteFlasher;

	void Awake ()
	{
		myAnim = GetComponent<Animator> ();
		spriteFlasher = GetComponent<SpriteFlasher> ();

		for (int i = 0; i < maxPlates; i++) 
		{
			GameObject newPlate = Instantiate (platePrefab);
			newPlate.SetActive (false);
			newPlate.transform.SetParent (this.gameObject.transform);
			newPlate.transform.localPosition = Vector2.zero;
			platePool.Add (newPlate);
		}

		currentHealth = maxhealth;
	}

	public void TakeDamage (GameObject attackerObj)
	{
		currentHealth--;
		if (currentHealth <= 0)
		{
			SpawnPlate ();
			currentHealth = maxhealth;
		}
		myAnim.Play ("Hit");
		PlayImpactSound ();
		spriteFlasher.FlashSprite ();
	}

	private void SpawnPlate ()
	{
		if (platePool.Count == 0) return;
		
		int lastIndex = platePool.Count - 1; 
		GameObject newPlate = platePool [lastIndex];
		platePool.Remove (newPlate);
		newPlate.transform.localPosition = new Vector2 (Random.Range(-1f,1f), Random.Range (-0.25f, 0.25f));
		newPlate.transform.SetParent (null);

		newPlate.SetActive (true);
	}

	void PlayImpactSound () 
	{
		AudioManager.instance.PlayEvent ("PlateRack_Impact", this.gameObject); 
	}
}
