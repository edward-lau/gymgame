﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour
{
	public float xPingPong;
	public float xSpeed;
	public float xRange;
	public float yPingPong;
	public float ySpeed;
	public float yRange;

	void Update()
	{
		xPingPong += Time.deltaTime * xSpeed;
		yPingPong += Time.deltaTime * ySpeed;
		float newX = Mathf.PingPong(xPingPong, xRange)-xRange;
		float newY = Mathf.PingPong(xPingPong, yRange)-yRange;
		transform.localPosition = new Vector3 (newX, newY, 0f);


	}
}
