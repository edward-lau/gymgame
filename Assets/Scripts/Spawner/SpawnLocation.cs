﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLocation : MonoBehaviour 
{
	public bool canSpawn = true;
	public Obstacle_Plate plate;

	void Start () 
	{
		if (plate) 
		{
			canSpawn = false;
		}
		Enemy_Spawner.instance.spawnLocations.Add (this);
	}

	void Update ()
	{
		if (plate == false)
		{
			canSpawn = true;
		}
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.gameObject.tag == "PlateObstacle" && plate == false)
		{
			canSpawn = false;
			plate = other.GetComponent<Obstacle_Plate>();
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (plate == false) return;

		if (other.gameObject == plate.gameObject)
		{
			canSpawn = true;
		}
	}
}