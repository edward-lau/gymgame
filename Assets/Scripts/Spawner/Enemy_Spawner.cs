﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Enemy_Spawner : MonoBehaviour 
{
	public AnimationCurve difficultyCurve;
	public float maxTime;
	public TextMeshProUGUI timerUi;
	public bool displayTime;
	public float maxDifficulty;

	public float maxSpawnTime;
	public float minSpawnTime;
	public float spawnTimeCounter;

	public List<WeightedEnemy> weightedEnemies;
	public List<BossContainer> bosses;
	public List<SpawnLocation> spawnLocations;
	public List<AIController> activeEnemies;

	public Dictionary<string, List<AIController>> poolByType;
	public List<AIController> totalPool;

	private float currentTime = 0.0f;

	private static Enemy_Spawner enemySpawner;
	public static Enemy_Spawner instance;
	void Awake ()
	{
		if (!enemySpawner) 
		{
			enemySpawner = FindObjectOfType (typeof (Enemy_Spawner)) as Enemy_Spawner;  
		}
		instance = enemySpawner;
	}

	void Start () 
	{
		for (int i = 0; i < weightedEnemies.Count; i++)
		{
			WeightedEnemy enemy = weightedEnemies [i];
			if (enemy.maxInstances == 0) 
				continue;

			for (int p = 0; p < enemy.maxInstances; p++)
			{
				AIController newEnemy = Instantiate(enemy.controller);  
				newEnemy.gameObject.SetActive (false);
				newEnemy.transform.SetParent (this.gameObject.transform);
				if (enemy.bodyManager)
				{
					newEnemy.bodyManager = enemy.bodyManager;
				}
				if(poolByType == null) 
				{
					poolByType = new Dictionary<string, List<AIController>>();
				}
				if(poolByType.ContainsKey(newEnemy.aiType) == false) 
				{
					poolByType[newEnemy.aiType] = new List<AIController>();
				}
				poolByType[newEnemy.aiType].Add(newEnemy);
				totalPool.Add (newEnemy.GetComponent<AIController> ());
			}
		}

		for (int i = 0; i < bosses.Count; i++) 
		{ 
			AIController newBoss = Instantiate(bosses[i].controller);  
			newBoss.gameObject.SetActive (false); 
			newBoss.transform.SetParent (this.gameObject.transform); 
			newBoss.bodyManager = bosses [i].bodyManager;
			if(poolByType.ContainsKey(newBoss.aiType) == false) 
			{
				poolByType[newBoss.aiType] = new List<AIController>();
			}
			poolByType[newBoss.aiType].Add(newBoss); 
		}

		spawnTimeCounter = Random.Range (minSpawnTime, maxSpawnTime);
		currentTime = maxTime;
	}
	
	void Update () 
	{
		if (currentTime < 0) return;
		
		currentTime -= Time.deltaTime;

		if (displayTime) 
		{
			timerUi.text = "" + (int)currentTime;
		} 
		else 
		{
			timerUi.text = "";
		}

		if (spawnTimeCounter > 0) 
		{
			spawnTimeCounter -= Time.deltaTime;
		}

		if (spawnTimeCounter <= 0) 
		{
			spawnTimeCounter = Random.Range (minSpawnTime, maxSpawnTime);
			Spawn ();
		}

		for (int i = 0; i < bosses.Count; i++) 
		{
			AIController boss = bosses[i].controller;
			if (bosses[i].spawnTime == (int)currentTime && bosses[i].hasSpawned == false) 
			{
				if (poolByType[boss.aiType].Count > 0) // if it's pool isn't empty, get it
				{
					bosses [i].hasSpawned = true;
					int lastIndex = poolByType[boss.aiType].Count - 1;
					AIController newBoss = poolByType[boss.aiType][lastIndex];
					poolByType[boss.aiType].Remove (newBoss);
					newBoss.transform.position = new Vector2 (0, 5);
					newBoss.gameObject.SetActive (true);
					activeEnemies.Add (newBoss);
				}
			}
		}
	}

	void Spawn ()
	{
		int difficultyTotal = (int)((difficultyCurve.Evaluate (1-(currentTime / maxTime))) * maxDifficulty);
		int difficultyCount = 0;

		List<AIController> enemyWave = new List<AIController> (); // list of enemies in current wave

		while (difficultyCount < difficultyTotal) // get the right number of enemies according to difficulty
		{
			if (totalPool.Count == 0 && enemyWave.Count == 0)  
			{ 
				return;
			} 
			else if (totalPool.Count == 0 && enemyWave.Count > 0)
			{
				break;
			}

			WeightedEnemy newEnemy = GetEnemy (); // get a weighted enemy
			string type = newEnemy.controller.aiType; 
			if (poolByType[type].Count > 0) // if it's pool isn't empty, get it
			{
				int lastIndex = poolByType[type].Count - 1;
				AIController enemy = poolByType[type][lastIndex];
				enemyWave.Add (enemy);
				poolByType[type].Remove (enemy);
				totalPool.Remove (enemy);
				difficultyCount += newEnemy.difficulty;
			}
		}
			
		List<SpawnLocation> blockedSpawns = new List<SpawnLocation> (); // a list of blocked spawns which will be damaged

		for (int i = 0; i < enemyWave.Count; i++) 
		{
			AIController newEnemy = enemyWave [i];
			SpawnLocation spawnLoc = spawnLocations [Random.Range (0, spawnLocations.Count)];
			if (spawnLoc.canSpawn == false) // if spawn location is blocked...
			{
				if (spawnLoc.plate.plateHealth > 1) // if plate will survive this spawn, add it to blocked spawns list and continue to next enemy
				{
					poolByType[newEnemy.aiType].Add (newEnemy);
					totalPool.Add (newEnemy);
					if (blockedSpawns.Contains (spawnLoc) == false) 
					{
						blockedSpawns.Add (spawnLoc);
					}
					continue;
				}
				else if (spawnLoc.plate.plateHealth == 1) // otherwise if plate will die this spawn, destroy it and spawn the enemy
				{
					spawnLoc.plate.TakeDamage();
				}
			}
			newEnemy.transform.position = GetRandomTransform (spawnLoc.gameObject, 0.1f, 0.1f);
			newEnemy.gameObject.SetActive (true);
			activeEnemies.Add (newEnemy);
		}

		for (int i = 0; i < blockedSpawns.Count; i++) // damage all blocked spawns
		{
			SpawnLocation spawnLoc = blockedSpawns [i];
			spawnLoc.plate.TakeDamage ();
		}
	}

	private WeightedEnemy GetEnemy()
	{
		int index = 0;

		if (weightedEnemies.Count > 1)  
		{
			int totalWeight = 0;
			foreach(WeightedEnemy enemy in weightedEnemies) // add up the total weight
			{
				if (enemy.maxInstances == null) continue;
				totalWeight += enemy.weight;  
			}

			int roll = Random.Range (0, totalWeight); // roll random number from 0 to total weight
			int sum = 0; // sum weights as we iterate over them

			for (int i = 0; i < weightedEnemies.Count; i++) 
			{
				sum += weightedEnemies[i].weight; 
				if (sum > roll) 
				{
					index = i; //If the added weight is now GREATER than what we rolled, we found our item, break 
					break; 
				}
			}
		}
		else  // if there is only one AudioClip the index is simply 0.
		{
			index = 0;
		}
		return weightedEnemies[index];
	}

	private Vector3 GetRandomTransform (GameObject obj, float minDist, float maxDist)
	{
		float rot = Random.Range(1f, 360f);
		Vector3 direction = Quaternion.AngleAxis(rot, Vector3.up) * Vector3.forward;
		Ray ray = new Ray(obj.transform.position, direction);

		return ray.GetPoint(Random.Range(minDist, maxDist));
	}

	private void PlaySfx ()
	{
		
	}
}

[System.Serializable]
public class WeightedEnemy 
{
	public AIController controller; 
	public int difficulty;
	public int weight;
	public int maxInstances;
	public BodManager bodyManager;
}

[System.Serializable]
public class BossContainer
{
	public AIController controller;
	public int spawnTime;
	public BodManager bodyManager;
	public bool hasSpawned;
}