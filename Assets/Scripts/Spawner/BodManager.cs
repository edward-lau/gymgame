﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodManager : MonoBehaviour {

	public GameObject prefab;
	public int maxInstances;
	private List<Body> bodies = new List<Body>();
	private List<Body> activeBods = new List<Body>();
	private List<GameObject> bossBod = new List<GameObject> ();
	public bool boss;

	void Awake () 
	{
		for (int i = 0; i < maxInstances; i++) 
		{
			GameObject newBod = Instantiate (prefab);
			newBod.SetActive (false);
			if (!boss) 
			{
				Body bod = newBod.GetComponent<Body> ();
				bod.bodManager = this;
				bod.transform.SetParent (this.transform);
				bodies.Add (bod);
			} 
			else 
			{
				newBod.transform.SetParent (this.transform);
				bossBod.Add (newBod);
			}

		}
	}
	
	public void SpawnBod (Vector2 position, bool flipY) 
	{
		if (bodies.Count > 0 && !boss) 
		{
			int lastIndex = bodies.Count - 1;
			Body bod = bodies [lastIndex];
			bodies.Remove (bod);
			bod.transform.position = position;
			activeBods.Add (bod);
			bod.gameObject.SetActive (true);
			bod.ActivateBody (flipY);
		} 
		else if (bossBod.Count > 0 && boss)
		{
			int lastIndex = bossBod.Count - 1;
			GameObject body = bossBod [lastIndex];
			bossBod.Remove (body);
			body.transform.position = position;
			body.SetActive (true);
		}
	}

	public void ReturnBod (Body bod)
	{
		bod.gameObject.SetActive (false);
		activeBods.Remove (bod);
		bodies.Add (bod);
	}
}
