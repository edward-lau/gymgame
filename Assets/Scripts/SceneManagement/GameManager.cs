﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameManager : MonoBehaviour 
{
	public GameObject gainsOverUi;

	public Scene currentScene; 
//	private PlayerController player;
	public GameObject player;
	private GainStates gainState;
	private int strengthStat;
	private int enduranceStat;
	private int speedStat;

	private static GameManager gameManager; 
	public static GameManager instance 
	{
		get
		{
			if (!gameManager) 
			{ 
				gameManager = FindObjectOfType (typeof (GameManager)) as GameManager;
			}
			return gameManager;
		}
	}

	void Awake ()
	{
		currentScene = SceneManager.GetActiveScene ();
	}

	void Start ()
	{
		if (PlayerController.Instance) 
		{
//			player = PlayerController.Instance;
//			player.gameObject.SetActive (true);
//			Load ();
			// LOAD HERE ??? YES!
		}
	}

	void OnDisable()
	{
//		Save ();
	}

	void Update ()
	{
		if (player) 
		{
			CheckGainsOver ();
		}
	}
		
	public void LoadScene (string sceneName)
	{
		// SAVE HERE ???
		SceneManager.LoadScene (sceneName, LoadSceneMode.Single);
	}
		
	private void CheckGainsOver ()
	{
		if (player.activeInHierarchy == false) 
		{
			gainState = GainStates.gainsOver;
		}

		if (gainState == GainStates.gainsOver) 
		{
			gainsOverUi.SetActive (true);
			if (Input.GetKeyDown (KeyCode.Space))
			{
				StartCoroutine (Reload ());
			}
		}
	}

//	public void Save()
//	{
//		FileStream file;
//		BinaryFormatter bf = new BinaryFormatter ();
//
//		if (File.Exists (Application.persistentDataPath + "/playerStats.dat")) {
//			file = File.Open (Application.persistentDataPath + "/playerStats.dat", FileMode.Open);
//		} else {
//			file = File.Create (Application.persistentDataPath + "/playerStats.dat");
//		}
//
//		PlayerStats stats = new PlayerStats ();
//		stats.strength = player.strengthStat;
//		stats.endurance = player.enduranceStat;
//		stats.speed = player.speedStat;
//
//		bf.Serialize (file, stats);
//		file.Close ();
//	}
//
//	public void Load()
//	{
//		if (File.Exists (Application.persistentDataPath + "/playerStats.dat")) 
//		{
//			BinaryFormatter bf = new BinaryFormatter ();
//			FileStream file = File.Open (Application.persistentDataPath + "/playerStats.dat", FileMode.Open);
//			PlayerStats stats = (PlayerStats)bf.Deserialize (file);
//			file.Close ();
//
//			player.SetStat (StatUpgrade.Strength, stats.strength);
//			player.SetStat (StatUpgrade.Endurance, stats.endurance);
//			player.SetStat (StatUpgrade.Speed, stats.speed);
//		}
//	}

	// public void ResetSave()
	// {
	// 	if (File.Exists (Application.persistentDataPath + "/playerStats.dat")) 
	// 	{
	// 		File.Delete (Application.persistentDataPath + "/playerStats.dat");
	// 	}
	// }

	IEnumerator Reload ()
	{
		SceneFade.instance.fadeSpeed = 0.8f;
		SceneFade.instance.BeginFade (1);
		AudioEffectFade.instance.FadeOut ();
		yield return new WaitForSeconds (1f);
		SceneManager.LoadScene (currentScene.name, LoadSceneMode.Single);
	}

}
	
public enum GainStates
{
	gaining,
	gainsOver
}

[Serializable]
class PlayerStats
{
	public int strength;
	public int endurance;
	public int speed;
}
	
