﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneCredits : MonoBehaviour {

	public Texture2D fadeTexture;
	
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			StartCoroutine (LoadTitle ());
		}
	}

	IEnumerator LoadTitle ()
	{
		SceneFade.instance.fadeTexture = fadeTexture;
		SceneFade.instance.fadeSpeed = 0.8f;
		SceneFade.instance.BeginFade (1);
		yield return new WaitForSeconds (1f);
		GameManager.instance.LoadScene ("0_TitleScreen");
	}
}
