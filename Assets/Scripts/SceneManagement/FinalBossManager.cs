﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBossManager : MonoBehaviour {

	public List<SpawnLocation> spawnLocations = new List<SpawnLocation> ();
//	public List<GameObject> ghostPrefabs = new List<GameObject> ();
//	public GameObject pentagram;
	public Texture2D fadeTexture;
	public float waitTime;
	public float fadeTime;
	public float loadTime;
	private bool win;

	void Start () 
	{
		win = false;
		spawnLocations = Enemy_Spawner.instance.spawnLocations;
	}
	
	void Update () 
	{
		if (GetEmptySpawnLocations () == 3) 
		{
			if (win == false) 
			{
				win = true;
				StartCoroutine (WaitAndLoadLevel ());
			}
		}
	}

	private int GetEmptySpawnLocations()
	{
		int count = 0;
		for (int i = 0; i < spawnLocations.Count; i++)
		{
			if (spawnLocations[i].canSpawn) 
			{
				count++;
			}
		}
		return count;
	}

	IEnumerator WaitAndLoadLevel ()
	{ 
		yield return new WaitForSeconds (waitTime);
		SceneFade.instance.fadeTexture = fadeTexture;
		SceneFade.instance.fadeSpeed = fadeTime;
		SceneFade.instance.BeginFade (1);
		AudioEffectFade.instance.fadeSpeed = fadeTime;
		AudioEffectFade.instance.FadeOut ();
		yield return new WaitForSeconds (loadTime-fadeTime-2);
		yield return new WaitForSeconds (7f);
		GameManager.instance.LoadScene ("4_Credits");
	}
}
