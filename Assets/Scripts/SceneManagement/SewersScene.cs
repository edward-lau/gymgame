﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SewersScene : MonoBehaviour {

	void Start () 
	{
		AudioManager.instance.PlayEvent ("Sewers_Water");
	}

	public void LoadNextScene () 
	{
		StartCoroutine (LoadNextLevel ());
	}

	IEnumerator LoadNextLevel ()
	{
		SceneFade.instance.fadeSpeed = 0.8f;
		SceneFade.instance.BeginFade (1);
		AudioEffectFade.instance.FadeOut ();
		yield return new WaitForSeconds (1f);
		GameManager.instance.LoadScene ("finalboss");
	}
}
