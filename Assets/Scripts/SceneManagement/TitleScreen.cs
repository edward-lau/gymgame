﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreen : MonoBehaviour {

	void Start () 
	{
		AudioManager.instance.PlayEvent ("amb_menu_wind");
	}
	
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			StartCoroutine (StartGame ());
		}
	}

	IEnumerator StartGame ()
	{
		SceneFade.instance.fadeSpeed = 0.8f;
		SceneFade.instance.BeginFade (1);
		AudioEffectFade.instance.FadeOut ();
		yield return new WaitForSeconds (1f);
		GameManager.instance.LoadScene ("gym_Ed");
	}
}
