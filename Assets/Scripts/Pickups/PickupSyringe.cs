﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSyringe : MonoBehaviour 
{
	SpriteRenderer renderer;

	void Start ()
	{
		renderer = GetComponent<SpriteRenderer> ();
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag ("Player")) 
		{
			MiniGameManager.Instance.EnableMiniGame ("Syringe");
			renderer.enabled = false;
		}
	}
}
