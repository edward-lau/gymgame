﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupManager : MonoBehaviour
{
	public PickupBase prefab;
	public float dropChance;
	public int maxInstances;
	private List<PickupBase> pickups = new List<PickupBase> ();

	private static PickupManager manager; 
	public static PickupManager instance
	{
		get
		{
			if (!manager) 
			{ 
				manager = FindObjectOfType (typeof (PickupManager)) as PickupManager;
			}
			return manager;
		}
	}

	void Awake () 
	{
		for (int i = 0; i < maxInstances; i++) 
		{
			PickupBase newPickup = Instantiate (prefab);
			newPickup.gameObject.SetActive (false);
			newPickup.transform.SetParent (this.transform);
			newPickup.pickupManager = this;
			pickups.Add (newPickup);
		}
	}

	public void SpawnPickup (Vector2 position) 
	{
		if (Random.Range (0f, 100f) > dropChance)
			return;

		if (pickups.Count > 0) 
		{
			int lastIndex = pickups.Count - 1;
			PickupBase pickup = pickups [lastIndex];
			pickups.Remove (pickup);
			pickup.transform.position = position;
			pickup.gameObject.SetActive (true);
		}
	}

	public void ReturnToPool (PickupBase pickup)
	{
		pickup.gameObject.SetActive (false);
		pickups.Add (pickup);
	}
}