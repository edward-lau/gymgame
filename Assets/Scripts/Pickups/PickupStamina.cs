﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupStamina : PickupBase 
{
	public float staminaBoost;
	public GameObject effectObj;

	public override void ApplyEffect ()
	{
		if (PlayerController.Instance.currentStamina < PlayerController.Instance.maxStamina) 
		{
			PlayerController.Instance.TakeDamage (-staminaBoost);
			pickupManager.ReturnToPool (this);
			EnablePickupSprites ();
			PlayPickupSound ();
		}
	}

	private void PlayPickupSound ()
	{
		AudioManager.instance.PlayEvent ("Pickup_Stamina", PlayerController.Instance.gameObject);
	}

	private void EnablePickupSprites ()
	{
		effectObj.SetActive (false);
		effectObj.transform.position = PlayerController.Instance.transform.position;
		effectObj.transform.SetParent (PlayerController.Instance.transform);
		effectObj.SetActive (true);
	}
}