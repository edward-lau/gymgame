﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.LuisPedroFonseca.ProCamera2D;
using TMPro;

public class PlayerController : MonoBehaviour 
{
	public List<Ability> abilitiesList = new List<Ability> ();

	public float speedStatBoost;
	public float strengthStatBoost;
	public float enduranceStatBoost;

	public float baseAttack;
	public float baseMoveSpeed;
	public float baseStamina;

	public float moveSpeed; 
	public float diagonalSpeedMod;
	private float currentMoveSpeed; 
	public float attackTime;

	public HurtEnemy basicAttack;
	public GameObject weaponHolder;
	public GameObject useHand;

	// STATS
	public Slider staminaSlider;
	public TextMeshProUGUI speedText;
	public TextMeshProUGUI strengthText;
	public TextMeshProUGUI enduranceText;
	public int speedStat = 1;
	public int strengthStat = 1;
	public int enduranceStat = 1;

	public bool isInBench;
	public float currentStamina;
	public float maxStamina;
	public float flashTime;

	private Rigidbody2D myRigidBody;
	public Animator anim;
	public SpriteRenderer myRenderer; 
	private Shader shaderGUItext;  
	private Shader shaderSpritesDefault;  
	private Vector2 knockbackDir;

	public bool isStunned = false; 
	public float stunTimeCounter;
	private float knockBackSpeed;
	private float knockBackCounter = 0;
	private float flashTimeCounter = 0;
	private byte flasher = 0; 

	public bool attacking; 
	public float attackTimeCounter;
	public bool canMove;

	public bool playerMoving; 
	private bool usingStuff;
	private float usingTime = .1f;
	private float usingTimeCounter;
	public WorkoutStation stationInUse;
	public Vector2 lastMove;

	private static bool playerExists; 
	private static PlayerController player; 
	public static PlayerController Instance  
	{
		get
		{
			if (!player)
			{ 
				player = FindObjectOfType (typeof (PlayerController)) as PlayerController;
			}

			return player;
		}
	}

	public GameObject GetPlayer()
	{
		return gameObject;
	}

	void Awake () 
	{
		myRigidBody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		shaderGUItext = Shader.Find("GUI/Text Shader");
		shaderSpritesDefault = Shader.Find("Sprites/Default");
		currentStamina = maxStamina;
		staminaSlider.maxValue = maxStamina;
		staminaSlider.value = currentStamina;
		speedText.text = "Speed: " + speedStat;
		strengthText.text = "Strength: " + strengthStat;
		enduranceText.text = "Endurance: " + enduranceStat;
		canMove = true;
		InitializeAbilities ();
	} 

	void InitializeAbilities () {
		for (int i = 0; i < abilitiesList.Count; i++) 
		{
			abilitiesList [i].Initialize ();
		}		
	}

	void Start ()
	{

	}

	public float TakeDamage (float damage)
	{
		currentStamina -= damage;
		if (currentStamina > maxStamina) 
		{
			currentStamina = maxStamina;
		}
		staminaSlider.value = currentStamina;
		return currentStamina;
	}
		

	public float TakeDamage (float damage, GameObject attacker, float stunTime, float knockSpeed, float knockBackTime, float shakeStrength, float shakeDur, bool playHitSound) 
	{
		if (isStunned == false) 
		{
			if (isInBench) 
			{
				stationInUse.LeaveStation ();
			}

			isStunned = true;
			stunTimeCounter = stunTime;
			knockBackCounter = knockBackTime; 
			knockBackSpeed = knockSpeed; 
			currentStamina -= damage;
			staminaSlider.value = currentStamina;
			knockbackDir = (this.transform.position - attacker.transform.position).normalized;
			myRigidBody.velocity = Vector2.zero;

			if (shakeStrength > 0 && shakeDur > 0) 
			{
				Vector2 shakeDir = (this.transform.position - attacker.transform.position).normalized;  
				float shakeAngle = GetAngle (this.transform.position, attacker.transform.position);
				ScreenShake (shakeDur, shakeDir, shakeAngle);
			}

			anim.SetBool ("PlayerHit", isStunned);
			anim.SetFloat ("HitX", knockbackDir.x);
			anim.SetFloat ("HitY", knockbackDir.y);
		
			flashTimeCounter = flashTime;
			flasher = 0;
			SetWhiteSprite ();

			if (playHitSound) 
			{
				PlayHitSound ();
				AudioEffectLowPass.instance.PlayLowPassHit ();
			}
		}

		return currentStamina;

	}

	public void UpgradeStat (StatUpgrade stat)
	{
		if (stat == StatUpgrade.Speed) 
		{
			speedStat += 1;
			moveSpeed = baseMoveSpeed + ((speedStat-1) * speedStatBoost);
			speedText.text = "Speed: " + speedStat; 
		} 
		else if (stat == StatUpgrade.Strength) 
		{
			strengthStat += 1;
			basicAttack.damage = baseAttack + ((strengthStat-1) * strengthStatBoost); 
			strengthText.text = "Strength: " + strengthStat;
		} 
		else if(stat == StatUpgrade.Endurance) 
		{
			enduranceStat += 1;
			maxStamina = baseStamina + ((enduranceStat-1) * enduranceStatBoost);
			staminaSlider.maxValue = maxStamina;
			enduranceText.text = "Endurance: " + enduranceStat;
		}
	}

	public void SetStat (StatUpgrade stat, int newValue)
	{
		if (stat == StatUpgrade.Speed) 
		{
			speedStat = newValue;
			moveSpeed = baseMoveSpeed + ((speedStat-1) * speedStatBoost);
			speedText.text = "Speed: " + speedStat; 
		} 
		else if (stat == StatUpgrade.Strength) 
		{
			strengthStat = newValue;
			basicAttack.damage = baseAttack + ((strengthStat-1) * strengthStatBoost); 
			strengthText.text = "Strength: " + strengthStat;
		} 
		else if(stat == StatUpgrade.Endurance) 
		{
			enduranceStat = newValue;
			maxStamina = baseStamina + ((enduranceStat-1) * enduranceStatBoost);
			staminaSlider.maxValue = maxStamina;
			enduranceText.text = "Endurance: " + enduranceStat;
		}
	}
	
	void Update ()
	{
		playerMoving = false;

		//stun time
		if (stunTimeCounter > 0) 
		{
			stunTimeCounter -= Time.deltaTime;
		}
		if (stunTimeCounter <= 0 && isStunned == true) 
		{
			isStunned = false;
			anim.SetBool ("PlayerHit", isStunned);
			Time.timeScale = 1f;
			SetNormalSprite ();
			if (currentStamina <= 0) 
			{
				this.gameObject.SetActive (false);
				MusicManager.instance.StopTrack ("Main");
			}
		}

		// Movement
		if (CanMove()) 
		{
			if (Input.GetAxisRaw ("Horizontal") > 0.5f || Input.GetAxisRaw ("Horizontal") < -0.5f) 
			{
				myRigidBody.velocity = new Vector2 (Input.GetAxisRaw ("Horizontal") * currentMoveSpeed, myRigidBody.velocity.y);
				playerMoving = true;
				lastMove = new Vector2 (Input.GetAxisRaw ("Horizontal"), 0f);
			}

			if (Input.GetAxisRaw ("Vertical") > 0.5f || Input.GetAxisRaw ("Vertical") < -0.5f) 
			{
				myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, Input.GetAxisRaw ("Vertical") * currentMoveSpeed);
				playerMoving = true;
				lastMove = new Vector2 (0f, Input.GetAxisRaw ("Vertical"));
			}

			if (Input.GetAxisRaw ("Horizontal") < 0.5f && Input.GetAxisRaw ("Horizontal") > -0.5f) 
			{
				myRigidBody.velocity = new Vector2 (0f, myRigidBody.velocity.y);
			}

			if (Input.GetAxisRaw ("Vertical") < 0.5f && Input.GetAxisRaw ("Vertical") > -0.5f) 
			{
				myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, 0f);
			}

			if (Mathf.Abs(Input.GetAxisRaw ("Horizontal")) > 0.5f && Mathf.Abs (Input.GetAxisRaw ("Vertical")) > 0.5f)
			{
				currentMoveSpeed = moveSpeed * diagonalSpeedMod;
			} 
			else 
			{
				currentMoveSpeed = moveSpeed;
			}
		}

		// Use stuff
		if (Input.GetKeyDown (KeyCode.F) && usingStuff == false && attacking == false) 
		{
			usingStuff = true;
			if (isInBench == false) 
			{
				myRigidBody.velocity = Vector2.zero;
				useHand.SetActive (true);
				usingTimeCounter = usingTime;
			} 
			else 
			{
				stationInUse.LeaveStation ();
			}
		}

		// Use timer
		if (usingTimeCounter > 0f) 
		{
			usingTimeCounter -= Time.deltaTime;
		} 
		else 
		{
			usingStuff = false;
			useHand.SetActive (false);
		}

		// Attack 
		if (Input.GetKeyDown (KeyCode.Space) && CanAttack()) 
		{
			StopMoving ();
			weaponHolder.SetActive (true);
			attackTimeCounter = attackTime;
			attacking = true;
			anim.SetBool ("PlayerAttacking", true);
			PlayMeleeSound ();
		}

		// Attack timer
		if (attackTimeCounter > 0f) 
		{
			attackTimeCounter -= Time.deltaTime;
		} 
		else 
		{
			attacking = false;
			anim.SetBool ("PlayerAttacking", false);
			weaponHolder.SetActive (false);
		}

		if (isStunned)
		{
			if (flashTimeCounter > 0) 
			{
				flashTimeCounter -= Time.deltaTime;
			} 
			else 
			{
				flashTimeCounter = flashTime;
				flasher++;
				switch (flasher) 
				{
				case 1:
					SetNormalSprite ();
					break;
				case 2: 
					SetColorSprite (new Color (1f, 0.4f, 0.4f));
					break;
				case 3:
					SetNormalSprite ();
					break;
				case 4:
					SetWhiteSprite ();
					flasher = 0;
					break;
				}
			}
			if (knockBackCounter > 0) 
			{
				transform.Translate (knockbackDir.normalized * knockBackSpeed * Time.deltaTime);
				knockBackCounter -= Time.deltaTime;
			}
		} 
		else
		{
			anim.SetFloat ("MoveX", Input.GetAxisRaw ("Horizontal"));
			anim.SetFloat ("MoveY", Input.GetAxisRaw ("Vertical"));
			anim.SetBool ("PlayerMoving", playerMoving);
			anim.SetFloat ("LastMoveX", lastMove.x); 
			anim.SetFloat ("LastMoveY", lastMove.y);
		}

		UpdateAbilities ();
	}

	private void UpdateAbilities() 
	{
		for (int i = 0; i < abilitiesList.Count; i++) 
		{
			abilitiesList [i].CallUpdate ();
		}
	}

	private bool CanMove ()
	{
		if (attacking == false && isInBench == false && isStunned == false && canMove == true) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	private bool CanAttack ()
	{
		if (attacking == false && isInBench == false && isStunned == false)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void SetWhiteSprite() 
	{
		myRenderer.material.shader = shaderGUItext;
		myRenderer.color = Color.white; 
	}

	public void SetColorSprite(Color color)
	{
		myRenderer.material.shader = shaderGUItext;
		myRenderer.color = color; 
	}

	public void SetNormalSprite()
	{
		myRenderer.material.shader = shaderSpritesDefault;
		myRenderer.color = Color.white;
	}
	public void ScreenShake (float duration, Vector2 strength, float angle)
	{ 
		ProCamera2DShake.Instance.Shake (0.04f, strength, 0, 0f, angle, default(Vector3), 0.034f, true); 
	}

	public void StopMoving ()
	{
		myRigidBody.velocity = Vector2.zero;
	}

	private float GetAngle(Vector2 pointA, Vector2 pointB)
	{
		Vector2 direction = (pointA - pointB).normalized;
		float angle = Mathf.Atan2(direction.y,  direction.x) * Mathf.Rad2Deg;
		if (angle < 0f) angle += 360f;
		return angle;
	}

	private void PlayMeleeSound ()
	{
		AudioManager.instance.PlayEvent ("Player_Melee_Whoosh", this.gameObject);
	}

	private void PlayHitSound ()
	{
		AudioManager.instance.PlayEvent ("Player_Hit", this.gameObject);
	}
}