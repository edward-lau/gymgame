﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController_Snail : MonoBehaviour {

	public float moveSpeed;
	private Rigidbody2D myRigidBody;
	private Animator anim;
	private bool isMoving;
	private Vector2 lastMove;
	public bool canMove;
	public SpriteRenderer myRenderer;

	private static PlayerController_Snail playerController_Snail;
	public static PlayerController_Snail instance
	{
		get 
		{
			if (!playerController_Snail)
			{
				playerController_Snail = FindObjectOfType (typeof(PlayerController_Snail)) as PlayerController_Snail;
			}

			return playerController_Snail;
		}
	}

	void Awake ()
	{
		myRigidBody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}
	
	void Update () 
	{
		isMoving = false;

		if (canMove)
		{
			if (Input.GetAxisRaw ("Horizontal") > 0.5f || Input.GetAxisRaw ("Horizontal") < -0.5f) {
				myRigidBody.velocity = new Vector2 (Input.GetAxisRaw ("Horizontal") * moveSpeed, myRigidBody.velocity.y);
				isMoving = true;
				lastMove = new Vector2 (Input.GetAxisRaw ("Horizontal"), 0f);
			}

			if (Input.GetAxisRaw ("Vertical") > 0.5f || Input.GetAxisRaw ("Vertical") < -0.5f) {
				myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, Input.GetAxisRaw ("Vertical") * moveSpeed);
				isMoving = true;
			}

			if (Input.GetAxisRaw ("Horizontal") < 0.5f && Input.GetAxisRaw ("Horizontal") > -0.5f) {
				myRigidBody.velocity = new Vector2 (0f, myRigidBody.velocity.y);
			}

			if (Input.GetAxisRaw ("Vertical") < 0.5f && Input.GetAxisRaw ("Vertical") > -0.5f) {
				myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, 0f);
			}
		}

		anim.SetFloat ("MoveX", Input.GetAxisRaw ("Horizontal"));
		anim.SetBool ("isMoving", isMoving);
		anim.SetFloat ("LastMoveX", lastMove.x); 
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag("Laser"))
		{
			this.gameObject.SetActive (false);
		}
	}
}
