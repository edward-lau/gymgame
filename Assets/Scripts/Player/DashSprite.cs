﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashSprite : MonoBehaviour {

	private Animator anim;

	void Awake ()
	{
		anim = GetComponent<Animator> ();
	}

	void OnEnable () 
	{
		this.gameObject.transform.position = PlayerController.Instance.transform.position + new Vector3 (0f, 0.5f, 0f);
		anim.SetFloat ("LastMoveX", PlayerController.Instance.lastMove.x);
		anim.SetFloat ("LastMoveY", PlayerController.Instance.lastMove.y);
	}
}