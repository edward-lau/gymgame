﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class HurtEnemy : MonoBehaviour {

	public float damage;
	public float stunTime;
	public float knockBackSpeed; 
	public float knockBackTime;
	public float shakeStrength;
	public float shakeDuration;
	public bool playSfx;

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag("Enemy"))
		{
			other.gameObject.GetComponent<AIController>().TakeDamage (damage, PlayerController.Instance.gameObject, stunTime, knockBackSpeed, knockBackTime, shakeStrength, shakeDuration, playSfx);
		}

		if (other.gameObject.CompareTag ("PlateRack"))
		{
			other.gameObject.GetComponent<PlateRack>().TakeDamage (this.gameObject); 
		}
	}

	void OnEnable ()
	{
		Vector2 shakeDir = PlayerController.Instance.lastMove;
		float shakeAngle = GetAngle (PlayerController.Instance.transform.position, this.transform.position);
		ProCamera2DShake.Instance.Shake (0.04f, shakeDir, 0, 0f, shakeAngle, default(Vector3), 0.034f, true);
	}

	private float GetAngle(Vector2 pointA, Vector2 pointB)
	{
		Vector2 direction = (pointA - pointB).normalized;
		float angle = Mathf.Atan2(direction.y,  direction.x) * Mathf.Rad2Deg;
		if (angle < 0f) angle += 360f;
		return angle;
	}
}
