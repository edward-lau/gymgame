﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxFootsteps : MonoBehaviour {

	public string[] footstepNames;
	PlayerController playerController;
	Animator anim;
	float currentFrameFootstepLeft;
	float lastFrameFootstepLeft; 
	float currentFrameFootstepRight;
	float lastFrameFootstepRight = 1f;

	void Awake ()
	{
		anim = GetComponent<Animator> ();
	}

	private void PlayFootstep ()
	{
		for (int i = 0; i < footstepNames.Length; i++)
		{
			AudioManager.instance.PlayEvent (footstepNames[i], gameObject);
		}
	}

	void Update ()
	{
		currentFrameFootstepLeft = anim.GetFloat ("FootstepL");
		if (currentFrameFootstepLeft > 0 && lastFrameFootstepLeft < 0) 
		{
			PlayFootstep ();
		}
		lastFrameFootstepLeft = anim.GetFloat ("FootstepL");

		currentFrameFootstepRight = anim.GetFloat ("FootstepR");
		if (currentFrameFootstepRight < 0 && lastFrameFootstepRight >= 0) 
		{
			PlayFootstep ();
		}
		lastFrameFootstepRight = anim.GetFloat ("FootstepR");
	}
}