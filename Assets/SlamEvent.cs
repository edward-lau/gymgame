﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlamEvent : MonoBehaviour {

	public Ability_Slam slamAbility; 
	

	public void Slam () 
	{
		slamAbility.Slam ();
	}
}
